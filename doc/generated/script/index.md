% Script modules

* [problem](problem.html) -- Main interface
* [solver](solver.html) -- Solver interfaces
* [mapped_block](mapped_block.html) -- Mapped block mesh generator
* [merge_blocks](merge_blocks.html) -- Support for tying mapped-block meshes
* [mateval](mateval.html) -- Interpreter for matrix expressions
* [sparse](sparse.html) -- Sparse matrix support library
* [sym](sym.html) -- Symbolic expressions
* [util](util.html) -- General utilities
