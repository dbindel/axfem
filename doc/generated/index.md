% Auto-generated documentation

# Script modules

* [problem](script/problem.html) -- Main interface
* [solver](script/solver.html) -- Solver interfaces
* [mapped_block](script/mapped_block.html) -- Mapped block mesh generator
* [merge_blocks](script/merge_blocks.html) -- Support for tying
  mapped-block meshes
* [mateval](script/mateval.html) -- Interpreter for matrix expressions
* [sparse](script/sparse.html) -- Sparse matrix support library
* [sym](script/sym.html) -- Symbolic expressions
* [util](script/util.html) -- General utilities

# Core libraries

* [mvbuilder](corelib/mvbuilder.html) -- Interface to block
  matrix/vector assembly
* [chebboundary](corelib/chebboundary.html) -- Chebyshev polynomial
  boundary approximation
* [fesvalues](corelib/fesvalues.html) -- Finite element value sensitivities
* [material](corelib/material.html) -- Material / element matrix
  definition
  
# Numerical libraries

* [areigs](numeric/areigs.html) -- Interface to ARPACK
* [assembler](numeric/assembler.html) -- Fast assembly of finite
  element matrices
* [mvbuilder](numeric/mvbuilder.html) -- Matrix and vector assembly
  operations
* [block_alloc](numeric/block_alloc.html) -- Arena-based memory
  allocation
* [cscmatrix](numeric/cscmatrix.html) -- Basic sparse matrix class
* [umfmatrix](numeric/umfmatrix.html) -- Interface to UMFPACK
* [chebfun](numeric/chebfun.html) -- Chebyshev approximation of smooth
  functions
* [chebcurve](numeric/chebcurve.html) -- Chebyshev approximation of
  smooth curves
