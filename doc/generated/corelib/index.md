% Core libraries

* [mvbuilder](mvbuilder.html) -- Interface to block matrix/vector assembly
* [chebboundary](chebboundary.html) -- Chebyshev polynomial boundary approximation
* [fesvalues](fesvalues.html) -- Finite element value sensitivities
* [material](material.html) -- Material / element matrix definition
