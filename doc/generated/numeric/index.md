% Numerical libraries

* [areigs](areigs.html) -- Interface to ARPACK
* [assembler](assembler.html) -- Fast assembly of finite element matrices
* [mvbuilder](mvbuilder.html) -- Matrix and vector assembly operations
* [block_alloc](block_alloc.html) -- Arena-based memory allocation
* [cscmatrix](cscmatrix.html) -- Basic sparse matrix class
* [umfmatrix](umfmatrix.html) -- Interface to UMFPACK
* [chebfun](chebfun.html) -- Chebyshev approximation of smooth functions
* [chebcurve](chebcurve.html) -- Chebyshev approximation of smooth curves
