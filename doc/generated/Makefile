DOCS=   script/util.html script/sparse.html \
	script/sym.html script/mateval.html script/solver.html \
	script/problem.html \
	script/mapped_block.html \
	script/merge_blocks.html \
	numeric/areigs.html numeric/block_alloc.html \
	numeric/assembler.html numeric/mvbuilder.html \
	numeric/chebfun.html numeric/chebcurve.html \
	numeric/cscmatrix.html numeric/umfmatrix.html \
	corelib/chebboundary.html \
	corelib/material.html corelib/fesvalues.html \
	corelib/problem.html

LUA=../../tools/lua-5.1.4/src/lua
LDOC=$(LUA) ../../tools/ldoc/ldoc.lua

.PHONY: html pdf clean

html: $(DOCS) script/index.html numeric/index.html corelib/index.html index.html
pdf:  ${DOCS:.html=.pdf}

index.html: index.md
	pandoc $< -s --toc -c pandoc.css -o $@

script/%.md: ../../script/%.lua
	$(LDOC) -p pandoc -attribs '.lua' -o $@ $^

numeric/%.md: ../../src/numeric/%.h ../../src/numeric/%.cc
	$(LDOC) -p pandoc -attribs '.C' -o $@ $^

numeric/%.md: ../../src/numeric/%.h
	$(LDOC) -p pandoc -attribs '.C' -o $@ $^

corelib/%.md: ../../src/corelib/%.h ../../src/corelib/%.cc
	$(LDOC) -p pandoc -attribs '.C' -o $@ $^

corelib/%.md: ../../src/corelib/%.h
	$(LDOC) -p pandoc -attribs '.C' -o $@ $^

%.pdf: %.md
	pandoc $< -o $@

%.html: %.md
	pandoc $< -s --toc -c ../pandoc.css \
		--mathjax --highlight-style pygments -o $@

clean:
	rm -f $(DOCS) ${DOCS:.html=.pdf} ${DOCS:.html=.md} 
	rm -f corelib/index.html numeric/index.html script/index.html index.html
	rm -f *~
