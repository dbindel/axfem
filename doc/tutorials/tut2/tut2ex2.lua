--ldoc
--[[

# Exercise 2.2 - Both Sides Clamped

Could you change the boundary conditions to include both 
edges to be clamped?

--]]

--ldoc

require "mapped_block"

T0 = 300

SILICON = {
   id    = 0, -- unique material id
   E     = 165e9,
   nu    = 0.26,
   rho   = 2330.0,
   kappa = 142,
   alpha = 2.6e-6,
   cv    = 710,
   T0    = T0 } -- T0 on the lhs is a key in material table,
                -- T0 on the rhs is the value from global variable T0


function hemisphere_mesh_generator(R, h, theta1, theta2)
   local function map(x,y)
      local r = R + y*h/2
      local t = pi/2-( (1-x)*theta2 + (1+x)*theta1 )/2
      return r*cos(t), -r*sin(t)
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 96,             -- divisions along meridian
      n = 5}              -- divisions through thickness
end

R      = 1e-3  -- mean radius
h      = 1e-5  -- thickness
theta1 = 15*pi/180  -- angle at the south pole side
theta2 = 90*pi/180  -- angle at the north pole side


P = Problem:create{ params  = {R,h,theta1,theta2},
                 m       = 2,
                 meshgen = hemisphere_mesh_generator }

P:add_material(SILICON)

P:mesh_gradient()


--ldoc
--[[

## Solution

--]]

function free(r,z,d)
  return false
end

function fixed1(r,z,d)
  return abs(theta1 - atan2(r,-z)) < 0.01*(d/r)
end

function fixed2(r,z,d)
  return abs(theta2 - atan2(r,-z)) < 0.01*(d/r)
end

P:set_boundary_conditions{ radial  = { fixed1,fixed2},
                           angular = { fixed1,fixed2},
                           axial   = { fixed1,fixed2},
                           temperature={free} }

--ldoc

f0 = P:get_eigenfrequency()
BF = P:get_bryans_factor()
Q  = P:get_quality_factor()

print("Frequency      : "..f0)
print("Bryan's factor : "..BF) 
print("Quality factor : "..Q)

P:write_inp{"CCdisp0.inp"; field=0}
P:write_inp{"CCdisp1.inp"; field=1}
P:write_inp{"CCdisp2.inp"; field=2}

