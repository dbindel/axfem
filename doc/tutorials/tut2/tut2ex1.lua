--ldoc
--[[

# Exercise 2.1 - Thickness Variation 

Could you write a mesh generator which linearly decrease the 
thickness as a function of $\theta$ from `hmax` to `hmin`?

--]]

--ldoc

require "mapped_block"

T0 = 300

SILICON = {
   id    = 0, -- unique material id
   E     = 165e9,
   nu    = 0.26,
   rho   = 2330.0,
   kappa = 142,
   alpha = 2.6e-6,
   cv    = 710,
   T0    = T0 } -- T0 on the lhs is a key in material table,
                -- T0 on the rhs is the value from global variable T0

--ldoc
--[[

## Solution

--]]
function thickness_variation_mesh_generator(R, hmax, hmin, theta1, theta2)
   local function map(x,y)
      local t = pi/2-( (1-x)*theta2 + (1+x)*theta1 )/2
      local h = hmin + (hmax-hmin)*(1+x)/2 
      local r = R + y*h/2
      return r*cos(t), -r*sin(t)
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 96,             -- divisions along meridian
      n = 5}              -- divisions through thickness
end


R      = 1e-3  -- mean radius
hmax   = 2e-5  -- max thickness at theta1
hmin   = 1e-5  -- min thickness at theta2
theta1 = 15*pi/180  -- angle at the south pole side
theta2 = 90*pi/180  -- angle at the north pole side

P = Problem:create{ params  = {R,hmax,hmin,theta1,theta2},
                 m       = 2,
                 meshgen = thickness_variation_mesh_generator }

--[[
 The rest of the code is similar to the tutorial
--]]
--ldoc

P:add_material(SILICON)

P:mesh_gradient()

P:write_eps("thickness_variation.eps")

function free(r,z,d)
  return false
end

function fixed(r,z,d)
  return abs(theta1 - atan2(r,-z)) < 0.01*(d/r)
end

P:set_boundary_conditions{ radial  = { fixed},
                           angular = { fixed},
                           axial   = { fixed},
                           temperature={free} }

f0 = P:get_eigenfrequency()
BF = P:get_bryans_factor()
Q  = P:get_quality_factor()

print("Frequency      : "..f0)
print("Bryan's factor : "..BF) 
print("Quality factor : "..Q)
