--ldoc
--[[ 

# Exercise 1.1 - Tilted Ring

In the tutorial, we considered the ring to be a thin cylinder.
Could you add a tilt $\theta$ to the cross section so that the ring
becomes a cut from a cone? Could you write a `for` loop to investigate
how $f_0$, $BF$, and $Q$ change as a function of $\theta$? 

<center>
<img src="tiltedring.png" width="300" height="400"/>
</center>

--]]

--[[

## Solution

--]]
require "mapped_block"

T0 = 300

SILICON = {
   id    = 0, -- unique material id
   E     = 165e9,
   nu    = 0.26,
   rho   = 2330.0,
   kappa = 142,
   alpha = 2.6e-6,
   cv    = 710,
   T0    = T0 } -- T0 on the lhs is a key in material table,
                -- T0 on the rhs is the value from global variable T0

function tilted_ring_mesh_generator(R,h,L,theta)
   local function map(x,y)
      local xx = x*h/2 
      local yy = y*L/2
      local r  = R   + xx*cos(theta) + yy*sin(theta)
      local z  = L/2 - xx*sin(theta) + yy*cos(theta)  

      return r,z
   end
   
   return mapped_block{
            phi = map,
            material = SILICON,
            m = 2,
            n = 10    } 
end

R = 2e-3 
h = 1.6e-4
L = 7e-4

-- Opens a file in write mode for the results
f = io.open("tilted_results.txt","w")


-- Sweeps the tilt angle in 5 degree steps from 0 to 90.
for deg=0,90,5 do 

theta = deg*pi/180
P = Problem:create{ params  = {R,h,L,theta},
                 meshgen = tilted_ring_mesh_generator,
                 refinement = 3, -- refines the mesh returned by the
                                 -- mesh generator 3 times 
                 m       = 2 }
                      
P:add_material(SILICON)

P:mesh_gradient() -- computes finite difference mesh gradient

P:write_eps("tilted_ring.eps")

function free(r,z,d) return false end
P:set_boundary_conditions{ radial={free},angular={free},
                            axial={free},temperature={free}}

f0 = P:get_eigenfrequency()
BF = P:get_bryans_factor()
Q  = P:get_quality_factor()

-- Writes the value f0, BF, and Q to file
f:write(deg.." "..f0.." "..BF.." "..Q.."\n")

end

f:close()

--[[

The saved results are plotted in files `ex1_f0.png`, `ex1_bf.png` and
`ex1_qf.png`.

<center>
<img src="ex1_f0.png" width="600" height="450" />
<img src="ex1_bf.png" width="600" height="450" />
<img src="ex1_qf.png" width="600" height="450" />
</center>

--]]
