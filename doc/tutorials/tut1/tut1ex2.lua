--ldoc
--[[ 

# Exercise 1.2 - Bulged Ring

In the tutorial we considered a rectangular cross section.
What happens if the sides were a bit bulged? Could you modify
the mesh generator to include the radius of curvature $k$ on
both inner and outer sides? Could you compare the concave and
convex situations? How much does Bryan's factor change with
a slight curvature effect for a rectangular cross section?

<center>
<img src="bulgedring.png" width="600" height="300"/>
</center>


--]]

--[[

## Solution

--]]

require "mapped_block"

T0 = 300

SILICON = {
   id    = 0, -- unique material id
   E     = 165e9,
   nu    = 0.26,
   rho   = 2330.0,
   kappa = 142,
   alpha = 2.6e-6,
   cv    = 710,
   T0    = T0 } -- T0 on the lhs is a key in material table,
                -- T0 on the rhs is the value from global variable T0

--[[ 

We have to change the mesh generator. There are various ways to write a
mapping function. Here we will assume that z-coordinate of mesh points are
not effected by bulging. In radial direction we will consider the effective
thickness to be larger than h. Using the figure below, we can compute the
relevant lengths. 

<center>
<img src="bulgeddiag.png" width="450" height="500" />
</center>


--]]

function bulged_ring_mesh_generator(R,h,L,k)
   local function map(x,y)
      local d  = sqrt(k*k-L*L/4) 
      local yy = y*L/2
      local u  = sqrt(k*k-yy*yy)-d
      if k < 0 then u = -u end
      local hy = h + 2*u
      local xx = x*hy/2 
      local r  = R   + xx
      local z  = yy  

      return r,z
   end
   
   return mapped_block{
            phi = map,
            material = SILICON,
            m = 2,
            n = 10    } 
end

--[[



--]]

R = 2e-3 
h = 1.6e-4
L = 7e-4

-- Open a file in write mode for the results
f = io.open("bulged_results.txt","w")


--[[ 

For various ratios $n=k/L$, we will compute results. `nvals` table
provides some samples for both concave and convex situations in
order of increasing curvature. 

--]]

nvals = {-3,-5,-10,-30,-50,-100,100,50,30,10,5,3}
for i,n in ipairs(nvals) do

k = L*n

--[[

The rest of the code is similar to the tutorial. After computing the results,
we will write them to a text file in order to process further with your
favorite graphing tool. 

--]]

P = Problem:create{ params  = {R,h,L,k},
                 meshgen = bulged_ring_mesh_generator,
                 refinement = 3,
                 m       = 2 }
                      
P:add_material(SILICON)

P:mesh_gradient() -- computes finite difference mesh gradient

-- this will output the mesh with a filename indexed with i
P:write_eps("bulged_ring_"..i..".eps")

function free(r,z,d) return false end
P:set_boundary_conditions{ radial={free},angular={free},
                            axial={free},temperature={free}}

f0 = P:get_eigenfrequency()
BF = P:get_bryans_factor()
Q  = P:get_quality_factor()

--[[

Now we can write the results to the txt file.  

--]]
f:write(k.." ")
f:write(f0.." ")
f:write(BF.." ")
f:write(Q.."\n")

end -- end of the loop

f:close()

--[[

If we plot the results using MATLAB, we observe the following behavior
for each value. Notice that the plots, `ex2_f0.png`, `ex2_bf.png`
and `ex2_qf.png` are generated for their dependence on the curvature. 

<center>
<img src="ex2_f0.png" width="600" height="450" />
<img src="ex2_bf.png" width="600" height="450" />
<img src="ex2_qf.png" width="600" height="450" />
</center>

--]]
