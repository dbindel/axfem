% Tutorials

1.  [Lua and the AxFEM front end](tut0/tut0.html)
2.  [Ring resonator](tut1/tut1.html)
3.  [Hemispherical shell](tut2/tut2.html)
4.  [Torus](tut3/tut3.html)
