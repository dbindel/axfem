--ldoc
--[[
# Exercise 0.1

Could you write a function to print the standard deviation 
of numbers in an array?  


--]]

--[[

## Solution

--]]

function stddev(t)
   local n = #t
   if n < 1 then return nil end
   
   local function mean (t)  -- you can embed functions within functions
      local s = 0           -- scope of s is mean function 
      for i,v in pairs(t) do 
         s = s + v
      end
      return s/n            -- scope of n is within stddev
   end  

   local s = 0
   local m = mean(t)
   for i,v in pairs(t) do
      s = s + (v-m)^2
   end
   
   return math.sqrt(s/(n-1))-- we use function sqrt from the math library
end 

-- Let's test it
t = {1,4,9,16,25,36,49,64,81,100}
print(stddev(t)) -- should print 34.1736

