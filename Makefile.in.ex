ifeq ($(USER),dbindel)

# deal.II install directory
DEALII_COMMON = /Users/dbindel/pkg/deal.II/common/
include $(DEALII_COMMON)/Make.global_options

# UMFPACK includes (needed while using deal.II + UMFPACK separate)
UMF_INCLUDE=/opt/local/include/ufsparse/
UMFLIB=/opt/local/lib/libSuiteSparse.dylib

# Libraries (debug and main)
libs.g   := $(lib-deal2.g)
libs.o   := $(lib-deal2.o)

# Lua platform (aix ansi bsd freebsd generic linux macosx mingw posix solaris)
LUA_PLAT=macosx

RUBY=/usr/local/bin/ruby

AXFEM_HOME = /Users/dbindel/work/axfem
D2L_SCRIPTS = $(AXFEM_HOME)/script

else #####################################################################

ifeq ($(USER), erdal)
# deal.II install directory
DEALII_COMMON = /home/erdal/Work/toolbox/deal.II/common
include $(DEALII_COMMON)/Make.global_options

# UMFPACK includes (needed while using deal.II + UMFPACK separate)
UMF_INCLUDE=/home/erdal/Work/toolbox/SuiteSparse/UMFPACK/Include
UMFLIB=/home/erdal/Work/toolbox/SuiteSparse/dinamik/libSuiteSparse.so.1.0

# Libraries (debug and main)
libs.g   := $(lib-deal2.g)
libs.o   := $(lib-deal2.o)

# Lua platform (aix ansi bsd freebsd generic linux macosx mingw posix solaris)
LUA_PLAT=linux

RUBY=/usr/local/bin/ruby

AXFEM_HOME = /home/erdal/Work/research/bitbucket/axfem
D2L_SCRIPTS = $(AXFEM_HOME)/script


else ### VIRTUAL MACHINE 

# deal.II install directory
DEALII_COMMON = /home/xubuntu/src/deal.II/common/
include $(DEALII_COMMON)/Make.global_options

# UMFPACK includes (needed while using deal.II + UMFPACK separate)
UMF_INCLUDE=/home/xubuntu/src/SuiteSparse/UMFPACK/Include
UMFLIB=/home/xubuntu/src/SuiteSparse/dinamik/libSuiteSparse.so.1.0

# Libraries (debug and main)
libs.g   := $(lib-deal2.g)
libs.o   := $(lib-deal2.o)

# Lua platform (aix ansi bsd freebsd generic linux macosx mingw posix solaris)
LUA_PLAT=linux

RUBY=/usr/local/bin/ruby

AXFEM_HOME = /home/xubuntu/src/axfem
D2L_SCRIPTS = $(AXFEM_HOME)/script

endif

endif
