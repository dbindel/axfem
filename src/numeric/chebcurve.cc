#include "chebcurve.h"
#include <cstdio>

inline double len2(double x, double y)
{
    return x*x+y*y;
}

//ldoc
/**
# Methods

The `project` finds a parameter value $s$ near a given initial value
such that $\|f(s)-x\|^2$ is locally minimized.  This is done by
a guarded Newton iteration with line search.

 */
double ChebCurve::project(double x, double y, double s) const
{
    double xs = fx(s), dxs = dfx(s), d2xs = d2fx(s);
    double ys = fy(s), dys = dfy(s), d2ys = d2fy(s);
    
    for (int cnt = 0; cnt < 10; ++cnt) {

        // Distance to curve and first two derivatives
        double g   = len2(xs-x, ys-y);
        double dg  = 2*( (xs-x)*dxs + (ys-y)*dys );
        double d2g = 2*( dxs*dxs + dys*dys + (xs-x)*d2xs + (ys-y)*d2ys );
        double Hg  = std::abs(d2g);

        // Newton step + guard vs overstep
        double ds = -dg/Hg;
        if (s+ds >  1) ds =  1-s;
        if (s+ds < -1) ds = -1-s;

        // Quit if we're not going anywhere!
        if (std::abs(ds) < 1e-10) {
            s += ds;
            return s;
        }
        
        // Line search
        double sp = s, gp = g;
        for (int k = 0; k < 10 && g >= gp; ++k) {
            s = sp+ds;
            xs = fx(s);
            ys = fy(s);
            g = len2(xs-x, ys-y);
            ds /= 2;
        }
        
        // Re-evaluate derivatives
        dxs = dfx(s), d2xs = d2fx(s);
        dys = dfy(s), d2ys = d2fy(s);
    }
    return s;
}

/**
The `project` function requires a good initial guess for the parameter
value corresponding to the point nearest a given $(x,y)$.  We find the
closest interpolation node on the curve, and use that as the initial
guess.

*/
int ChebCurve::nearest_node(double x, double y) const
{
    int inear = 0;
    double dnear = len2(fx.f0()-x, fy.f0()-y);
    for (int i = 1; i < fx.getN(); ++i) {
        double di = len2(fx.fval(i)-x, fy.fval(i)-y);
        if (di < dnear) {
            dnear = di;
            inear = i;
        }
    }
    return inear;
}
