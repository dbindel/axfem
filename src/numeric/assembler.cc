#include "assembler.h"

//ldoc
/**
## Computing nonzero counts

The `get_nnz` function is essentially a lightweight version of the
`fill_csc` routine.  For each column, we iterate over the column
contributions in order to figure out the nonzero structure.  The
nonzero locations are marked in a `marks` array, and the indices of
the nonzeros are stored in a `col_ir` array.  The `col_ir` array would
be redundant, except that it allows us to clear the `marks` array in
time proportional to the number of nonzeros in the column (rather than
proportional to $m$).

*/
int Assembler::get_nnz()
{
    std::vector<int>  col_ir(m);  // Indices where nonzeros are stored
    std::vector<bool> marks(m);   // Nonzero locations in col_ir

    int nnz = 0;
    for (int j = 0; j < n; ++j) {

        // Count nonzeros and store their location
        int nnzcol = 0;
        for (ColRec* crec = cols[j]; crec != NULL; crec = crec->next) {
            int  nrows = crec->ir[0];
            int* rows  = crec->ir+1;
            double* v  = crec->pr;
            for (int ii = 0; ii < nrows; ++ii) {
                int row = rows[ii];
                if (row >= 0 && row < m && !marks[row] && 
                    (include_zeros || v[ii] != 0)) {
                    col_ir[nnzcol++] = row;
                    marks[row] = 1;
                }
            }
        }
        
        // Clear the locations where there are nonzeros
        for (int k = 0; k < nnzcol; ++k)
            marks[col_ir[k]] = 0;

        // Accumulate column nonzero count into global count
        nnz += nnzcol;
    }
    return nnz;
}


/**
## Generating the internal representation

The representation used internally by the assembler stores
contributions to each column in a linked list, where each node in the
list consists of a count, a pointer to an array of row indices
affected, and a pointer to a parallel array of contributions to go
into the matrix.  The row index arrays can be used across several
column contributions; for example, this usually happens when we add an
element matrix to the system.

Out-of-range indices are interpreted as contributions that should be
discarded.  This is useful for implementing Dirichlet boundary
conditions or submatrix extraction routines, for example.  In order
to not waste space, we only keep around the data for the valid indices.

The `count_active_rows` routine scans through an array of row indices
`iAe` (these can be zero-based or one-based depending on `index_base`),
and figures out how many of them are in-range (or "active").
*/

int Assembler::count_active_rows(const int* iAe, int mAe, int index_base)
{
    int mAe_active = 0;
    for (int ii = 0; ii < mAe; ++ii) {
        int i = iAe[ii]-index_base;
        if (i >= 0 && i < m)
            ++mAe_active;
    }
    return mAe_active;
}

/**
Once we figure out the number of active rows, we can allocate
a record to store them.  The record is simply an array of `int`;
the first entry is the number of active rows, and the remaining
entries list those rows.
*/

int* Assembler::add_row_rec(const int* iAe, int mAe, int index_base)
{
    int mAe_active = count_active_rows(iAe, mAe, index_base);
    if (mAe_active == 0)
        return NULL;
    int* isave = ipool.malloc(mAe_active+1);
    isave[0] = mAe_active;
    for (int ii = 0, k = 1; ii < mAe; ++ii) {
        int i = iAe[ii]-index_base;
        if (i >= 0 && i < m)
            isave[k++] = i;
    }
    return isave;
}

/**
The `add_col_rec` function adds a column contribution record to the list
for column `j`.  The active row record (`iAe_active`) can be re-used
across several columns, so we pass it in as an argument rather than
computing it here.  However, we still need the complete row list `iAe`
so that we can figure out the subset of the column data `Ae` that is
parallel to active rows.
*/

void Assembler::add_col_rec(const int* iAe, int j, const double* Ae, int mAe, 
                            int* iAe_active, int index_base)
{
    int mAe_active = iAe_active[0];
    ColRec* csave = cpool.malloc(1);
    double* dsave = dpool.malloc(mAe_active);
    csave->ir = iAe_active;
    csave->pr = dsave;
    csave->next = cols[j];
    cols[j] = csave;
    pre_nnz += mAe_active;
    for (int ii = 0; ii < mAe; ++ii) {
        int i = iAe[ii]-index_base;
        if (i >= 0 && i < m)
            *dsave++ = Ae[ii];
    }
}

/**
To add entries, we simply set up a record of the active rows, then
scan through the columns and add a column contribution record for each.
*/
void Assembler::add_entries(const int* iAe, const int* jAe, const double* Ae, 
                            int mAe, int nAe, int index_base)
{
    int* iAe_active = add_row_rec(iAe, mAe, index_base);
    if (!iAe_active)
        return;
    for (int jj = 0; jj < nAe; ++jj, Ae += mAe) {
        int j = jAe[jj]-index_base;
        if (j >= 0 && j < n)
            add_col_rec(iAe, j, Ae, mAe, iAe_active, index_base);
    }
}


/**
# `Reassembler` methods

The `Reassembler` class is simple in comparison to the `Assembler` class,
because it only ever uses one representation.  To add entries, we just find
the location in the `pr` array where they should go, and we add them.
Attempts to add to a location that is not in the index structure are simply
discarded.

*/
void Reassembler::add_entries(const int* iAe, const int* jAe, const double* Ae, 
                              int mAe, int nAe, int index_base)
{
    for (int jj = 0; jj < nAe; ++jj, Ae += mAe) {
        int j = jAe[jj]-index_base;
        int col_start = jc[j];
        int col_end   = jc[j+1];
        if (j >= 0 && j < n) {
            for (int ii = 0; ii < mAe; ++ii) {
                int i = iAe[ii]-index_base;
                int k = find_row(col_start, col_end, i);
                if (k >= 0)
                    pr[k] += Ae[ii];
            }
        }
    }
}

/**
We use binary search to locate the index for a row in the part of the
`ir` array associated with a particular column.  Note that this will
not work unless the row indices are sorted within each column!

*/
int Reassembler::find_row(int klo, int khi, int i)
{
    while (klo < khi) {
        int kmid = klo + (khi-klo)/2;
        int imid = ir[kmid];
        if (imid < i)
            klo = kmid+1;
        else if (imid > i)
            khi = kmid;
        else
            return kmid;
    }
    return -1;
}
