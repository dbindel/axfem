#include <vector>
#include <algorithm>
#include <cstdio>

#include "cscmatrix.h"

using namespace std;

//ldoc
/**
The `reinit` function can be used to allocate the actual storage space
in the case when it wasn't known at construction time.  This would
usually be used to do things like create arrays of `CSCMatrix`
objects.

 */
void CSCMatrix::reinit(unsigned m, unsigned n, unsigned nnz)
{
    this->m = m;
    this->n = n;
    jc.resize(n+1);
    ir.resize(nnz);
    pr.resize(nnz);
    jc[n] = nnz;
    fill(pr.begin(), pr.end(), 0);
}

/**
## Multiplication

The `vmult` function computes $y = Ax$.  The `Tvmult` function computes
$y = A^T x$.
*/

void CSCMatrix::vmult(double* result, const double* x) const
{
    vector<unsigned>::const_iterator iir = ir.begin();
    vector<double>::const_iterator   aij = pr.begin();
    fill(result, result+m, 0);
    for (unsigned j = 0; j < n; ++j) {
        double xj = x[j];
        for (unsigned ii = jc[j]; ii < jc[j+1]; ++ii) {
            unsigned i = *iir++;
            result[i] += (*aij++) * xj;
        }
    }
}

void CSCMatrix::Tvmult(double* result, const double* x) const
{
    vector<unsigned>::const_iterator iir = ir.begin();
    vector<double>::const_iterator   aij = pr.begin();
    fill(result, result+n, 0);
    for (unsigned j = 0; j < n; ++j) {
        double rj = 0;
        for (unsigned ii = jc[j]; ii < jc[j+1]; ++ii) {
            unsigned i = *iir++;
            rj += (*aij++) * x[i];
        }
        result[j] = rj;
    }
}
