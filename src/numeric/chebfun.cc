#include "chebfun.h"

//ldoc
/**
# Methods

## Evaluation

Evaluation is done using barycentric interpolation, which simplifies
somewhat for interpolation through Chebyshev nodes.  See Chapter 5 of
[Trefethen's book][ATAP].

[chebm]: http://people.maths.ox.ac.uk/trefethen/cheb.m

*/
double ChebFun::eval(double xx) const
{
    if (N < 0)
        return 0;
    
    double zd = xx-1;
    if (zd == 0)
        return f[0];
    double pnum = f[0]/zd/2;
    double pden = 1/zd/2;

    int s = -1;
    for (int j = 1; j < N; ++j) {
        zd = xx-xc(j);
        if (zd == 0)
            return f[j];
        pnum += s*f[j]/zd;
        pden += s/zd;
        s = -s;
    }

    zd = xx+1;
    if (zd == 0)
        return f[N];
    pnum += f[N]*s/zd/2;
    pden += s/zd/2;
    
    return pnum/pden;
}

/**
## Differentiation

The differentiation matrix is a more-or-less mechanical translation of
Nick Trefethen's [`cheb.m`][chebm] function.  It is not FFT-based, but
we're never going to use very high degree polynomials in this context.

[chebm]: http://people.maths.ox.ac.uk/trefethen/cheb.m

*/
ChebFun ChebFun::deriv() const
{
    if (N <= 0)
        return ChebFun();

    ChebFun result(N);
    int sN = ( (N % 2) == 0 ? 1 : -1 );

    double d00 = (2*N*N+1)/6.0;
    result.f[0] =  d00*f[0] + sN*f[N]/2;
    result.f[N] = -d00*f[N] - sN*f[0]/2;
    for (int j = 1, s = -1; j < N; ++j, s = -s) {
        double xj = xc(j);
        result.f[0] += 2*s/(1-xj)*f[j];
        result.f[N] -= 2*s/(1-xj)*f[N-j];
    }
    
    for (int i = 1, si = -1; i < N; ++i, si = -si) {
        double xi = xc(i);
        result.f[i] = si*( sN*f[N]/(1+xi)-f[0]/(1-xi) )/2;
        for (int j = 1, sij = -si; j < N; ++j, sij = -sij) {
            double xj = xc(j);
            if (i == j)
                result.f[i] -= f[i]*xi/2/(1-xi*xi);
            else
                result.f[i] += f[j]*sij/(xi-xj);
        }
    }

    return result;
}

/**
## Quadrature

The quadrature routine is standard Clenshaw-Curtis.  It is again
a straightforward translation of one of Trefethen's codes,
[`clencurt.m`][clencurt].

[clencurt]: http://people.maths.ox.ac.uk/trefethen/clencurt.m

*/
double ChebFun::integral() const
{
    if (N < 0)
        return 0;
    
    std::vector<double> w(N+1);
    double result = 0;
    if ( (N%2) == 0 ) {
        result += (f[0]+f[N])/(N*N-1);
        for (int i = 1; i < N; ++i) {
            double wi = 2*(1-cos(i*M_PI)/(N*N-1))/N;
            for (int k = 1; k < N/2; ++k)
                wi -= 4*cos(2*k*i*M_PI/N)/(4*k*k-1)/N;
            result += wi*f[i];
        }
    } else {
        result += (f[0]+f[N])/(N*N);
        for (int i = 1; i < N; ++i) {
            double wi = 2.0/N;
            for (int k = 1; k <= (N-1)/2; ++k)
                wi -= 4*cos(2*k*i*M_PI/N)/(4*k*k-1)/N;
            result += wi*f[i];
        }
    }
    return result;
}

/**
## Changing the degree

To change the degree of a `ChebFun`, we clone the current function,
then evaluate the current function at each point on a new Chebyshev
grid using `operator=`.

If the function was initialized with no information, changing the
degree just resizes the function value array without trying to
initialize it.

*/
void ChebFun::setN(int M)
{
    if (N == M)
        return;
    if (N <= 0) {
        N = M;
        f.resize(N+1);
        return;
    }
    ChebFun newf(M, *this);
    N = M;
    f = newf.f;
}
