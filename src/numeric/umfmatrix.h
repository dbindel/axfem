#ifndef UMFMATRIX_H
#define UMFMATRIX_H

extern "C" {
#include "umfpack.h"
}

#include "cscmatrix.h"

//ldoc
/**
% UMFPACK solver interface

## Class declaration

This class wraps the [UMFPACK][umfpack] sparse direct solver library.
For the most part, you should only need to know that `solve` and
`Tsolve` solve linear systems with $A$ and $A^T$, respectively. The
`factor` routine is automatically invoked if needed.

Note that when an `UMFMatrix` object is constructed from a
`CSCMatrix`, it does *not* make a copy of the `CSCMatrix` data
structures.  This means that `solve`, `Tsolve`, and `factor` must not
be called after the `CSCMatrix` used for construction has been
deleted.

[umfpack]:  http://www.cise.ufl.edu/research/sparse/umfpack/
 */
class UMFMatrix {
public:

    UMFMatrix(const CSCMatrix*);
    UMFMatrix(const UMFMatrix&);
    ~UMFMatrix();

    UMFMatrix& operator=(const UMFMatrix& matrix);

    int get_m() const   { return n;         }
    int get_n() const   { return n;         }
    int is_real() const { return (Az == 0); }

    /** Factor the matrix */
    int factor();

    /** Solve a linear system A*x = b. */
    int solve(double* xx, double* xz, 
              const double* bx, const double* bz);
    int solve(double* x, const double* b);
    int Tsolve(double* x, const double* b);

    double& umf_control(int i) { return Control[i]; }
    double& umf_info(int i)    { return Info[i];    }

 private:

    int n;              // Matrix dimension
    const int* jc;      // Column offset array for A
    const int* ir;      // Row indices for nonzeros in A
    const double* Ax;   // Real part of nonzeros in A
    const double* Az;   // Imaginary part of nonzeros in A

    void* Symbolic; /* UMFPACK structs */
    void* Numeric;
    double Control[UMFPACK_CONTROL];
    double Info[UMFPACK_INFO];

    void umfpack_defaults();
    void umfpack_free();
};

//ldoc
#endif /* UMFMATRIX_H */
