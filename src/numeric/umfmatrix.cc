extern "C" {
#include <umfpack.h>
}

#include <cstring>
#include <cstdio>
#include <cassert>
#include <vector>

#include "cscmatrix.h"
#include "umfmatrix.h"

using std::vector;

//ldoc
/**
# Methods

## Constructors, destructors, and assignment

We do not actually make a copy of the underlying matrix data structure
when we initialize the `UMFMatrix`, so bad things can happen if the
matrix data gets deleted while we're still doing `solve` operations
with this object.

*/
UMFMatrix::UMFMatrix(const CSCMatrix* cscmat) :
    n(cscmat->get_n()),
    jc((const int*) cscmat->get_jc()),
    ir((const int*) cscmat->get_ir()),
    Ax(cscmat->get_pr()),
    Az(NULL)
{
    umfpack_defaults();
}


UMFMatrix::UMFMatrix(const UMFMatrix& matrix) :
    n(matrix.n), 
    jc(matrix.jc), 
    ir(matrix.ir), 
    Ax(matrix.Ax), 
    Az(matrix.Az)
{
    umfpack_defaults();
}


UMFMatrix& UMFMatrix::operator=(const UMFMatrix& matrix)
{
    if (this == &matrix)
        return *this;
    umfpack_free();
    n = matrix.n;
    jc = matrix.jc;
    ir = matrix.ir;
    Ax = matrix.Ax;
    Az = matrix.Az;
    umfpack_defaults();
    return *this;
}


UMFMatrix::~UMFMatrix()
{
    umfpack_free();
}

/**
## Factorization

Factorization consists of two phasis: symbolic and numeric.  In
principle, we could re-use the symbolic factorization for multiple
numerical factorizations.  In practice, we don't currently do so.
*/
int UMFMatrix::factor()
{
    umfpack_free();
    int status;
    if (Az) {
        status = (umfpack_zi_symbolic(n, n, jc, ir, Ax, Az, 
                                      &Symbolic, Control, Info) != UMFPACK_OK ||
                  umfpack_zi_numeric(jc, ir, Ax, Az, Symbolic, 
                                     &Numeric, Control, Info) != UMFPACK_OK);
    } else {
        status = (umfpack_di_symbolic(n, n, jc, ir, Ax, 
                                      &Symbolic, Control, Info) != UMFPACK_OK ||
                  umfpack_di_numeric(jc, ir, Ax, Symbolic, 
                                     &Numeric, Control, Info) != UMFPACK_OK);
    }
    return status;
}


/**
## Solve functions

If $A$ is complex, we solve with a complex RHS using the `umfpack_zi_solve`
function.  But if $A$ is real, we solve with a complex RHS using two real
solves with `umfpack_di_solve`.

Note that the factorization is done automatically if `factor` was not
previously called.

*/
int UMFMatrix::solve(double* xx, double* xz, 
                     const double* bx, const double* bz)
{
    int status = 0;
    if (!Numeric)
        status = factor();

    if (Az) {
        status = (status || 
                  umfpack_zi_solve(UMFPACK_A, jc, ir, Ax, Az, xx, xz, bx, bz,
                                   Numeric, Control, Info)) != UMFPACK_OK;
    } else {
        status = (status ||
                  umfpack_di_solve(UMFPACK_A, jc, ir, Ax, xx, bx, 
                                   Numeric, Control, Info) != UMFPACK_OK ||
                  umfpack_di_solve(UMFPACK_A, jc, ir, Ax, xz, bz, 
                                   Numeric, Control, Info) != UMFPACK_OK);
    }
    return status;
}


int UMFMatrix::solve(double* x, const double* b)
{
    assert(Az == NULL);
    int status = 0;
    if (!Numeric)
        status = factor();

    status = (status || 
              umfpack_di_solve(UMFPACK_A, jc, ir, Ax, x, b, 
                               Numeric, Control, Info) != UMFPACK_OK);
    return status;
}


int UMFMatrix::Tsolve(double* x, const double* b)
{
    assert(Az == NULL);
    int status = 0;
    if (!Numeric)
        status = factor();

    status = (status || 
              umfpack_di_solve(UMFPACK_Aat, jc, ir, Ax, x, b, 
                               Numeric, Control, Info) != UMFPACK_OK);
    return status;
}


/**
## Default settings and freeing `UMFPACK` objects

See the [UMFPACK][umfpack] user's manual for more detail on these.

*/
void UMFMatrix::umfpack_defaults()
{
    if (Az)
        umfpack_zi_defaults(Control);
    else
        umfpack_di_defaults(Control);
    Numeric  = NULL;
    Symbolic = NULL;
}


void UMFMatrix::umfpack_free()
{
    if (Az) {
        if (Numeric)    umfpack_zi_free_numeric(&Numeric);
        if (Symbolic)   umfpack_zi_free_symbolic(&Symbolic);
    } else {
        if (Numeric)    umfpack_di_free_numeric(&Numeric);
        if (Symbolic)   umfpack_di_free_symbolic(&Symbolic);
    }
}
