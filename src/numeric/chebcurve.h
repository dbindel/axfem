#ifndef CHEBCURVE_H
#define CHEBCURVE_H

#include <algorithm>
#include "chebfun.h"

//ldoc
/**
% Chebyshev curve representations

A `ChebCurve` represents a function $f : [-1,1] \rightarrow
\mathbb{R}^2$ by a pair of `ChebFun` objects.  In addition, we keep
around representations for $f'$ and $f''$ in order to compute tangents
and projections onto the curve.

## Class declaration

*/
class ChebCurve {
public:
    ChebCurve(const ChebFun& fx_, const ChebFun& fy_) :
        fx(fx_), fy(fy_)
    {
        int N = std::max(fx.getN(), fy.getN());
        fx.setN(N);
        fy.setN(N);
        dfx  = fx.deriv();
        dfy  = fy.deriv();
        d2fx = dfx.deriv();
        d2fy = dfy.deriv();
    }

    void eval(double s, double* x, double* y) const {
        *x = fx(s);
        *y = fy(s);
    }

    void tangent(double s, double* x, double* y) const {
        *x = dfx(s);
        *y = dfy(s);
    }

    double project(double x, double y, double s0) const;
    double project(double x, double y) const{
        return project(x, y, fx.xc(nearest_node(x,y)));
    }

    template<class P>
        void eval(double s, P& p) const { eval(s, &(p[0]), &(p[1])); }
    template<class P>
        void tangent(double s, P& p) const { tangent(s, &(p[0]), &(p[1])); }
    template<class P>
        double project(const P& p) const { return project(p[0], p[1]); }
    template<class P>
        void projectp(P& p) const { eval(project(p), p); }

private:
    ChebFun fx,   fy;
    ChebFun dfx,  dfy;
    ChebFun d2fx, d2fy;
    int nearest_node(double x, double y) const;
};

//ldoc
#endif /* CHEBCURVE_H */
