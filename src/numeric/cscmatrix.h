#ifndef CSCMATRIX_H
#define CSCMATRIX_H

#include <vector>
#include <algorithm>

//ldoc
/**
% Compressed sparse column matrices

# Class declaration

[Compressed sparse column storage][templatecsc] (also known as
Harwell-Boeing or compressed column storage) is the default form used
by UMFPACK and MATLAB, among other packages.  It consists of three arrays:

- `pr`: the nonzero element values in column-major order
- `ir`: the row indices for the nonzero element values
- `jc`: an $n+1$-dimensional array of offsets of column starts in `pr` and `ir`

The last entry of the `jc` array is the number of nonzero entries.

[templatecsc]: http://www.netlib.org/linalg/html_templates/node92.html

*/
class CSCMatrix {
public:
    CSCMatrix() : m(0), n(0) {}
    
    CSCMatrix(unsigned m, unsigned n, unsigned nnz) :
        m(m), n(n), jc(n+1), ir(nnz), pr(nnz) {
        jc[n] = nnz;
    }

    template<class iiter_t, class diter_t>
    CSCMatrix(unsigned m, unsigned n, iiter_t ijc, iiter_t iir, diter_t ipr);
    
    void reinit(unsigned m, unsigned n, unsigned nnz);

    void vmult (double* result, const double* x) const;
    void Tvmult(double* result, const double* x) const;
    
    unsigned  get_m() const { return m; }
    unsigned  get_n() const { return n; }

    unsigned* get_jc() { return &(jc[0]); }
    unsigned* get_ir() { return &(ir[0]); }
    double*   get_pr() { return &(pr[0]); }

    const unsigned* get_jc() const { return &(jc[0]); }
    const unsigned* get_ir() const { return &(ir[0]); }
    const double*   get_pr() const { return &(pr[0]); }

private:
    unsigned m, n;              // Row and column count
    std::vector<unsigned> jc;   // Column offset list
    std::vector<unsigned> ir;   // Row indices of nonzeros
    std::vector<double> pr;     // Values of nonzeros
};

/**
# Methods

## Initialization

The constructor that gets data iterators is templatized so that
we can initialize from MATLAB (for example), which uses long
integers for column offset counts.

 */
template<class iiter_t, class diter_t>
CSCMatrix::CSCMatrix(unsigned m, unsigned n, 
                     iiter_t ijc, iiter_t iir, diter_t ipr) :
    m(m), n(n), jc(n+1), ir(ijc[n]), pr(ijc[n])
{
    unsigned nnz = ijc[n];
    std::copy(ijc, ijc+n+1, jc.begin());
    std::copy(iir, iir+nnz, ir.begin());
    std::copy(ipr, ipr+nnz, pr.begin());
}

//ldoc
#endif /* CSCMATRIX_H */
