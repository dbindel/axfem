#ifndef MVBUILDER_H
#define MVBUILDER_H

#include <vector>
#include "cscmatrix.h"
#include "assembler.h"

//ldoc
/**
% Block matrix-vector assemblers

The `VecBuilder` and `CSCBuilder` classes assembly block matrix and
vector expressions.  The basic operations are adding scalar multiples
of contributed pieces into a data structure to store the assembled data.

## Vector builder

*/
class VecBuilder {
public:
    VecBuilder(std::vector<double>& v) : result(v) {}

    void add(double x, unsigned i) {
        result[i] += x;
    }

    void add(const std::vector<double>& v, double c, unsigned i) {
        for (unsigned k = 0; k < v.size(); ++k, ++i)
            result[i] += c*v[k];
    }

private:
    std::vector<double>& result;
};

/**
## Sparse matrix builder interface

It may be worth integrating some of this into `Assembler`.

*/
class CSCBuilder {
public:
    CSCBuilder(int m, int n);

    void add_scalar(double x, unsigned i, unsigned j);
    void add_rvec(const std::vector<double>& v, 
                  double c, unsigned i, unsigned j);
    void add_cvec(const std::vector<double>& v, 
                  double c, unsigned i, unsigned j);
    void add_eye(unsigned n, double c, unsigned i, unsigned j);
    void add_matrix(const CSCMatrix& A, double c, unsigned i, unsigned j);
    void add_matrixT(const CSCMatrix& A, double c, unsigned i, unsigned j);

    CSCMatrix* get_matrix();
private:
    Assembler assembler;
};

//ldoc
#endif /* MVBUILDER_H */
