#include "mvbuilder.h"

#include <cstdio>
#include <algorithm>

using namespace std;

//ldoc
/**
## Sparse matrix assembler methods
*/

CSCBuilder::CSCBuilder(int m, int n) :
    assembler(m,n)
{
}

void CSCBuilder::add_scalar(double x, unsigned i, unsigned j)
{
    assembler.add_entry(i, j, x);
}

void CSCBuilder::add_rvec(const vector<double>& v, 
                          double c, unsigned i, unsigned j)
{
    for (vector<double>::const_iterator vi = v.begin(); vi != v.end(); ++vi)
        assembler.add_entry(i, j++, c* (*vi));
}

void CSCBuilder::add_cvec(const vector<double>& v, double c,
                          unsigned i, unsigned j)
{
    for (vector<double>::const_iterator vi = v.begin(); vi != v.end(); ++vi)
        assembler.add_entry(i++, j, c* (*vi));
}

void CSCBuilder::add_eye(unsigned n, double c, unsigned i, unsigned j)
{
    for (unsigned k = 0; k < n; ++k) 
        assembler.add_entry(i+k, j+k, c);
}

void CSCBuilder::add_matrix(const CSCMatrix& A, 
                            double c, unsigned i, unsigned j)
{
    unsigned n = A.get_n();
    const unsigned* jc = A.get_jc();
    const unsigned* ir = A.get_ir();
    const double*   pr = A.get_pr();
    for (unsigned k = 0; k < n; ++k) {
        for (unsigned l = jc[k]; l < jc[k+1]; ++l) {
            unsigned ii = i+ir[l];
            unsigned jj = j+k;
            assembler.add_entry(ii, jj, c*pr[l]);
        }
    }
}

void CSCBuilder::add_matrixT(const CSCMatrix& A, 
                             double c, unsigned i, unsigned j)
{
    unsigned n   = A.get_n();
    const unsigned* jc = A.get_jc();
    const unsigned* ir = A.get_ir();
    const double*   pr = A.get_pr();
    for (unsigned k = 0; k < n; ++k) {
        for (unsigned l = jc[k]; l < jc[k+1]; ++l) {
            unsigned ii = i+k;
            unsigned jj = j+ir[l];
            assembler.add_entry(ii, jj, c*pr[l]);
        }
    }
}

CSCMatrix* CSCBuilder::get_matrix()
{
    return assembler.get_matrix();
}
