#ifndef CHEBFUN_H
#define CHEBFUN_H

#include <cmath>
#include <vector>
#include <cstdio>

//ldoc
/**
% Chebyshev function approximations

The `ChebFun` class approximates a smooth function
$f : [-1,1] \rightarrow \mathbb{R}$ by a polynomial interpolation
through a set of $N+1$ Chebyshev nodes.  `ChebFun`s may be added,
subtracted, or multiplied node-by-node.

The inspiration for this class came from the 
[MATLAB ChebFun package][chebfun], which is beautifully
documented in the book 
[Approximation Theory and Approximation Practice][ATAP].
Needless to say, the C++ implementation has only a fraction of
the functionality of the MATLAB version.

[chebfun]: http://www2.maths.ox.ac.uk/chebfun/
[ATAP]:    http://www2.maths.ox.ac.uk/chebfun/ATAP/

# Class interface

*/
class ChebFun {
public:
    ChebFun() : N(0), f(1) { f[0] = 0; }

    ChebFun(int N) : N(N), f(N+1) {}

    template <class F>
    ChebFun(int N, F ff) : N(N), f(N+1) {
        for (int i = 0; i <= N; ++i)
            f[i] = ff(xc(i));
    }

    template <class F>
    ChebFun& operator=(F ff) {
        for (int i = 0; i <= N; ++i) 
            f[i] = ff(xc(i));
        return *this;
    }
    
    template <class F>
    ChebFun operator*(F ff) const {
        ChebFun result = *this;
        for (int i = 0; i <= N; ++i)
            result.f[i] *= ff(xc(i));
        return result;
    }

    template <class F>
    ChebFun operator-(F ff) const {
        ChebFun result = *this;
        for (int i = 0; i <= N; ++i)
            result.f[i] -= ff(xc(i));
        return result;
    }

    template <class F>
    ChebFun operator+(F ff) const {
        ChebFun result = *this;
        for (int i = 0; i <= N; ++i)
            result.f[i] += ff(xc(i));
        return result;
    }

    template <class F>
    ChebFun operator-() const {
        ChebFun result = *this;
        for (int i = 0; i <= N; ++i)
            result.f[i] = -f[i];
        return result;
    }

    double operator()(double xx) const { return eval(xx); }

    double eval(double xx) const;
    ChebFun deriv() const;
    double integral() const;

    double  f0() const { return f[0]; }
    double  fN() const { return f[N]; }

    void    setN(int);
    int     getN() const      { return N;                 }
    double  fval(int i) const { return f[i];              }
    double& fval(int i)       { return f[i];              }
    double  xc(int i) const   { return cos( (M_PI*i)/N ); }
    
private:
    int N;                  // Chebyshev mesh intervals
    std::vector<double> f;  // Function values at Chebyshev grid
};    

//ldoc
#endif /* CHEBFUN_H */
