#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include <vector>
#include <algorithm>
#include "block_alloc.h"
#include "cscmatrix.h"

//ldoc
/**
% Matrix assembly classes

[Compressed sparse column storage][templatecsc] (also known as
Harwell-Boeing or compressed column storage) is the default form used
by UMFPACK and MATLAB, among other packages.  It consists of three arrays:

- `pr`: the nonzero element values in column-major order
- `ir`: the row indices for the nonzero element values
- `jc`: an $n+1$-dimensional array of offsets of column starts in `pr` and `ir`

The last entry of the `jc` array is the number of nonzero entries.

While compressed sparse column storage is convenient for matrix-vector
multiplication and for solvers, it is generally less convenient for matrix
assembly, unless the structure of the matrix is known in advance.
Therefore, we provide an `Assembler` class that maintains an alternate
internal representation of the matrix that can be converted to
compressed sparse column form, and a `Reassembler` class that helps
assemble a new compressed sparse column matrix assuming that the nonzero
structure is known a priori.

[templatecsc]: http://www.netlib.org/linalg/html_templates/node92.html


# `Assembler` and `Reassembler` class declarations

The `Assembler` class accumulates element contributions to be turned
into a sparse matrix in compressed sparse column format.  The assembly
consists of two phases: accumulation of element matrices, and
compression into the final format.

In the accumulation phase, we keep linked lists of data that will go
into each column.  Each entry in a column's linked list corresponds to
one column of an element matrix.  The entry has two pointers: one to a
count of the number of entries and a list of affected rows, and
another to the actual data.  The row count and list is shared across
all the columns of a single element matrix, which cuts down somewhat
on the memory overhead for storing indices.  Memory is allocated from
a fast pool-type allocator.

In the compression phase, we merge duplicates in each column.  The
routine to compress the data into CSC format takes pre-allocated index
and data arrays as input.  To figure out the number of nonzero entries
that should be allocated in this compressed data structure, one can
either take a conservative estimate (e.g. the total number of
nonzeroes including duplicates, as returned by `get_pre_nnz`), or one
can get the exact nonzero count using a "lightweight" version of the
compression algorithm described above (as implemented by `get_nnz`).

*/
class Assembler {
public:
    Assembler(int m, int n, bool include_zeros = true) : 
        m(m), n(n), pre_nnz(0), include_zeros(include_zeros),
        cols(n) {}

    int get_m() const { return m; }
    int get_n() const { return n; }

    void add_entries(const int* i, const int* j, const double* A, int m, int n,
                     int index_base = 0);
    void add_entry(int i, int j, double aij, int index_base = 0) {
        add_entries(&i, &j, &aij, 1, 1, index_base);
    }

    int get_nnz();
    int get_pre_nnz() const { return pre_nnz; }

    template<class intT> 
    void fill_csc(intT* jc, intT* ir, double* pr);

    template<class intT> 
    void refill_csc(intT* jc, intT* ir, double* pr);

    void fill_csc(CSCMatrix& A) {
        A.reinit(m, n, get_nnz());
        fill_csc(A.get_jc(), A.get_ir(), A.get_pr());
    }

    CSCMatrix* get_matrix() {
        CSCMatrix* result = new CSCMatrix(m, n, get_nnz());
        fill_csc(result->get_jc(), result->get_ir(), result->get_pr());
        return result;
    }
    
private:
    struct ColRec {      // Record of one column of an element matrix: 
        int*    ir;      //  Row index data 
        double* pr;      //  Column data    
        ColRec* next;    //  List link      
    };

    int m, n;            // Matrix dimensions
    int pre_nnz;         // Number of nonzeros, including duplicates
    bool include_zeros;  // Copy element zeros into matrix?

    std::vector<ColRec*> cols;  // Column lists
    Arena<ColRec> cpool;        // Storage arena for ColRecs
    Arena<int>    ipool;        // Storage for row index data
    Arena<double> dpool;        // Storage for element entry data
    
    int count_active_rows(const int* iAe, int mAe, int index_base);
    int* add_row_rec(const int* iAe, int mAe, int index_base);
    void add_col_rec(const int* iAe, int j, const double* Ae, int mAe, 
                     int* iAe_active, int index_base);
};

/**
The `Reassembler` class accumulates element contributions into an
existing compressed sparse column matrix data structure.  Elements
that don't fit into the existing index structure are silently ignored.

*/
class Reassembler {
public:
    template<class intT>
    Reassembler(const intT* jc1, const intT* ir1, int m, int n) :
        m(m), n(n), jc(n+1), ir(jc1[n]), pr(jc1[n]) {
        std::copy(jc1, jc1+n+1,   jc.begin());
        std::copy(ir1, ir1+jc[n], ir.begin());
        clear();
    }

    void clear()        { std::fill(pr.begin(), pr.end(), 0); }

    int get_m() const   { return m;     }
    int get_n() const   { return n;     }
    int get_nnz() const { return jc[n]; }

    void add_entries(const int* i, const int* j, const double* A, 
                     int m, int n, int index_base = 0);

    template<class intT>
    void fill_csc(intT* jc_out, intT* ir_out, double* pr_out) {
        std::copy(jc.begin(), jc.end(), jc_out);
        std::copy(ir.begin(), ir.end(), ir_out);
        std::copy(pr.begin(), pr.end(), pr_out);
    }

private:
    int m, n;                 // Matrix dimensions
    std::vector<int> jc;      // Local storage for column counts
    std::vector<int> ir;      // Local storage for row indices
    std::vector<double> pr;   // Local storage for entry data

    int find_row(int klo, int khi, int i);
};

/**
# `Assembler` methods

## Generating the CSC representation

The `fill_csc` function builds a compressed representation one column
at a time by accumulating all the contributions for a column into a
*dense* vector.  At the same time, we build a list of positions
(without repeats) of nonzeros in the accumulation vector.  As we go,
we keep track of whether we have seen a nonzero before by keeping a
bit vector with a mark for each known nonzero location.  After
processing all the data for the column, we sort the indices that
correspond to nonzero positions and copy in the corresponding data
from the accumulation vector.  Finally, we zero out the nonzero
entries in the bit vector and the accumulation vector.

We assume there is sufficient storage preallocated in `ir` and `pr`.

*/
template<class intT>
void Assembler::fill_csc(intT* jc, intT* ir, double* pr)
{
    std::vector<double> col_pr(m);  // Dense column representation
    std::vector<bool>   marks(m);   // Nonzero locations in col_pr

    jc[0] = 0;
    for (int j = 0; j < n; ++j) {

        // Form dense column j, recording nonzero locations in ir.
        int nnzcol = 0;
        for (ColRec* crec = cols[j]; crec != NULL; crec = crec->next) {
            int  nrows = crec->ir[0];
            int* rows  = crec->ir+1;
            double* v  = crec->pr;
            for (int ii = 0; ii < nrows; ++ii) {
                int row = rows[ii];
                if (row >= 0 && row < m && 
                    (include_zeros || v[ii] != 0)) {
                    col_pr[row] += v[ii];
                    if (!marks[row]) {
                        marks[row] = 1;
                        ir[nnzcol++] = row;
                    }
                }
            }
        }

        // Sort the list of nonzero locations
        std::sort(ir, ir+nnzcol);
        for (int ii = 0; ii < nnzcol; ++ii) {
            int i     = ir[ii];
            pr[ii]    = col_pr[i];
            col_pr[i] = 0; 
            marks[i]  = 0;
        }
        
        // Move pointers to next column and update jc
        ir += nnzcol;
        pr += nnzcol;
        jc[j+1] = jc[j]+nnzcol;

    }
}

/**
The `refill_csc` function is similar to `fill_csc`, except that we
assume that `ir` and `jc` are preallocated, and that all contributions
go to positions in the preallocated index structure.  Therefore, we do
not need to build the index structure, as we go.

*Note*: It is an unchecked error to add to locations outside the
pre-defined index space when using `refill_csc`.  If out-of-bounds
contributions are made, not only might they not show up where intended;
they might show up some other place entirely!

*/
template<class intT>
void Assembler::refill_csc(intT* jc, intT* ir, double* pr)
{
    std::vector<double> col_pr(m);
    for (int j = 0; j < n; ++j) {
        for (ColRec* crec = cols[j]; crec != NULL; crec = crec->next) {
            int  nrows = crec->ir[0];
            int* rows  = crec->ir+1;
            double* v  = crec->pr;
            for (int ii = 0; ii < nrows; ++ii) {
                int row = rows[ii];
                if (row >= 0 && row < m && 
                    (include_zeros || v[ii] != 0)) {
                    col_pr[row] += v[ii];
                }
            }
        }
        int jstart = jc[j];
        int jend   = jc[j+1];
        for (int ii = jstart; ii < jend; ++ii) {
            int i     = ir[ii];
            pr[ii]    = col_pr[i];
            col_pr[i] = 0; 
        }
    }
}

//ldoc
#endif /* ASSEMBLER_H */
