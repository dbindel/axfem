#ifndef CHEBBOUNDARY_H
#define CHEBBOUNDARY_H

#include <base/point.h>
#include <grid/tria.h>
#include <grid/tria_boundary.h>

#include "chebcurve.h"

//ldoc
/**
% Chebyshev boundary curves

[Boundary objects][d2b] are used by the [deal.ii library][d2] to
represent the boundary of a domain for tasks like mesh refinement or
the placement of mid-side nodes in higher-order isoparametric
elements.  Given points on a boundary, the basic task of the boundary
object is to produce additional intermediate boundary points.  While
deal.ii provides implementations for some default boundaries (straight
boundaries, cylinders, etc), is is up to users to provide
implementations for more complicated boundaries.  We do this using
[Chebyshev interpolants of boundary curves][chebcurve], which can be
highly accurate for smooth boundaries.

[d2]:  http://www.dealii.org/
[d2b]: http://www.dealii.org/developer/doxygen/deal.II/group__boundary.html
[chebcurve]:  ../numeric/chebcurve.html


# Class interface

The only method that is not an overloaded method from the [boundary
base class][d2b] is `is_point_on_boundary`, which is used in the code
to set up boundary conditions (currently a funny hybrid of deal.ii and
HiQLab conventions).  This should be refactored at some point.

*/
class ChebBoundary : public dealii::StraightBoundary<2> {
public:
    typedef dealii::Point<2>         P2;
    typedef dealii::Triangulation<2> T2;
    typedef dealii::Boundary<2>      B2;
        
    ChebBoundary(const ChebCurve& curve) : curve(curve) {}
    virtual ~ChebBoundary() {}

    P2 get_new_point_on_line(const T2::line_iterator& line) const; 
    
    void get_intermediate_points_on_line(const T2::line_iterator& line,
                                        std::vector<P2>& points) const;

    bool is_point_on_boundary(const P2& p, double dmax) const;

private:
    ChebCurve curve;
};

//ldoc
#endif /* CHEBBOUNDARY_H */
