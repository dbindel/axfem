#include "fesvalues.h"

using namespace std;
using namespace dealii;

typedef DoFHandler<2>::active_cell_iterator aciter_t;
typedef Point<2> P2;

//ldoc
/**
## Method definition

The `FESValues` function only has one interesting method, `reinit`.
This updates the `fe_values` array for a cell, and computes the
sensitivities at the quadrature points.  The `Xp` argument gives the
sensitivities of the nodal coordinates; these are interpolated to
get the quadrature point sensitivities and associated sensitivity of
the weights and the gradients.

*/
void FESValues::reinit(aciter_t& cell, P2* Xp)
{
    unsigned ldofs = fe_values.dofs_per_cell;
    unsigned nqpts = fe_values.n_quadrature_points;

    fe_values.reinit(cell);
    for (unsigned q = 0; q < nqpts; ++q) {

        rp[q] = 0;
        for (unsigned k= 0; k<ldofs; ++k)
            rp[q] += Xp[k](0)*fe_values.shape_value(k,q);

        double A[4] = {0, 0, 0, 0};
        for (unsigned k = 0; k < ldofs; ++k) {
            Tensor<1,2> Gek = fe_values.shape_grad(k,q);
            A[0] -= Gek[0]*Xp[k](0);
            A[1] -= Gek[1]*Xp[k](0);
            A[2] -= Gek[0]*Xp[k](1);
            A[3] -= Gek[1]*Xp[k](1);
        }
        double r  = fe_values.quadrature_point(q)(0);
        double tr = -(A[0]+A[3]);
        dwt[q]    = (rp[q] + r*tr)*fe_values.JxW(q);

        for (unsigned k = 0; k < ldofs; ++k) {
            Tensor<1,2> Gek = fe_values.shape_grad(k,q);
            Gp[q*ldofs+k][0] = A[0]*Gek[0] + A[2]*Gek[1];
            Gp[q*ldofs+k][1] = A[1]*Gek[0] + A[3]*Gek[1];
        }
    }
}

