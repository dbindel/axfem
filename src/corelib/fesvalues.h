#ifndef FESVALUES_H
#define FESVALUES_H

#include <fe/fe_values.h>
#include <dofs/dof_tools.h>
#include <vector>

//ldoc
/**
# Finite element shape sensitivities

The [FEValues][fev] class in [deal.ii][d2] allows the evaluation of shape
functions and their gradients at quadrature points.  It essentially acts
as a cursor that can be set up once, then reinitialized for each element
in the mesh.  The `FESValues` class defined below extends `FEValues` with
information about the sensitivities of the shape functions, gradients,
and point positions to some geometric perturbation.

[fev]: http://www.dealii.org/developer/doxygen/deal.II/classFEValuesBase.html

## Class definition

In addition to the `fe_values` information, we compute the sensitivities of
the radial coordinate at quadrature points, the sensitivities of the weights,
and the sensitivities of the gradients.  We keep this information in public
variables, which is consistent with how [FEValues][fev] works.

*/
class FESValues {
public:
    FESValues(dealii::FEValues<2>& fe_values) :
        fe_values(fe_values),
        rp(fe_values.n_quadrature_points),
        dwt(fe_values.n_quadrature_points),
        Gp(fe_values.dofs_per_cell * fe_values.n_quadrature_points) {}

    void reinit(dealii::DoFHandler<2>::active_cell_iterator& cell, 
                dealii::Point<2>* Xp);

    dealii::FEValues<2>& fe_values;          // Values from deal.ii
    std::vector<double> rp;                  // Derivative of r coords
    std::vector<double> dwt;                 // Derivative of quadrature wts
    std::vector< dealii::Tensor<1,2> > Gp;   // Derivative of gradients
};

//ldoc
#endif /* FESVALUES_H */
