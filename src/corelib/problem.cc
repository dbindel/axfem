#include <fe/fe_values.h>
#include <fe/fe_tools.h>
#include <dofs/dof_tools.h>
#include <grid/grid_out.h>
#include <numerics/vectors.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>

#include "problem.h"
#include "material.h"
#include "fesvalues.h"
#include "mvbuilder.h"

using namespace dealii;
using namespace std;

typedef DoFHandler<2>::active_cell_iterator aciter_t;

//ldoc
/**
# Methods

## Constructor and destructor
*/

Problem::Problem(int azimuthal_number, unsigned refinement, unsigned lagp) :
    lagp(lagp), refinement(refinement),
    azimuthal_number(azimuthal_number),
    fe(lagp), mapping(lagp), dh(tria)
{
}

Problem::~Problem()
{
    typedef vector<ChebBoundary*>::iterator biter_t;
    for (biter_t i = chebBoundaries.begin(); i != chebBoundaries.end(); ++i)
        delete *i;
}


/**
## Mesh initialization

The `mesh_initialize` populates a mesh from an array of nodal
coordinates (`x0 y0 x1 y1 ...`), an array of cell information records
('mat0 bl0 br0 tl0 tr01 mat1 ...`), and an array of boundary
specifiers.

 */
void Problem::mesh_initialize(unsigned nvertices,   const double* vertices,
                              unsigned ncells,      const double* cells, 
                              unsigned nboundaries, ChebBoundary** boundaries)
{
    vector< Point<2> > vertices_list(nvertices);
    vector< CellData<2> > cells_list(ncells);

    for (unsigned i = 0; i < nvertices; ++i) {
        vertices_list[i][0] = vertices[2*i+0];
        vertices_list[i][1] = vertices[2*i+1];
    }

    for (unsigned i = 0; i < ncells; ++i) {
        cells_list[i].material_id = cells[5*i+0];
        cells_list[i].vertices[0] = cells[5*i+1];
        cells_list[i].vertices[1] = cells[5*i+2];
        cells_list[i].vertices[2] = cells[5*i+3];
        cells_list[i].vertices[3] = cells[5*i+4];
    }

    tria.create_triangulation(vertices_list, cells_list, SubCellData());

    for (unsigned i = 0; i < nboundaries; ++i)
        add_chebboundary(*boundaries[i]);

    tria.refine_global(refinement);
    dh.distribute_dofs(fe);
}


/**
## deal.ii mesh output functions
 */

void Problem::write_msh(const char* fname)
{
    GridOut grid_out;
    ofstream os(fname);
    grid_out.write_msh(tria,os);
}

void Problem::write_eps(const char* fname)
{
    GridOut grid_out;
    ofstream os(fname);
    grid_out.write_eps(tria,os);
}

/**
## SVG output functions

There are two modes for SVG output.  In the first mode, we just show the mesh,
using a lovely green-and-purple color scheme.  In the second mode, we show the
mesh colored (grey scale) according to some displacement field, along with a 
color scale bar.  The first mode corresponds to setting `evaluator` to `NULL`.

We have to enter the effective ranges used for each of the coordinates
(and the value, if it is used).  This can be obtained using
`get_bbox`.
*/

void Problem::write_svg(const char* fname, 
                        double xmin, double xmax, 
                        double ymin, double ymax,
                        double vmin, double vmax,
                        CellValue* evaluator)
{
    QGauss<2> qf(lagp+1); // just to make fe_values happy
    FEValues<2> fev(mapping, fe, qf, update_support_points | update_values);
    const vector<Point<2> > fesp = fe.get_unit_support_points();
    int scale = (int) (500.0/std::max(xmax-xmin, ymax-ymin));

    const int npts = 8;
    const double pts[2*npts] = {0,0, 0.5,0, 1,0, 1,0.5, 
                                1,1, 0.5,1, 0,1, 0,0.5};
    typedef DoFHandler<2>::active_cell_iterator citer;

    FILE* fp = fopen(fname, "w+");
    fprintf(fp, "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\n");
    fprintf(fp, "<rect width=\"%d\" height=\"%d\" "
            "style=\"fill:white;stroke:black;stroke-width:2\"/>\n",
            (int) ((xmax-xmin)*scale), 
            (int) ((ymax-ymin)*scale));
    if (evaluator) {
        fprintf(fp, "<defs>\n"
                "<linearGradient id=\"grad1\" "
                "y1=\"0%%\" y2=\"100%%\" x1=\"0%%\" x2=\"0%%\">\n"
                "<stop offset=\"0%%\" style=\"stop-color:rgb(20,20,20)\"/>\n"
                "<stop offset=\"100%%\" style=\"stop-color:rgb(200,200,200)\"/>\n"
                "</linearGradient>\n"
                "</defs>\n");
        fprintf(fp, "<rect x=\"%d\" y=\"10\" width=\"%d\" height=\"%d\" "
                "style=\"stroke:black;stroke-width:0.1\" "
                "fill=\"url(#grad1)\" />\n",
                (int) ((xmax-xmin)*scale)+10, 
                20,
                (int) ((ymax-ymin)*scale)-20);
        fprintf(fp, "<text x=\"%d\" y=\"15\" fill=\"black\">%g</text>",
                (int) ((xmax-xmin)*scale)+35, vmax);
        fprintf(fp, "<text x=\"%d\" y=\"%d\" fill=\"black\">%g</text>",
                (int) ((xmax-xmin)*scale)+35, 
                (int) ((ymax-ymin)*scale)-5,
                vmin);
    }
            
    for (citer cell = dh.begin_active(); cell != dh.end(); ++cell) {
        fev.reinit(cell);
        fprintf(fp, "<polygon points=\"");
        for (unsigned k = 0; k < npts; ++k) {
            Point<2> p0(pts[2*k+0], pts[2*k+1]);
            Point<2> p = mapping.transform_unit_to_real_cell(cell,p0);
            fprintf(fp, "%f,%f ", 
                    (p(0)-xmin)*scale, 
                    (ymax-p(1))*scale);
        }
        if (evaluator) {
            double v = evaluator->value(cell, 0.5, 0.5);
            v = (v-vmin)/(vmax-vmin);
            int s = (int) (200-v*180);
            fprintf(fp, "\"\nstyle=\"fill:rgb(%d,%d,%d);stroke:purple;"
                    "stroke-width:0\"/>\n", s, s, s);
        } else {
            fprintf(fp, "\"\nstyle=\"fill:lime;stroke:purple;"
                    "stroke-width:0.2\"/>\n");
        }
    }
    fprintf(fp, "</svg>\n");
    fclose(fp);
}

void Problem::get_bbox(double& xmin, double& xmax, 
                       double& ymin, double& ymax,
                       double& vmin, double& vmax,
                       CellValue* evaluator)
{
    QGauss<2> qf(lagp+1); // just to make fe_values happy
    FEValues<2> fev(mapping, fe, qf, update_support_points | update_values);
    const vector<Point<2> > fesp = fe.get_unit_support_points();

    const int npts = 8;
    const double pts[2*npts] = {0,0, 0.5,0, 1,0, 1,0.5, 
                                1,1, 0.5,1, 0,1, 0,0.5};
    typedef DoFHandler<2>::active_cell_iterator citer;

    xmax = -1e8, xmin = 1e8;
    ymax = -1e8, ymin = 1e8;
    vmax = -1e8, vmin = 1e8;
    for (citer cell = dh.begin_active(); cell != dh.end(); ++cell) {
        fev.reinit(cell);
        for (unsigned k = 0; k < npts; ++k) {
            Point<2> p0(pts[2*k+0], pts[2*k+1]);
            Point<2> p = mapping.transform_unit_to_real_cell(cell,p0);
            xmin = std::min(xmin, p(0));
            xmax = std::max(xmax, p(0));
            ymin = std::min(ymin, p(1));
            ymax = std::max(ymax, p(1));
            if (evaluator) {
                double v = evaluator->value(cell, p0(0), p0(1));
                vmin = std::min(vmin, v);
                vmax = std::max(vmax, v);
            }
        }
    }
}

/**
# Writing Data in UCD File Format

*/
void Problem::write_inp(const char* fname,
                        double xmin, double xmax, 
                        double ymin, double ymax,
                        double vmin, double vmax,
                        CellValue* evaluator,
                        unsigned int nslices,
                        unsigned int which)
{
   typedef DoFHandler<2>::active_cell_iterator citer;
   typedef unsigned int uint;
   typedef std::vector<unsigned int> uintv;

   std::ofstream out (fname); 

   uint m = get_azimuthal_number();
   uint n2dc = tria.n_active_cells();
   uint n2dv = tria.n_used_vertices();  
   uint n3dv = n2dv * nslices; 
   uint n3dc = n2dc * nslices;

   out << n3dv << " " << n3dc << " 1 0 0" << std::endl; 

   std::vector<double> ndata (n3dv,0);

   uint nid = 0;
   uintv to3d (n3dv,0);

 
   for (uint s = 0; s < nslices; ++s) {
      double t = s *  (2*M_PI/nslices);
      double trig = cos(m*t);
      if (which == 1) trig = sin(m*t); 

      std::set<uint> included;
      std::set<uint>::iterator set_iter;

   for (citer cell = dh.begin_active(); cell != dh.end(); ++cell) {
       // deal.ii provides
       const std::vector<Point<2> > all_v2d = tria.get_vertices();
       const std::vector<bool> used_v2d = tria.get_used_vertices();

       std::vector<Point<2> > v2d;
       for (uint idx = 0; idx < 4; ++idx) {
           Point<2> pt = cell->vertex(idx);
           uint key = cell->vertex_index(idx);

           if(included.find(key) == included.end()) {
              included.insert(key);
              to3d[n2dv*s+key] = nid;
              out << nid << " "
                  << pt(0) * cos(t) << " "
                  << pt(0) * sin(t) << " "
                  << pt(1) << std::endl;
              switch (idx) {
                  case 0: 
                     ndata[nid] = evaluator->value(cell,0,0)*trig; break;
                  case 1: 
                     ndata[nid] = evaluator->value(cell,1,0)*trig; break;
                  case 2: 
                     ndata[nid] = evaluator->value(cell,1,1)*trig; break;
                  case 3: 
                     ndata[nid] = evaluator->value(cell,0,1)*trig; break;
                  default:
                     ndata[nid] = 0;
              }
             
              nid++; 
           } // if a new node 
       } // end of vertices
   } // end of 2d cells
   } // end of slices


   // Write to file
   uint cid = 0;
   for (uint s = 0; s < nslices; ++s) {
     for (citer cell = dh.begin_active(); cell != dh.end(); ++cell) {
         out << cid << " 0 hex";
         std::vector<uint> current, next;
         for (uint idx = 0; idx < 4; ++idx) {
            current.push_back(n2dv*s+cell->vertex_index(idx));
            next.push_back((s<nslices-1 ? n2dv*(s+1):0) +
                             cell->vertex_index(idx));

         } // vertices

           out << " " << to3d[current[0]]   
               << " " << to3d[current[1]]   
               << " " << to3d[current[3]]   
               << " " << to3d[current[2]]   
               << " " << to3d[   next[0]]   
               << " " << to3d[   next[1]]   
               << " " << to3d[   next[3]]   
               << " " << to3d[   next[2]] << std::endl;
           cid++; 
      }  // cells
   } // slices

   out << "1 1" << std::endl;
   out << "field, dimensionless" << std::endl;
   for (nid = 0; nid < ndata.size(); ++nid)
       out << nid << " " << ndata[nid] << std::endl;
   out.close();
}
 

/**
## Mesh differentiation
 */

ArrayP2* Problem::mesh_deriv(Problem& pp, Problem& pm, double h)
{
    QGauss<2> qf(lagp+1); // just to make fe_values happy
    FEValues<2> fevp(pp.mapping, pp.fe, qf, update_support_points);
    FEValues<2> fevm(pm.mapping, pm.fe, qf, update_support_points);
    const vector<Point<2> > fesp = pp.fe.get_unit_support_points();
    const vector<Point<2> > fesm = pm.fe.get_unit_support_points();

    ArrayP2* result = new ArrayP2();
    typedef DoFHandler<2>::active_cell_iterator citer;
    for (citer cellp = pp.dh.begin_active(), cellm = pm.dh.begin_active(); 
         cellp != pp.dh.end() && cellm != pm.dh.end(); ++cellp, ++cellm) {
        fevp.reinit(cellp);
        fevm.reinit(cellm);
        for (unsigned k = 0; k < fesp.size(); ++k) {
            Point<2> p1 = pm.mapping.transform_unit_to_real_cell(cellm,fesp[k]);
            Point<2> p2 = pp.mapping.transform_unit_to_real_cell(cellp,fesm[k]);
            Point<2> d = (p2-p1)/(2*h);
            result->push_back(d);
        }
    }
    return result;
}

/**
## Matrix assembly
*/

void Problem::assemble_allg(ProblemMatrices& p)
{
    unsigned nt = t_dof_count;
    unsigned nu = u_dof_count;
    Assembler Mtt(nt,nt);
    Assembler Ktt(nt,nt);
    Assembler Kut(nu,nt);
    Assembler Muu(nu,nu);
    Assembler Kuu(nu,nu);
    Assembler Buu(nu,nu);

    QGauss<2> qf(lagp+1);  
    FEValues<2> fe_values(mapping, fe, qf, update_values |
                          update_gradients | update_jacobians |
                          update_quadrature_points | update_JxW_values );
    
    const unsigned ldofs = fe.dofs_per_cell;
    const unsigned lda = 4*ldofs;
    vector<double> Ae(lda*lda);
    vector<unsigned> ldofidx(ldofs);

    vector<int> uidx(lda);
    vector<int> tidx(lda);
    fill(uidx.begin(), uidx.end(), -1);
    fill(tidx.begin(), tidx.end(), -1);
    
    for (aciter_t cell = dh.begin_active(); cell != dh.end(); ++cell) {
        fe_values.reinit(cell);
        Material& m = materials[cell->material_id()];
        cell->get_dof_indices(ldofidx);
        get_reduced_indices(ldofidx,uidx,tidx);

        fill(Ae.begin(), Ae.end(), 0);
        m.get_M(fe_values, Ae);
        Muu.add_entries(&(uidx[0]), &(uidx[0]), &(Ae[0]), lda, lda);
        Mtt.add_entries(&(tidx[0]), &(tidx[0]), &(Ae[0]), lda, lda);

        fill(Ae.begin(), Ae.end(), 0);
        m.get_K(fe_values, Ae, azimuthal_number);
        Kuu.add_entries(&(uidx[0]), &(uidx[0]), &(Ae[0]), lda, lda);
        Ktt.add_entries(&(tidx[0]), &(tidx[0]), &(Ae[0]), lda, lda);
        Kut.add_entries(&(uidx[0]), &(tidx[0]), &(Ae[0]), lda, lda);

        fill(Ae.begin(), Ae.end(), 0);
        m.get_B(fe_values, Ae);
        Buu.add_entries(&(uidx[0]), &(uidx[0]), &(Ae[0]), lda, lda);
    }

    Muu.fill_csc(p.Muu);
    Kuu.fill_csc(p.Kuu);
    Buu.fill_csc(p.Buu);
    Mtt.fill_csc(p.Mtt);
    Ktt.fill_csc(p.Ktt);
    Kut.fill_csc(p.Kut);
}


void Problem::assemble_alls(ProblemMatrices& p, ArrayP2& meshgrad)
{
    unsigned nt = t_dof_count;
    unsigned nu = u_dof_count;
    Assembler Mtt(nt,nt);
    Assembler Ktt(nt,nt);
    Assembler Kut(nu,nt);
    Assembler Muu(nu,nu);
    Assembler Kuu(nu,nu);
    Assembler Buu(nu,nu);

    QGauss<2> qf(lagp+1);  
    FEValues<2> fe_values(mapping, fe, qf, update_values |
                          update_gradients | update_jacobians |
                          update_quadrature_points | update_JxW_values );
    FESValues fes_values(fe_values);

    const unsigned ldofs = fe.dofs_per_cell;
    const unsigned lda = 4*ldofs;
    vector<double> Ae(lda*lda);
    vector<unsigned> ldofidx(ldofs);

    vector<int> uidx(lda);
    vector<int> tidx(lda);
    fill(uidx.begin(), uidx.end(), -1);
    fill(tidx.begin(), tidx.end(), -1);
    
    Point<2>* Xp = &(meshgrad[0]);
    for (aciter_t cell = dh.begin_active(); cell != dh.end(); ++cell) {
        fes_values.reinit(cell, Xp);
        Material& m = materials[cell->material_id()];
        cell->get_dof_indices(ldofidx);
        get_reduced_indices(ldofidx,uidx,tidx);

        fill(Ae.begin(), Ae.end(), 0);
        m.get_Ms(fes_values, Ae);
        Muu.add_entries(&(uidx[0]), &(uidx[0]), &(Ae[0]), lda, lda);
        Mtt.add_entries(&(tidx[0]), &(tidx[0]), &(Ae[0]), lda, lda);

        fill(Ae.begin(), Ae.end(), 0);
        m.get_Ks(fes_values, Ae, azimuthal_number);
        Kuu.add_entries(&(uidx[0]), &(uidx[0]), &(Ae[0]), lda, lda);
        Ktt.add_entries(&(tidx[0]), &(tidx[0]), &(Ae[0]), lda, lda);
        Kut.add_entries(&(uidx[0]), &(tidx[0]), &(Ae[0]), lda, lda);

        fill(Ae.begin(), Ae.end(), 0);
        m.get_Bs(fes_values, Ae);
        Buu.add_entries(&(uidx[0]), &(uidx[0]), &(Ae[0]), lda, lda);

        Xp += ldofs;
    }
    
    Muu.fill_csc(p.Muu);
    Kuu.fill_csc(p.Kuu);
    Buu.fill_csc(p.Buu);
    Mtt.fill_csc(p.Mtt);
    Ktt.fill_csc(p.Ktt);
    Kut.fill_csc(p.Kut);
}


/**
## Boundary condition handling

The boundary definitions and boundary condition handling still need
some serious clean-up.
*/

void Problem::add_chebboundary(ChebBoundary &cb)
{
    double dmin = GridTools::minimal_cell_diameter(tria);
    chebBoundaries.push_back(&cb);
    unsigned ncb = chebBoundaries.size();
    Triangulation<2>::active_cell_iterator cell;
    for (cell = tria.begin_active(); cell != tria.end(); ++cell)
        for (unsigned j=0; j<4; ++j) {
            if (cell->face(j)->at_boundary()) {
                Point<2> p1 = cell->face(j)->vertex(0);
                Point<2> p2 = cell->face(j)->vertex(1);
                if (cb.is_point_on_boundary(p1,dmin) && 
                    cb.is_point_on_boundary(p2,dmin))
                    cell->face(j)->set_boundary_indicator(ncb);
            }
        }
    tria.set_boundary(ncb,cb);
}


inline 
void label_active(vector<bool>& inactive, 
                  vector<int>::iterator j, 
                  unsigned& current)
{
    vector<bool>::iterator i = inactive.begin();
    for (; i != inactive.end(); ++i, ++j) {
        if (*i)
            *j = -1;
        else
            *j = current++;
    }
}


void Problem::set_boundary_conditions(lua_State* L, int bc)
{
    unsigned n = dh.n_dofs();

    vector<bool> zeroradial  (n,false);
    vector<bool> zeroangular (n,false);
    vector<bool> zeroaxial   (n,false);
    vector<bool> zerotemp    (n,false);

    find_zero_boundary_indices(L, zeroradial,  bc, "radial"     );
    find_zero_boundary_indices(L, zeroangular, bc, "angular"    );
    find_zero_boundary_indices(L, zeroaxial,   bc, "axial"      );
    find_zero_boundary_indices(L, zerotemp,    bc, "temperature");

    typedef vector<int>::iterator vii_t;
    vii_t ri[4];
    for (unsigned j = 0; j < 4; ++j) {
        reduced_indices[j].resize(n);
        ri[j] = reduced_indices[j].begin();
    }

    u_dof_count=0;
    label_active(zeroradial,  ri[0], u_dof_count);
    label_active(zeroangular, ri[1], u_dof_count);
    label_active(zeroaxial,   ri[2], u_dof_count);

    t_dof_count=0;
    label_active(zerotemp, ri[3], t_dof_count);
}


void Problem::get_reduced_indices(const vector<unsigned>& ldofidx,
                                  vector<int>& ruldofidx,
                                  vector<int>& rtldofidx)
{
    unsigned n = ldofidx.size();

    typedef vector<int>::iterator vii_t;
    vii_t ri[4];
    for (unsigned j = 0; j < 4; ++j)
        ri[j] = reduced_indices[j].begin();

    for (unsigned j=0; j<n; ++j) {
        unsigned i = ldofidx[j];
        ruldofidx[4*j+0] = ri[0][i]; 
        ruldofidx[4*j+1] = ri[1][i]; 
        ruldofidx[4*j+2] = ri[2][i]; 
        rtldofidx[4*j+3] = ri[3][i]; 
    }
}

void Problem::find_zero_boundary_indices(lua_State* L,
                                         vector<bool>& indices,
                                         int bc, const char* tag)
{
    unsigned n = dh.n_dofs();
    double dmin = GridTools::minimal_cell_diameter(tria);

    vector< Point<2 > > support_points (n, Point<2> ());
    DoFTools::map_dofs_to_support_points(mapping,dh,support_points);
    vector<bool> component_select (1,true);
    vector<bool> boundary_dofs (n,false);
    DoFTools::extract_boundary_dofs(dh,component_select,boundary_dofs);

    lua_getfield(L,bc,tag);
    unsigned num_bc_func =  (unsigned) lua_objlen(L,-1);
    for (unsigned j=0; j< num_bc_func; ++j) {
        for (unsigned i=0; i < n; ++i) {
            if (boundary_dofs[i]) {
                double r = support_points[i](0);
                double z = support_points[i](1);
                lua_rawgeti(L,-1,j+1);
                lua_pushnumber(L,r);
                lua_pushnumber(L,z);
                lua_pushnumber(L,dmin/100);
                if (lua_pcall(L,3,1,0) != 0) {
                    cout << "pcall fails!" << endl;
                    cout << lua_tostring(L, -1) << endl;
                }
                indices[i] = (indices[i] || lua_toboolean(L,-1));
                lua_pop(L,1);
            }
        }
    }
    lua_pop(L,1);
}

/**
## Cell field evaluator

The `DOFValue` class evaluates the mean displacement in the solution
vector $v$.  The value of `which` (0, 1, or 2) determines which
displacement component we use.  This function is used to generate
the color fields used for plotting with `write_svg`.
 */

Problem::DOFValue::DOFValue(Problem& problem, vector<double>& v, 
                            unsigned which) : 
    problem(problem), v(v), which(which),
    ldofidx(problem.fe.dofs_per_cell),
    uidx(4*problem.fe.dofs_per_cell),
    tidx(4*problem.fe.dofs_per_cell),
    qf(problem.lagp+1),
    fe_values(problem.mapping, problem.fe, qf,
              update_values | update_gradients | update_jacobians |
              update_quadrature_points | update_JxW_values )
{
}

double Problem::DOFValue::value(CellValue::citer_t& cell, double x, double y)
{
    fe_values.reinit(cell);
    cell->get_dof_indices(ldofidx);
    problem.get_reduced_indices(ldofidx,uidx,tidx);
    uidx[3] = tidx[3];
    unsigned ldofs = fe_values.dofs_per_cell;
    unsigned nqpts = fe_values.n_quadrature_points;
    unsigned lda   = 4*ldofs;
    double result = 0;
    double volume = 0;
    for (unsigned q=0; q < nqpts; ++q) {
        double r = fe_values.quadrature_point(q)(0);
        double wt = fe_values.JxW(q);
        for (unsigned i=0; i<ldofs; ++i) {
            double Ni = fe_values.shape_value(i,q);
            volume += Ni * wt;
            if (uidx[4*i+which] >= 0) {
                double Ni = fe_values.shape_value(i,q);
                result += v[uidx[4*i+which]] * Ni * wt;
            }
        }
    }
    return result / volume;
}
