#include <grid/tria.h>
#include <grid/tria_iterator.h>
#include <grid/tria_accessor.h>

#include "chebboundary.h"
#include "chebcurve.h"

#include <cstdio>

using namespace dealii;

//ldoc
/**
# Methods
*/

typedef const Triangulation<2>::line_iterator line_iter;
typedef Point<2> P2;
typedef std::vector<P2> P2vec;


P2 ChebBoundary::get_new_point_on_line(line_iter &line) const
{
    P2vec points(1);
    get_intermediate_points_on_line(line,points);
    return points[0];
} 


void ChebBoundary::get_intermediate_points_on_line(line_iter &line,
                                                   P2vec &points) const
{
    P2 p1 = line->vertex(0); 
    P2 p2 = line->vertex(1);
    unsigned n = points.size();
    for (unsigned j = 0; j < n; ++j) {
        points[j] = ( (n-j)*p1 + (j+1)*p2 )/(n+1);
        curve.projectp(points[j]);
    }
}


bool ChebBoundary::is_point_on_boundary(const P2 &p, double dmax) const
{
    P2 p1(p);
    curve.projectp(p1);
    bool result = (p.distance(p1) < 1e-2*dmax);
    return result;
}

