#ifndef LUAEXT_H
#define LUAEXT_H

#include <string>

extern "C" {
#include <lua.h>
}


inline double lua_popnumber(lua_State* L)
{
    double result = lua_tonumber(L,-1);
    lua_pop(L,1);
    return result;
}


inline int lua_popint(lua_State* L)
{
    int result = lua_tointeger(L,-1);
    lua_pop(L,1);
    return result;
}


inline unsigned lua_popuint(lua_State* L)
{
    return (unsigned) lua_popint(L);
}


inline std::string lua_popstring(lua_State* L)
{
    std::string result = lua_tostring(L,-1);
    lua_pop(L,1);
    return result;
}


inline void lua_setinregistry(lua_State* L, const void* key)
{
    lua_pushlightuserdata(L, (void*) key);
    lua_insert(L,-2);
    lua_settable(L, LUA_REGISTRYINDEX);
}


inline void lua_getfromregistry(lua_State* L, const void* key)
{
    lua_pushlightuserdata(L, (void*) key);
    lua_gettable(L, LUA_REGISTRYINDEX);
}


inline void lua_getobjfield(lua_State* L, const void* objkey, const char* name)
{
    lua_getfromregistry(L, objkey);
    lua_pushstring(L, name);
    lua_gettable(L,-2);
    lua_remove(L,-2);
}

class LuaFun {
public:
    LuaFun(lua_State* L, int f) : L(L) {
        lua_pushvalue(L,f);
        lua_setinregistry(L,this);
    }
    LuaFun(const LuaFun& Lf)            { copy(Lf); }
    LuaFun& operator=(const LuaFun& Lf) { clear(); copy(Lf); return *this; }
    ~LuaFun()                           { clear(); }

    double operator()(double xx) {
        lua_getfromregistry(L, this);
        lua_pushnumber(L, xx);
        lua_call(L,1,1);
        double result = lua_tonumber(L,-1);
        lua_pop(L,1);
        return result;
    }
private:
    lua_State* L;
    void clear() {
        lua_pushnil(L);
        lua_setinregistry(L, this);
    }
    void copy(const LuaFun& Lf) {
        L = Lf.L;
        lua_getfromregistry(L, &Lf);
        lua_setinregistry(L, this);
    }
};


#endif /* LUAEXT_H */
