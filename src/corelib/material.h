#ifndef DEAL2LAB_MATERIAL_H
#define DEAL2LAB_MATERIAL_H

#include <fe/fe_values.h>
#include "fesvalues.h"

#include <string>
#include <map>
#include <vector>

//ldoc
/**
% Materials

This class is a work in progress.  Eventually, we would like to
support several different material models, at which point this will be
split into an abstract interface together with several
implementations.  At present we only support thermoelastic elements.

## Class interface

The `set_property` and `get_property` functions are called from the
Lua side to manipulate material properties.  When `done_properties` is
called, those properties are transferred into internal storage.

The `get_M`, `get_K`, and `get_B` functions get the element mass,
stiffness, and damping contributions, respectively.

*/
class Material {
public:

    void set_property(const std::string& name, double value) { 
        properties[name] = value; 
    }
    double get_property(const std::string& name) { 
        return properties[name]; 
    }
    double& operator[](const std::string& name) { 
        return properties[name]; 
    }
    void done_properties();

    void get_M(dealii::FEValues<2>& fe_values, std::vector<double>& Me) const;
    void get_K(dealii::FEValues<2>& fe_values, std::vector<double>& Ke,
               unsigned m) const;
    void get_B(dealii::FEValues<2>& fe_values, std::vector<double>& Be) const;

    void get_Ms(FESValues& fes_values, std::vector<double>& Me) const;
    void get_Ks(FESValues& fes_values, std::vector<double>& Ke, 
                unsigned m) const;
    void get_Bs(FESValues& fes_values, std::vector<double>& Me) const;

private:
    std::map<std::string, double> properties;  // Property dictionary

    double E;       // Young's modulus
    double nu;      // Poisson ratio
    double rho;     // Mass density
    double kappa;   // Coefficient of thermal diffusivity
    double alpha;   // Coefficient of thermal expansion
    double cv;      // Specific heat at constant volume
    double T0;      // Operating temperature

    double beta;    // Derived parameter
    double lambda;  // Lame parameters
    double mu;
    double cd;      // Specific heat at constant density
};

//ldoc
#endif // DEAL2LAB_MATERIAL_H
