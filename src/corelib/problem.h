#ifndef PROBLEM_H
#define PROBLEM_H

#include <fe/fe_system.h>
#include <fe/mapping_q.h>
#include <grid/tria.h>
#include <grid/grid_tools.h>
#include <dofs/dof_handler.h>
#include <base/point.h>
#include <base/quadrature_lib.h>

#include <vector>

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "cscmatrix.h"
#include "material.h"
#include "chebboundary.h"

typedef int lua_Object;
typedef std::vector< dealii::Point<2> > ArrayP2;

//ldoc
/**
% Problem class

The `Problem` class is the main class around which the system is organized.
A `Problem` object represents a finite element mesh, along with associated
material definitions and boundary definitions.  It does *not* represent
the solvers that act on those definitions.

# Class declarations

## Matrix collections

The `ProblemMatrices` structure represents different pieces of the mass,
stiffness, or damping/gyroscopic term for mechanical and thermal problems.
It can also store the derivatives of these matrices with respect to some
parameter.

 */
struct ProblemMatrices {
    CSCMatrix Muu;  // Purely mechanical mass
    CSCMatrix Kuu;  // Purely mechanical stiffness
    CSCMatrix Buu;  // Purely mechanical damping / gyroscopic term
    CSCMatrix Mtt;  // Purely thermal mass
    CSCMatrix Ktt;  // Purely thermal stiffness
    CSCMatrix Kut;  // Thermoelastic coupling term
};

/**
## Cell evaluation

Ideally, this would return some field value at a point in a given
element, where `x` and `y` are parent domain coordinates and `cell` is
the given cell.  In practice, looking at arbitrary parent domain
coordinates seems pretty useless.  I keep it anyhow for future reference.
*/

class CellValue {
public:
    typedef dealii::DoFHandler<2>::active_cell_iterator citer_t;
    virtual ~CellValue() {}
    virtual double value(citer_t& cell, double x, double y) = 0;
};

/**
## Problem class declaration

A problem object is constructed in two phases: first, we build the
basic object, call `add_material` to add material information, then
call `mesh_initialize` to set up the actual mesh definition, and then
call `set_boundary_conditions` to set up the boundary conditions.
That last should go away at some point.

The `mesh_deriv` function computes sensitivities of node positions to
some underlying parameter by two-sided finite differencing.

The `assemble` functions fill a `ProblemMatrices` object with the
assembled mass, damping, and stiffness.
*/

class Problem {
public:
    Problem(int azimuthal_number, unsigned refinement, unsigned lagp);
    ~Problem();

    // -- Problem set up
    Material& add_material(unsigned id) { 
        if (id >= materials.size())
            materials.resize(id+1);
        return materials[id]; 
    }

    void set_boundary_conditions(lua_State*L, lua_Object bc);
 
    void mesh_initialize(unsigned nvertices,   const double* vertices,
                         unsigned ncells,      const double* cells, 
                         unsigned nboundaries, ChebBoundary** boundaries);

    // -- Sensitivity of node positions via finite differences
    ArrayP2* mesh_deriv(Problem& pp, Problem& pm, double h);

    // -- Matrix assembly
    void assemble_allg(ProblemMatrices& p);
    void assemble_alls(ProblemMatrices& p, ArrayP2& meshgrad);

    // -- Mesh output functions
    void write_msh(const char* fname);
    void write_eps(const char* fname);
    void write_svg(const char* fname, 
                   double xmin, double xmax, 
                   double ymin, double ymax,
                   double vmin, double vmax,
                   CellValue* evaluator = NULL);
    void get_bbox(double& xmin, double& xmax, 
                  double& ymin, double& ymax,
                  double& vmin, double& vmax,
                  CellValue* evaluator = NULL);

    void write_inp(const char* fname,
                   double xmin, double xmax, 
                   double ymin, double ymax,
                   double vmin, double vmax,
                   CellValue* evaluator = NULL,
                   unsigned int nslices = 72,
                   unsigned int which = 0);

    // -- Mesh property accessors
    int  get_azimuthal_number()      { return azimuthal_number; }
    void set_azimuthal_number(int m) { azimuthal_number = m;    }

    double get_maximal_cell_diameter() { 
        return dealii::GridTools::maximal_cell_diameter(tria);
    }

    CellValue* new_DOFValue(std::vector<double>& v, unsigned which) {
        return new DOFValue(*this, v, which);
    }

private:

   class DOFValue : public CellValue {
    public:
        DOFValue(Problem& problem, std::vector<double>& v, unsigned which);
        double value(CellValue::citer_t& cell, double x, double y);
    private:
        Problem& problem;
        std::vector<double>& v;
        unsigned which;
        std::vector<unsigned> ldofidx;
        std::vector<int> uidx;
        std::vector<int> tidx;
        dealii::QGauss<2> qf;
        dealii::FEValues<2> fe_values;
    };

    unsigned lagp;         // Degree of polynomials for Lagrangian elements
    unsigned refinement;   // Level of refinement (default is 0)
    int azimuthal_number;  // Azimuthal wave number

    dealii::Triangulation<2> tria;     // Finite element mesh
    dealii::FE_Q<2>          fe;       // Scalar Lagrange FE implementation
    dealii::MappingQ<2>      mapping;  // Qp mappings on boundary cells
    dealii::DoFHandler<2>    dh;       // Manage dof indexing

    unsigned u_dof_count;                 // Number of mechanical dofs
    unsigned t_dof_count;                 // Thermal dofs
    std::vector<int> reduced_indices[4];  // Map full to reduced indices

    std::vector<Material>      materials;      // Materials used
    std::vector<ChebBoundary*> chebBoundaries; // Boundary objects

    void add_chebboundary(ChebBoundary &lb);

    void get_reduced_indices(const std::vector<unsigned> &ldofidx,
                             std::vector<int>  &ruldofidx,
                             std::vector<int>  &rtldofidx);

    void find_zero_boundary_indices(lua_State* L, std::vector<bool> &,
                                    int bc, const char* tag);
};

//ldoc
#endif /* PROBLEM_H */
