#include "material.h"

#include <iostream>

using namespace std;
using namespace dealii;

//ldoc
/**
# Methods

## Material property transfer

The `done_properties` function transfers information out of the
properties dictionary and into internal variables.

*/
void Material::done_properties()
{
    // Direct properties
    E     = get_property("E");
    nu    = get_property("nu");
    rho   = get_property("rho");
    kappa = get_property("kappa");
    alpha = get_property("alpha");
    cv    = get_property("cv");
    T0    = get_property("T0");

    // Indirect (Lame parameters)
    lambda = nu*E/((1+nu)*(1-2*nu));
    mu     = E/(2*(1+nu));
    cd     = lambda+2*mu;
    
    // Thermal coefficient
    beta   = E*alpha/(1-2*nu);
}


/**
## B-matrix helpers

The $B$ matrix is used in elastic finite elements to map nodal
displacements into stresses.  The `helper_Kuu` function is a dummy
function that contains the definitions of `matexpr` functions that
compute $B$ matrices and their sensitivities, as well as the
constitutive tensor for isotropic linear elasticity.

*/
inline void helper_Kuu()
{
    /* <generator matexpr>
      function Bmat(G,N_r,m) =
      [ G(1),   0,         0     ;
        N_r,    m*N_r,     0     ;
        0,      0,         G(2)  ;
       -m*N_r,  G(1)-N_r,  0     ;
        0,      G(2),     -m*N_r ;
        G(2),   0,         G(1)  ];

      function Bpmat(Gp,rpN_rr,m) = 
      [ Gp(1),       0,               0         ;   
       -rpN_rr,     -m*rpN_rr,        0         ;
        0,           0,               Gp(2)     ; 
        m*rpN_rr,    Gp(1)+rpN_rr,    0         ;
        0,           Gp(2),           m*rpN_rr  ;
        Gp(2),       0,               Gp(1)     ];

      function trB(B) = [1, 1, 1, 0, 0, 0]*B;

      function Gmat(G,N_r,m) = 
      [ G(1); m*N_r; G(2) ];

      function Gpmat(Gp,rpN_rr,m) = 
      [ Gp(1); -m*rpN_rr; Gp(2) ];

      function Ciso(cd,l,mu) =
      [ cd, l, l, 0, 0, 0;
        l, cd, l, 0, 0, 0;
        l, l, cd, 0, 0, 0;
        0, 0, 0,  mu, 0, 0;
        0, 0, 0,  0, mu, 0;
        0, 0, 0,  0, 0, mu];
    */

}


/**
## Basic element matrices

These are the basic system matrices for coupled thermoelasticity.
That is, each node has four degrees of freedom, associated with
displacement in $r,z,\theta$ and variation in temperature.

These are the usual finite element quadrature loops: `q` varies over
the quadrature point indices, `i` and `j` vary over the shape function
indices.  The innermost expression evaluates the integrand sum at a given
point for a given pair of shape functions using `matexpr`.  Note that
when `i` and `j` are the same, only one of the writes to the diagonal matrix
blocks will "take" -- a deliberate design decision about aliasing in `matexpr`
that proves useful in situations like this.

*/
void Material::get_M(FEValues<2>& fe_values, vector<double>& Me) const
{
    unsigned ldofs = fe_values.dofs_per_cell;
    unsigned nqpts = fe_values.n_quadrature_points;
    unsigned lda   = 4*ldofs;
    for (unsigned q=0; q < nqpts; ++q) {
        double r = fe_values.quadrature_point(q)(0);
        double rho_wt = rho*r*fe_values.JxW(q);
        for (unsigned i=0; i<ldofs; ++i) {
            double Ni = fe_values.shape_value(i,q);
            for (unsigned j=i; j<ldofs; ++j) {
                double Nj = fe_values.shape_value(j,q);
                double* DDij = &(Me[(4*j)*lda+(4*i)]);
                double* DDji = &(Me[(4*i)*lda+(4*j)]);
                /* <generator matexpr>
                   input Ni, Nj, rho_wt, cv;
                   inout DDij[lda](4,4), DDji[lda](4,4);
                   z   = [0; 0; 0];
                   DDij += [eye(3), z; z', cv] * Ni*Nj * rho_wt;
                   DDji += [eye(3), z; z', cv] * Ni*Nj * rho_wt;
                 */
            }
        }
    }
}


void Material::get_K(FEValues<2>& fev, vector<double>& Ke,
               unsigned m) const
{
    unsigned ldofs = fev.dofs_per_cell;
    unsigned nqpts = fev.n_quadrature_points;
    unsigned lda   = 4*ldofs;
    for (unsigned q=0; q < nqpts; ++q) {
        double r  = fev.quadrature_point(q)(0);
        double wt = r*fev.JxW(q);
        for (unsigned i=0; i<ldofs; ++i) {
            const Tensor<1,2> Gi = fev.shape_grad(i,q);
            double Ni   = fev.shape_value(i,q);
            double Ni_r = Ni/r;
            for (unsigned j=i; j<ldofs; ++j) {
                const Tensor<1,2> Gj = fev.shape_grad(j,q);
                double Nj   = fev.shape_value(j,q);
                double Nj_r = Nj/r;
                double* DDij = &(Ke[(4*i)+(4*j)*lda]);
                double* DDji = &(Ke[(4*j)+(4*i)*lda]);
                /* <generator matexpr>
                   input m, kappa, beta, lambda, mu, cd;
                   input r, wt, Ni, Nj, Ni_r, Nj_r, Gi(2), Gj(2);
                   inout DDij[lda](4,4), DDji[lda](4,4);

                   // Displacement-to-strain matrices
                   Bi = Bmat(Gi,Ni_r,m);
                   Bj = Bmat(Gj,Nj_r,m);
                   
                   // Three-dimensional gradients
                   G3i = Gmat(Gi,Ni_r,m);
                   G3j = Gmat(Gj,Nj_r,m);
                   
                   // Mechanical stiffness
                   C  = Ciso(cd,lambda,mu);
                   Auu = (Bi'*C*Bj) * wt;

                   // Thermal stiffness
                   Att = G3i'*G3j * (kappa * wt);

                   // Thermo-mechanical interation
                   Autij = (trB(Bi)'*Nj) * (beta * wt);
                   Autji = (trB(Bj)'*Ni) * (beta * wt);

                   // Assemble into (i,j) and (j,i) slots
                   DDij += [Auu,   Autij; 0,0,0, Att];
                   DDji += [Auu',  Autji; 0,0,0, Att];
                */
            }
        }
    }
}


void Material::get_B(FEValues<2>& fe_values, vector<double>& Be) const
{
    unsigned ldofs = fe_values.dofs_per_cell;
    unsigned nqpts = fe_values.n_quadrature_points;
    unsigned lda   = 4*ldofs;
    for (unsigned q=0; q < nqpts; ++q) {
        double r = fe_values.quadrature_point(q)(0);
        double rho_wt = rho*r*fe_values.JxW(q);
        for (unsigned i=0; i<ldofs; ++i) {
            double Ni = fe_values.shape_value(i,q);
            for (unsigned j=i; j<ldofs; ++j) {
                double Nj = fe_values.shape_value(j,q);
                double* DDij = &(Be[(4*j)*lda+(4*i)]);
                double* DDji = &(Be[(4*i)*lda+(4*j)]);
                /* <generator matexpr>
                   input Ni, Nj, rho_wt, cv;
                   inout DDij[lda](3,3), DDji[lda](3,3);
                   J= [ 0, -1,  0;
                       -1,  0,  0;
                        0,  0,  0 ];
                   DDij += J * (Ni*Nj * rho_wt);
                   DDji += J * (Ni*Nj * rho_wt);
                 */
            }
        }
    }
}


/**
## Element sensitivity contributions

These functions compute the sensitivity of the element matrices to
geometric perturbations.

*/
void Material::get_Ms(FESValues& fes_values, vector<double>& Me) const
{
    FEValues<2>& fe_values = fes_values.fe_values;
    unsigned ldofs = fe_values.dofs_per_cell;
    unsigned nqpts = fe_values.n_quadrature_points;
    unsigned lda   = 4*ldofs;
    for (unsigned q=0; q < nqpts; ++q) {
        double rho_dwt = rho*fes_values.dwt[q];
        for (unsigned i=0; i<ldofs; ++i) {
            double Ni = fe_values.shape_value(i,q);
            for (unsigned j=i; j<ldofs; ++j) {
                double Nj = fe_values.shape_value(j,q);
                double* DDij = &(Me[(4*j)*lda+(4*i)]);
                double* DDji = &(Me[(4*i)*lda+(4*j)]);
                /* <generator matexpr>
                   input Ni, Nj, rho_dwt, cv;
                   inout DDij[lda](4,4), DDji[lda](4,4);
                   z   = [0; 0; 0];
                   DDij += [eye(3), z; z', cv] * Ni*Nj * rho_dwt;
                   DDji += [eye(3), z; z', cv] * Ni*Nj * rho_dwt;
                 */
            }
        }
    }
}


void Material::get_Ks(FESValues& fes_values, vector<double>& Ke,
                      unsigned m) const
{
    FEValues<2>& fev = fes_values.fe_values;
    unsigned ldofs = fev.dofs_per_cell;
    unsigned nqpts = fev.n_quadrature_points;
    unsigned lda   = 4*ldofs;
    unsigned m2    = m*m;
    for (unsigned q=0; q < nqpts; ++q) {
        double r  = fev.quadrature_point(q)(0);
        double rp = fes_values.rp[q];
        double rp_r = rp/r;
        double wt = r*fev.JxW(q);
        double dwt = fes_values.dwt[q];
        Tensor<1,2>* Gp = &(fes_values.Gp[q*ldofs]);
        for (unsigned i=0; i<ldofs; ++i) {
            double Ni = fev.shape_value(i,q);
            double Ni_r = Ni/r;
            Tensor<1,2> Gi = fev.shape_grad(i,q);
            Tensor<1,2> Gpi = Gp[i];
            for (unsigned j=i; j<ldofs; ++j) {
                double Nj   = fev.shape_value(j,q);
                double Nj_r = Nj/r;
                Tensor<1,2> Gj = fev.shape_grad(j,q);
                Tensor<1,2> Gpj = Gp[j];
                double* DDij = &(Ke[(4*i)+(4*j)*lda]);
                double* DDji = &(Ke[(4*j)+(4*i)*lda]);
                /* <generator matexpr> 
                   input m, wt, dwt, rp_r, Ni, Nj;
                   input Ni_r, Nj_r, Gi(2), Gj(2), Gpi(2), Gpj(2); 
                   input lambda, cd, mu, kappa, beta;
                   inout DDij[lda](4,4), DDji[lda](4,4);

                   // Strain matrices and derivatives
                   Bi  = Bmat (Gi,Ni_r,m);
                   Bj  = Bmat (Gj,Nj_r,m);
                   Bpi = Bpmat(Gpi,Ni_r*rp_r,m);
                   Bpj = Bpmat(Gpj,Nj_r*rp_r,m);

                   // Three-dimensional gradients and derivs
                   G3i  = Gmat (Gi,Ni_r,m);
                   G3j  = Gmat (Gj,Nj_r,m);
                   G3pi = Gpmat(Gpi,Ni_r*rp_r,m);
                   G3pj = Gpmat(Gpj,Nj_r*rp_r,m);

                   // Sensitivity of mechanical stiffness
                   C    = Ciso(cd,lambda,mu);
                   Auu  = Bi'*C*Bj;
                   Apuu = Bpi'*C*Bj + Bi'*C*Bpj;
                   Kuu  = Apuu*wt + Auu*dwt;

                   // Sensitivity of thermal stiffness
                   Att  = G3i'*G3j;
                   Aptt = G3pi'*G3j + G3i'*G3pj;
                   Ktt  = Aptt*(kappa*wt) + Att*(kappa*dwt);

                   // Sensitivity of thermo-mechanical interation
                   Autij  = trB(Bi)' *Nj;
                   Autji  = trB(Bj)' *Ni;
                   Aputij = trB(Bpi)'*Nj;
                   Aputji = trB(Bpj)'*Ni;
                   Kutij = Autij * (beta*dwt) + Aputij * (beta*wt);
                   Kutji = Autji * (beta*dwt) + Aputji * (beta*wt);

                   // Assemble into global matrix
                   z = [0; 0; 0];
                   DDij += [Kuu, Kutij; z', Ktt];
                   DDji += [Kuu', Kutji; z', Ktt];
                */
            }
        }
    }
}


void Material::get_Bs(FESValues& fes_values, vector<double>& Be) const
{
    FEValues<2>& fe_values = fes_values.fe_values;
    unsigned ldofs = fe_values.dofs_per_cell;
    unsigned nqpts = fe_values.n_quadrature_points;
    unsigned lda   = 4*ldofs;
    for (unsigned q=0; q < nqpts; ++q) {
        double rho_dwt = rho*fes_values.dwt[q];
        for (unsigned i=0; i<ldofs; ++i) {
            double Ni = fe_values.shape_value(i,q);
            for (unsigned j=i; j<ldofs; ++j) {
                double Nj = fe_values.shape_value(j,q);
                double* DDij = &(Be[(4*j)*lda+(4*i)]);
                double* DDji = &(Be[(4*i)*lda+(4*j)]);
                /* <generator matexpr>
                   input Ni, Nj, rho_dwt, cv;
                   inout DDij[lda](3,3), DDji[lda](3,3);
                   J= [ 0, -1,  0;
                       -1,  0,  0;
                        0,  0,  0 ];
                   DDij += J * (Ni*Nj * rho_dwt);
                   DDji += J * (Ni*Nj * rho_dwt);
                 */
            }
        }
    }
}
