#ifndef SOLVER_H
#define SOLVER_H

#include <vector>

#include "areigs.h"
#include "cscmatrix.h"
#include "umfmatrix.h"

class ArpackProblem : public ArpackDS {
public:
    ArpackProblem(CSCMatrix& M, CSCMatrix& K) :
    M(M), K(K), umfp(&K), scratch(M.get_n()) {
        set_n(M.get_n());
        set_which("LA");
        set_mode(3);
        set_shift(0);
    }
    
    void eigs(unsigned nev)
    {
        set_nev(nev);
        d.resize(get_ncv());
        vv.resize(get_n()*get_ncv());
        compute_eigs(&(d[0]), &(vv[0]));
    }
    
    double get_mode(unsigned i, std::vector<double>& v)
    {
        std::copy(vv.begin()+i*get_n(), vv.begin()+(i+1)*get_n(), v.begin());
        return d[i];
    }

protected:

    void times_OP1(double* x, double* opx) {
        M.vmult(&(scratch[0]), x);
        umfp.solve(opx, &(scratch[0]));
    }

    void times_M  (double* x, double* Mx) {
        M.vmult(Mx, x);
    }

private:
    CSCMatrix& M;
    CSCMatrix& K;
    UMFMatrix umfp;
    std::vector<double> scratch;
    std::vector<double> d;
    std::vector<double> vv;
};

#endif // SOLVER_H
