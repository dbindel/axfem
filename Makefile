all: tools lib app wrap

.PHONY: tools lib app test clean clean-all

PWD = $(shell pwd)

app: 
	(cd src/app; make)

lib:
	(cd src/corelib; make)
	(cd src/numeric; make)

test:
	(cd test; make)

wrap:
	@echo "=================================== Building wrappers"
	@echo "package.path = package.path .. ';$(PWD)/script/?.lua'..';$(PWD)/test/common/?.lua'" > init.lua
	@echo "require 'startup'" >> init.lua
	@echo 'LUA_INIT=@$(PWD)/init.lua $(PWD)/src/app/axfem $$@' > axfem
	@chmod +x axfem
	@echo 'LUA_INIT=@$(PWD)/init.lua gdb $(PWD)/src/app/axfem $$@' > axfemg
	@chmod +x axfemg
	@echo 'LUA_INIT=@$(PWD)/init.lua valgrind --dsymutil=true --leak-check=full $(PWD)/src/app/axfem $$@' > axfemv
	@chmod +x axfemv

tools:
	(cd tools; make)

clean: 
	rm -f axfem axfemg axfemv init.lua
	(cd src/app; make clean)
	(cd src/corelib; make clean)
	(cd src/numeric; make clean)
	(cd test; make clean)
	(cd doc; make clean)

clean-all: clean
	(cd tools; make clean)
