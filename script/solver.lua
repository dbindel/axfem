--ldoc
--[[
% Solver computations

# Basic formulas

The point of the Lua-based solver extensions is to make the
computations as close to the mathematics as possible.

## Eigenvalue perturbation

In general, if $(u, \omega^2)$ is a simple eigenpair for
the eigenvalue problem
$$
  (K_{uu} - \omega^2 M_{uu}) u = 0
$$
then we can write the perturbative formula for the eigenvalue and
vector
$$
  (K_{uu} - \omega^2 M_{uu}) \delta u + 
  (-2 \omega M_{uu} u) \delta \omega +
  (\delta K_{uu} - \omega^2 \delta M_{uu}) u = 0.
$$
Multiplying through on the left by $u^T$ gives a formula for
computing the eigenvalue sensitivity with no further work:
$$
  \delta \omega = 
  \frac{ u^T (\delta K_{uu} - \omega^2 \delta M_{uu}) u }
       { 2 \omega \, u^T M_{uu} u }.
$$
The main reason for posing this system in terms of the variable
$\lambda$ rather than the original variable $\delta \omega$ is that
using $\delta \omega$ directly can lead to rather poor scaling in
the linear solve.  By the same rationale, it might actually make
sense to use a symmetric bordering (i.e. enforce $-\omega_0^2 u_0^T
M_{uu} \delta u = 0$), but we're not currently using this condition.

The `eigenfrequency_sensitivity` routine computes the sensitivity
of the eigenvalue using the Rayleigh quotient, without doing any
additional linear solves for the eigenvector.  The
`solve_eigenvector_perturbation` routine solves the linear system
to compute the eigenvector perturbation.  The
`create_eigenpert_solver` and `create_sparsity_eigenpert` routines
build (and factor) the bordered matrix and its sparsity pattern,
respectively.


## Bryan's factor

Bryan's factor is given by
$$
   BF = \frac{1}{m} \frac{b_{uu}}{m_{uu}}.
$$
where $m$ is the azimuthal wave number, $b_{uu} = u^T B_{uu} u$,
and $m_{uu} = u^T M_{uu} u$.  The sensitivity of Bryan's factor
is given by the quotient rule as
$$
   \delta BF = \frac{1}{m} 
     \frac{ (\delta b_{uu}) m_{uu} - b_{uu} (\delta m_{uu}) }
          { (m_{uu})^2 }
$$
where
$$
   \delta b_{uu} = 2 (\delta u)^T B_{uu} u + u^T (\delta B_{uu}) u
$$
and
$$
   \delta m_{uu} = 2 (\delta u)^T M_{uu} u + u^T (\delta M_{uu}) u.
$$


## Thermoelastic damping

The quality factor due to thermoelastic damping is (approximately)
$$
  Q_{\mathrm{TED}} = \left| \frac{k_{uu}}{k_{ut}} \right|,
$$
where $k_{uu} = u^T K_{uu} u$, $k_{ut} = u^T K_{u \theta} \theta_i$,
and $\theta_i$ is the imaginary part of the temperature variation
induced by the volumentric strain rate.  We again differentiate
using the quotient rule, which gives
$$
   \delta Q_{\mathrm{TED}} = 
   \operatorname{sgn}(Q)
   \frac{(\delta k_{uu}) k_{ut} - k_{uu} (\delta k_{ut})}{k_{ut}^2}
$$
where
$$
   \delta k_{uu} = 2 (\delta u)^T K_{uu} u + u^T (\delta K_{uu}) u
$$
and
$$
   \delta k_{ut} = 
     (\delta u)^T K_{u \theta} \theta_i +
     u^T K_{u \theta} (\delta \theta_i) +
     u^T (\delta K_{u \theta}) \theta_i.
$$
However, `deal.ii` does not have direct support for complex matrices,
so we avoid complex arithmetic by generating equivalent real forms.


# Computational routines

## Eigensolver wrapper

The `find_lowest_eigenmode` function just calls the `ArpackProblem` class.
--]]

function find_lowest_eigenmode(P,v)
   local Muu = P:get_Muug()
   local Kuu = P:get_Kuug()
   local es = ArpackProblem:new(Muu, Kuu)
   es:eigs(1)
   local w2 = es:get_mode(0, v)
   es:delete()
   return sqrt(w2)
end

--[[
## Symbol convenience functions

The `newsym` function creates a new symbol (matrix, vector, or scalar)
with deferred evaluation.  The function that generates the actual
value is called exactly once, when it is needed.  We assume the symbol
value will not change after that, so subsequent calls are not needed.
--]]

local function newsym(mtype,f,args,m,n)
   return Sym.new{ op="terminal", mtype=mtype, m=m, n=n,
                   eval = f and function() return f(unpack(args)) end }
end

local function matsym(f,m,n,...) return newsym("matrix", f, {...}, m,n) end
local function vecsym(f,m,...)   return newsym("cvec",   f, {...}, m)   end
local function scalsym(f,...)    return newsym("scalar", f, {...})      end

--[[
The `idx` function returns an index range object used to slice results.
--]]

local function idx(a,b)
   if b then  return Sym.new_index({a,b}, b-a+1)
   else       return Sym.new_index(a,1)
   end
end

--[[
The `UMFbmatrix` and `bvcat` helpers generate symbolic forms for
factoring an assembled block matrix and for assembling a block
column vector, respectively.
--]]

local function UMFbmatrix(...)
   return Sym.UMF(Sym.assemble(Sym.bmatrix(...)))
end

local function bvcat(...)
   return Sym.assemble(Sym.vcat(...))
end

--[[
For convenience, we write `T` for transposition of a symbolic expression
throughout the rest of this file.
--]]

local T = Sym.transp

--[[
## Overloaded `Solver` constructor

The overloaded `Solver` constructor creates a caching evaluator for
symbolic expressions, and uses that to create concise routines for
computing the frequency, Bryan's factor, and quality factor for the
lowest frequency mode at the specified azimuthal wave number.
The evaluator takes care of making sure that we re-use subexpressions
that are common across the computations.
--]]

Solver = Class.new_base()

function Solver:delete()
end

function Solver:new(P, T0)

   -- New solver and evaluation context
   T0 = T0 or 300
   local solver = Class.new(Solver)
   local eval   = CMatEval.new()
  
   -- Get azimuthal wave number and basic dimensions
   local m  = P:get_azimuthal_number()
   local nu = P:get_Muug().m
   local nt = P:get_Mttg().m

   -- Symbols for basic matrices
   local Muu = matsym(P.get_Muug, nu,nu, P)
   local Buu = matsym(P.get_Buug, nu,nu, P)
   local Kuu = matsym(P.get_Kuug, nu,nu, P)
   local Kut = matsym(P.get_Kutg, nu,nt, P)
   local Mtt = matsym(P.get_Mttg, nt,nt, P)
   local Ktt = matsym(P.get_Kttg, nt,nt, P)
   
   -- Symbols for resonant frequency and mode shape
   local w = scalsym(nil)
   local u = vecsym(nil, nu)

   -- Evaluator for frequency and mode shape
   local function eval_eigenmode()
      u.terminal = SparseV:new(nu)
      w.terminal = find_lowest_eigenmode(P, u.terminal)
      tolua.takeownership(u.terminal)
   end
   w.eval = eval_eigenmode
   u.eval = eval_eigenmode
   
   -- Matrix used for computing eigenvector perturbations
   local w2   = w*w
   local inv_AM = UMFbmatrix{ {Kuu-w2*Muu, -w2*Muu*u},
                              {T(u),       0    } }

   -- Computation of temperature variation
   local inv_AT = UMFbmatrix{ {Ktt, -w*Mtt}, 
                              {w*Mtt, Ktt} }
   local tri = inv_AT * bvcat(Sym.zero(nt,1), -T0*w*T(Kut)*u)
   local tr  = tri(idx(1,nt))
   local ti  = tri(idx(nt+1,2*nt))
   
   -- Symbolic modal projections
   local muu = T(u)*(Muu*u)
   local buu = T(u)*(Buu*u)
   local kuu = T(u)*(Kuu*u)
   local kut = T(u)*Kut*ti

   -- Generate functions to evaluate frequency, Bryan's factor, and Q
   local uu = eval:future( u )
   local ww = eval:future( w )
   local BF = eval:future( buu / muu / m )
   local sQ = eval:future( kuu / kut )

   local dw0, dBF, dsQ = {}, {}, {}
   local nparams = P:get_num_params()
   for i=1,nparams do
      
      -- Generate symbols for the perturbations to u, ti, matrices
      local dMuu = matsym(P.get_Muus, nu,nu, P, i-1)
      local dBuu = matsym(P.get_Buus, nu,nu, P, i-1)
      local dKuu = matsym(P.get_Kuus, nu,nu, P, i-1)
      local dKut = matsym(P.get_Kuts, nu,nt, P, i-1)
      local dMtt = matsym(P.get_Mtts, nt,nt, P, i-1)
      local dKtt = matsym(P.get_Ktts, nt,nt, P, i-1)

      local duw  = inv_AM * bvcat((w*w)*(dMuu*u)-dKuu*u,0)
      local du   = duw(idx(1,nu))
      local dw   = duw(idx(nu+1)) * w/2

      local dtri = inv_AT *        
            bvcat( dw*Mtt*ti + w*dMtt*ti - dKtt*tr,
                   -(T0*dw*T(Kut)*u + T0*w*T(dKut)*u + T0*w*T(Kut)*du +
                   dw*Mtt*tr + dKtt*ti + w*dMtt*tr) )
      local dti  = dtri(idx(nt+1,2*nt))

      -- Compute perturbations to the modal projections
      local dmuu = 2*(T(du)*(Muu*u)) + T(u)*dMuu*u
      local dbuu = 2*(T(du)*(Buu*u)) + T(u)*dBuu*u
      local dkuu = 2*(T(du)*(Kuu*u)) + T(u)*dKuu*u
      local dkut = T(du)*Kut*ti + T(u)*Kut*dti + T(u)*dKut*ti

      -- Compute perturbations to frequency, Bryan's factor, Q
--      dw0[i] = eval:future( (T(u)*dKuu*u-(w*w)*(T(u)*dMuu*u))/(2*w*muu) )
      dw0[i] = eval:future( dw )
      dBF[i] = eval:future( (dbuu*muu-buu*dmuu)/(muu*muu*m) )
      dsQ[i] = eval:future( (dkuu*kut-kuu*dkut)/(kut*kut) )

   end

   -- Add methods to satisfy the old solver interface
   local function sgnQ() return sQ()/abs(sQ()) end
   function solver:get_eigenfrequency()          return ww()              end
   function solver:get_eigenvector()             return uu()              end
   function solver:compute_bryans_factor()       return BF()              end
   function solver:compute_quality_factor()      return abs(sQ())         end
   function solver:eigenfrequency_sensitivity(i) return dw0[i+1]()        end
   function solver:bryansfactor_sensitivity(i)   return dBF[i+1]()        end
   function solver:qualityfactor_sensitivity(i)  return sgnQ()*dsQ[i+1]() end
   solver.get_bryans_factor  = solver.compute_bryans_factor
   solver.get_quality_factor = solver.compute_quality_factor

   -- Add profiling
   local profiling = {
      "get_eigenfrequency",
      "compute_bryans_factor",
      "compute_quality_factor",
      "eigenfrequency_sensitivity",
      "bryansfactor_sensitivity",
      "qualityfactor_sensitivity"
   }
   for i,k in ipairs(profiling) do
      solver[k] = fprofiler(solver[k], k)
   end

   return solver
end
