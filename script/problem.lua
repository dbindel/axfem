--ldoc
--[[
% Problem class interface extensions

This module extends the interface of the C++ `Problem` class that
is exported via `tolua++`.  Some of the extension methods in this
module are meant to allow the Lua examples to continue using the
same interface as the C++ core (rapidly) evolves.  At some point,
though, we may just change the user interface to this class.

## Mesh generator adapter

Originally, the `Problem` class constructor took a mesh generation
function and an associated parameter list as arguments.  That mesh
generation function returned a structure with nodes in a `vertices`
array, cell connectivities in a `cell_info` array, and boundary
information in a `boundaries` list (as provided by the `mapped_block`
function).  The class has since been revised so that the
`mesh_initialize` function passes in this connectivity information.
The `meshgen_wrapper` function converts old-style mesh generator
functions to use the new interface based on `mesh_initialize`.

--]]
local function meshgen_wrapper(meshgen, params)
   return function(self)
      local m = meshgen(unpack(params))
      local v = {}
      local c = {}
      local b = {}
      for i,vi in ipairs(m.vertices) do
         table.insert(v, vi.x)
         table.insert(v, vi.y)
      end
      for i,ci in ipairs(m.cell_info) do
         table.insert(c, ci.matid.id or 0)
         table.insert(c, ci.bl-1)
         table.insert(c, ci.br-1)
         table.insert(c, ci.tl-1)
         table.insert(c, ci.tr-1)
      end
      m.boundaries = m.boundaries or {}
      self:mesh_initialize(v, c, m.boundaries)
   end
end

--[[
## Overloaded mesh constructor
--]]

local old_problem_new = Problem.new
function Problem:new(p)

   local name       = p.name
   local m          = p.m       or error("Missing azimuthal number m")
   local meshgen    = p.meshgen or error("Missing meshgen function")
   local meshgen    = p.meshgen or error("Missing mesh generator")
   local params     = p.params     or {}
   local refinement = p.refinement or 0
   local lagp       = p.lagp       or 2
   local T0         = p.T0         or 300

   meshgen = meshgen_wrapper(meshgen, params)
   local obj = old_problem_new(self, m, refinement, lagp)
   meshgen(obj)

   obj.probname   = name
   obj.params     = params
   obj.refinement = refinement
   obj.lagp       = lagp
   obj.meshgen    = meshgen
   obj.T0         = T0

   obj.assembled_matrices = nil
   obj.assembled_sensitivities = {}
   obj.meshgrads = {}

   return obj
end

function Problem:create(p)
   local problem = Problem:new(p)
   tolua.takeownership(problem)
   return problem
end

--[[
## Wrappers for the `Solver` interface

The goal is to get rid of an external solver class, and call the solvers
through the `Problem` interface directly.

--]]

local solver_functions = {
 "get_eigenfrequency",
 "get_eigenvector",
 "get_bryans_factor",
 "get_quality_factor",
 "compute_bryans_factor",
 "compute_quality_factor",
 "eigenfrequency_sensitivity",
 "qualityfactor_sensitivity",
 "bryansfactor_sensitivity"
}

for _,v in pairs(solver_functions) do
   Problem[v] = function(self, ...)
      if not self.solver then
         self.solver = Solver:new(self, self.T0)
         tolua.takeownership(self.solver)
      end
      return self.solver[v](self.solver, ...)
   end 
end


--[[
## Material setup

In Lua, a material is just a table of properties (`E`, `nu`, `rho`, etc)
and a zero-based numeric index.  In C++, there is a corresponding object,
which is used to compute the response of an element made of a particular
material type.  This wrapper sets up the properties of the materials

This interface will probably change substantially as soon as we get a
wider variety of material types integrated into the system.

--]]
local base_add_material = Problem.add_material
function Problem:add_material(p)
   local mat = base_add_material(self, p.id);
   local fields = {"E", "nu", "rho", "kappa", "alpha", "cv", "T0"};
   for k,v in pairs(fields) do
      mat:set_property(v, p[v] or 0)
   end
   mat:done_properties()
end

--[[
## Mesh output support

By default, the `write_svg` function will plot inside a box that
contains all of the device points with an additional 5% margin on each
side.  However, the user can override any of these dimensions.
--]]

local old_write_svg = Problem.write_svg
function Problem:write_svg(p, xmin, xmax, ymin, ymax, vmin, vmax)
   local fname, evaluator
   if type(p) == "string" then
      fname = p
   elseif type(p) == "table" then
      fname = p[1] or p.name
      xmin = p.xmin
      xmax = p.xmax
      ymin = p.ymin
      ymax = p.ymax
      if p.field then
         evaluator = self:new_DOFValue(self:get_eigenvector(), p.field)
      end
   end
   local xmin0, xmax0, ymin0, ymax0, vmin0, vmax0 = self:get_bbox(0,0,0,0,0,0,
                                                                  evaluator)
   local offsetx = (xmax0-xmin0)*0.1
   local offsety = (ymax0-ymin0)*0.1
   xmin0, xmax0 = xmin0-offsetx, xmax0+offsetx
   ymin0, ymax0 = ymin0-offsety, ymax0+offsety
   xmin, xmax = xmin or xmin0, xmax or xmax0
   ymin, ymax = ymin or ymin0, ymax or ymax0
   old_write_svg(self, fname, xmin, xmax, ymin, ymax, vmin0, vmax0, evaluator)
   if evaluator then evaluator:delete() end
end

--[[

## 3D Mesh and Field Output

--]]
local old_write_inp = Problem.write_inp
function Problem:write_inp(p, xmin, xmax, ymin, ymax, vmin, vmax, nslices)
   local fname, evaluator
   if type(p) == "string" then
      fname = p
   elseif type(p) == "table" then
      fname = p[1] or p.name
      xmin = p.xmin
      xmax = p.xmax
      ymin = p.ymin
      ymax = p.ymax
      if p.field then
         evaluator = self:new_DOFValue(self:get_eigenvector(), p.field)
      end
   end
   local xmin0, xmax0, ymin0, ymax0, vmin0, vmax0 = self:get_bbox(0,0,0,0,0,0,
                                                                  evaluator)
   local offsetx = (xmax0-xmin0)*0.1
   local offsety = (ymax0-ymin0)*0.1
   xmin0, xmax0 = xmin0-offsetx, xmax0+offsetx
   ymin0, ymax0 = ymin0-offsety, ymax0+offsety
   xmin, xmax = xmin or xmin0, xmax or xmax0
   ymin, ymax = ymin or ymin0, ymax or ymax0
   nslices = nslices or 180 
   which = p.field or 0  
   old_write_inp(self, fname, xmin, xmax, ymin, ymax, vmin0, vmax0, evaluator, nslices, which)
   if evaluator then evaluator:delete() end
end

--[[
## Mesh gradient support

The mesh generator function takes a list of parameters, which we
assume control geometry.  We can query the number of parameters with
the `get_num_params` method, in case we want to loop over all of them.
--]]

function Problem:get_num_params()
   return #(self.params)
end

--[[
We support semi-analytic differentiation with of the behavior respect
to geometric parameters; that is, we use centered finite differences
to estimate how changing the parameters changes the node positions,
but compute all other derivatives analytically.  The `get_stepsizes`
function automatically chooses step sizes to be a fixed fraction of
the parameter magnitude.  This heuristic should *not* be used to get
step sizes if the original parameter value is zero!
--]]

function Problem:get_stepsizes(stepsizeratio)
   local stepsizes = {}
   for i,param in ipairs(self.params) do
      stepsizes[i] = param/stepsizeratio
   end
   return stepsizes
end

--[[
Given a set of step sizes, the `mesh_gradient` function instantiates
two meshes corresponding to slightly perturbed functions (where the
perturbation is given by the `stepsizes` returned from
`get_stepsizes`), then calls `mesh_deriv` to record the approximate
derivative of all the node positions with respect to the parameter
changes.
--]]

function Problem:mesh_gradient(stepsizes)

   stepsizes = stepsizes or 1000
   if(type(stepsizes) == 'number') then
      stepsizes = self:get_stepsizes(stepsizes)
   end

   local m = self:get_azimuthal_number()
   for i=1,#stepsizes do

      local param = self.params[i]
      self.params[i] = param + stepsizes[i]
      local probp = old_problem_new(Problem, m, self.refinement, self.lagp)
      self.meshgen(probp)

      self.params[i] = param - stepsizes[i]
      local probm = old_problem_new(Problem, m, self.refinement, self.lagp)
      self.meshgen(probm)

      self.params[i] = param
      self.meshgrads[i] = self:mesh_deriv(probp, probm, stepsizes[i])
      tolua.takeownership(self.meshgrads[i])

      probm:delete()
      probp:delete()
   end
end

--[[
## Assembly extensions

Originally, space for the system matrices and sensitivity matrices was
managed on the C++ side by the `Problem` object.  Now, the Lua
environment actually takes ownership of those objects.  The first time
a system matrix is requested, the getter function calls the assembly
routine to build all the system matrices, and takes ownership of the
result.  Then the requested matrix is returned.  The assembly only
occurs once, with subsequent getter calls using a cached version.
Similarly, the sensitivities with respect to a given parameter are
assembled only once, the first time one of them is requested, and
they are subsequently cached.

The `make_matrix_getter` and `make_sensitivity_getter` functions
build the actual getters for the different matrices.
--]]

local function make_matrix_getter(mat_name)
   local function fmat(self)
      if not self.assembled_matrices then
         self.assembled_matrices = ProblemMatrices:new()
         tolua.takeownership(self.assembled_matrices)
         self:assemble_allg(self.assembled_matrices)
      end
      return self.assembled_matrices[mat_name]
   end
   return fmat
end

local function make_sensitivity_getter(mat_name)
   local function fmat(self, k)
      if not self.assembled_sensitivities[k] then
         self.assembled_sensitivities[k] = ProblemMatrices:new()
         tolua.takeownership(self.assembled_sensitivities[k])
         self:assemble_alls(self.assembled_sensitivities[k], 
                            self.meshgrads[k+1])
      end
      return self.assembled_sensitivities[k][mat_name]
   end
   return fmat
end

--[[
The getter functions for the system matrices have names like `get_Muug`, etc;
the getters for the sensitivity matrices are called `get_Muus`, etc.
--]]

local mat_names = {'Muu', 'Kuu', 'Buu', 'Mtt', 'Ktt', 'Kut'}
for i,mat_name in ipairs(mat_names) do
   local methodg = 'get_' .. mat_name .. 'g'
   local methods = 'get_' .. mat_name .. 's'
   Problem[methodg] = make_matrix_getter(mat_name)
   Problem[methods] = make_sensitivity_getter(mat_name)
end

--[[
## Profiling extensions

The `fprofiler` function adds a wrapper to an existing function to
time all calls to that function.  We profile the time spent in what
are probably the three most expensive operations purely within the
mesh class: system matrix assembly, sensitivity matrix assembly,
and the mesh gradient computation.
--]]

local profiling = { 'assemble_allg', 'assemble_alls', 'mesh_gradient' }
for i,v in ipairs(profiling) do
   Problem[v] = fprofiler(Problem[v], v)
end

