--
-- Evaluate symbolic matrix-vector expressions with axfem sparse objects
--
require "sym"
require "sparse"

--ldoc
--[[
% Matrix expression evaluation

# Extended symbol system support for UMFPACK inverses

The basic symbol system is supposed to be generic, but easy to
extend to handle specific features.  The matrix-via-UMFPACK
would be one of these.
--]]

function Sym.UMF(s)
   return Sym.new("UMF", Sym.cast(s))
end

function Sym.MatTyper:UMF_A(s)
   s.mtype,s.m,s.n = "matrix",s[1].m,s[1].n
   assert(s.m == s.n, "Cannot solve with rectangular matrices")
   return s
end

--[[
# Matrix evaluator

The `MatEval` object converts symbolic matrix representations into
real scalar or vector results.  The evaluator is responsible for
managing scratch space for intermediates.

Before evaluation, all expressions should be typed, normalized, and
simplified with the `CollapseIdent` class.  This ensures, for example,
that the only time an explicit zero appears is in the assembly of a
top-level matrix expression.  We evaluate these assembly expressions into
code that builds an explicit representation by calling methods to add
scalar multiples of scalars, vectors, and sparse matrix terminals at
given offset locations.  The code has the calling sequence

    assembler_fun(assembler, c, i, j)

where `assembler` is an object on which assembly methods are called,
`c` is a scale factor, and `i` and `j` are the offsets of the block
where the given contribution should be added.  The assembler should
support the following operations that add $c$ times the indicated type
(an $n$-by-$n$ identity, a matrix, a row or column vector, or a scalar)
with leading entry at position $(i,j)$:

    add_eye(n, c, i, j)
    add_matrix(A, c, i, j)
    add_rvec(v, c, i, j)
    add_cvec(w, c, i, j)
    add_scalar(a, c, i, j)

The assembler object must also know that if `transpose` is true, it
should effectively be assembling into the transposed matrix.

## The evaluator class
--]]

MatEval = Sym.new_pass("evaluate", Sym.mat_lookup)

function MatEval:new()
   return Class.new(MatEval)
end

--[[
## Scratch space allocation

Every vector produced by the evaluator is decorated with a `scratch`
field to indicate whether it is an intermediate result that can be
modified.

A vector result of a call to `eval` is generally treated as an
intermediate scratch vector.  The call metamethod, which is used at
the top level by the user, gets rid of the scratch marker before
returning results.  For matrix results where `eval` returns a function
that acts on an assembler, scale factor, and offset indices, the call
metamethod wraps the function to give default values to the scale
factor and offsets if they are left unspecified.  For vector results
where `eval` returns a function (because it was assembled as a `bmatrix`),
we assemble the vector here.
--]]

local function scratch(v)
   if not v.scratch then
      local vs = v:clone()
      vs.scratch = true
      return vs
   else
      return v
   end
end

function MatEval:__call(s)
   local result = self:eval(s)
   if type(result) == "table" then
      result.scratch = nil
   elseif type(result) == "function" then
      if s.mtype == "cvec" or s.mtype == "rvec" then
         local v = SparseV:new( (s.m or 1)*(s.n or 1) )
         local builder = VecBuilder:new(v)
         result(builder,1,1,1)
         builder:delete()
         tolua.takeownership(v)
         return v
      else
         return function(assembler, c, i, j)
            result(assembler, c or 1, i or 1, j or 1)
         end
      end
   end
   return result
end

--[[
## Evaluating terminals

If a terminal symbol has an `eval` field, we call that first to make
sure that the value stored in the `terminal` field is actually up to
date.  Then we return the terminal value itself, first making sure it is
not marked as scratch.
--]]

function MatEval:terminal(s)
   local t = s.terminal
   if (not t) and s.eval then
      t = s.eval() or s.terminal
      s.terminal = t
   end
   if type(t) == "table" then t.scratch = nil end
   return t
end

--[[
## Evaluating assembly operations

The `matrix_adder` function converts an ordinary matrix into a
function to add that matrix to an assembler, if needed.
--]]

local function matrix_adder(m)
   if type(m) == "function" then
      return m
   else
      return function(assembler, c, i, j)
         assembler:add_matrix(m, c, i, j)
      end
   end
end

--[[
A `zero` node could happen because we returned a structurally
zero vector or scalar, but it could also happen because we want
to add a zero block to an assembled object.  Of course, adding
a zero block does not actually do anything.
--]]

function MatEval:zero(s)
   if s.mtype == "matrix" or s.mtype == "rvec" or s.mtype == "cvec" then
      return function(assembler, c, i, j) end
   else
      return 0
   end
end

--[[
Adding a scalar multiple of the identity is a common enough occurrence
that we assume special support in the assembler (an `add_eye` method).
--]]

function MatEval:eye(s)
   return function(assembler, c, i, j)
      assembler:add_eye(s.m, c, i, j)
   end
end

--[[
To assemble sums, differences, and scalar multiples of matrices,
we just make various calls to the assembler to put in the right
contributions.
--]]

function MatEval:add_AA(s,a,b)
   a, b = matrix_adder(a), matrix_adder(b)
   return function(assembler, c, i, j)
      a(assembler, c, i, j)
      b(assembler, c, i, j)
   end
end

function MatEval:sub_AA(s,a,b)
   a, b = matrix_adder(a), matrix_adder(b)
   return function(assembler, c, i, j)
      a(assembler,  c, i, j)
      b(assembler, -c, i, j)
   end
end   

function MatEval:unm_A(s,a)
   a = matrix_adder(a)
   return function(assembler, c, i, j)
      a(assembler, -c, i, j)
   end
end

function MatEval:mul_cA(s,a,b)
   b = matrix_adder(b)
   return function(assembler, c, i, j)
      b(assembler, c*a, i, j)
   end
end

function MatEval:mul_Ac(s,a,b)
   a = matrix_adder(a)
   return function(assembler, c, i, j)
      a(assembler, c*b, i, j)
   end
end

function MatEval:div_Ac(s,a,b)
   a = matrix_adder(a)
   return function(assembler, c, i, j)
      a(assembler, c/b, i, j)
   end
end

--[[
To add a transposed matrix into something to be assembled, 
we think of toggling the `transpose` state before adding the block
(then toggling it again afterward).  The point of the toggle
is that we might want nested transpositions.
--]]

function MatEval:transp_A(s,a)
   a = matrix_adder(a)
   return function(assembler, c, i, j)
      assembler.transpose = not assembler.transpose
      a(assembler, c, j, i)
      assembler.transpose = not assembler.transpose
   end
end

--[[
The `bmatrix` function is the only place we actually make use of the
offset arguments to the assembler!
--]]

local function bmatrix_adder(s,m)
   if type(m) == "function" then
      return m
   else
      local handler_name = 'add_' .. s.mtype
      return function(assembler, c, i, j)
         assembler[handler_name](assembler, m, c, i, j)
      end
   end
end

function MatEval:bmatrix(s, ...)
   local adders = {...}
   for k = 1,#s do adders[k] = bmatrix_adder(s[k], adders[k]) end
   return function(assembler, c, i, j)
      local nrows, ncols
      local k = 1
      local ii = 1
      for ib = 1,s.blockm do
         local jj = 1
         for jb = 1,s.blockn do
            nrows, ncols = s[k].m or 1, s[k].n or 1
            adders[k](assembler, c, i+ii-1, j+jj-1)
            jj = jj + ncols
            k = k+1
         end
         ii = ii + nrows
      end
   end
end

--[[
## Matrix and vector assembly

Assembling a matrix is slightly subtle, just because of how `deal.ii`
separates the matrix contents from the sparsity pattern.  There are
two phases to matrix assembly: first, we build the matrix structure,
and then we build the matrix contents.  If the matrix structure is
known to remain the same even if the matrix contents might change (due
to an updated variable on which the matrix depends), then we can reuse
the matrix structure by setting the symbol `reuse_sparsity` field to true.

There is one important subtlety here that has to do with how object
ownership is managed.  We have to make sure that the sparsity pattern
is available as long as the sparse matrix object is used, but the Lua
garbage collection system does not know this.  Therefore, we attach a
pointer to the sparsity pattern to the matrix object when it is
assembled.

In contrast to the matrix assembly case, assembling a vector is
straightforward.
--]]

function MatEval:assemble(s, f)
   if type(f) ~= "function" then return f end
   if s.mtype == "cvec" or s.mtype == "rvec" then
      local v = SparseV:new( (s.m or 1)*(s.n or 1) )
      local builder = VecBuilder:new(v)
      f(builder,1,1,1)
      builder:delete()
      tolua.takeownership(v)
      return v
   elseif s.mtype == "matrix" then
      local builder = CSCBuilder:new(s.m, s.n)
      f(builder,1,1,1)
      local A = builder:get_matrix()
      builder:delete()
      tolua.takeownership(A)
      return A
   else
      error("Cannot assemble type " .. tostring(s.mtype) .. 
            " in " .. tostring(s))
   end
end

--[[
## UMFPACK factorization

The UMFPACK interface has been overloaded to look the same as the
Sparse interface (i.e. `vmult` and `Tvmult` have reasonable
interpretations in terms of solve operations), so the only support
we need to put UMFPACK into the evaluator is something to actually
compute the factorization.
--]]

function MatEval:UMF_A(s,A)
   local LU = UMFMatrix:new(A)
   tolua.takeownership(LU)
   return LU
end

--[[
## Evaluating slicing operations

For the moment, we do not allow slicing of matrices.  Taking slices
of vectors is fine, though.  I assume a copying slice operation,
so that there is no need to make a clone.
--]]

function MatEval:slice_xI(s,a,b)
   if s.mtype == "scalar" then
      return a(type(b) == "table" and b[1] or b)
   else
      local result = a:slice(b)
      result.scratch = true
      return result
   end
end

MatEval.slice_yI = MatEval.slice_xI

--[[
## Evaluating arithmetic expressions

To evaluate a subtree, we get scratch space for the result (from the
children or via a new allocation), then apply a mutating operation to
compute the new result.  For example, if we add two vectors and the
first vector is itself a scratch result, then the evaluator accumulates
the sum into that scratch vector and passes it on to the parent.
At the top of the evaluation tree, before returning the result to the
user, we make sure it is not still marked as a scratch vector.

Note that matrix expressions should be normalized before evaluation --
I do not say how to form matrix-matrix products here!
--]]

function MatEval:add_xx(s,a,b)  return scratch(a):add(b)      end
function MatEval:sub_xx(s,a,b)  return scratch(a):add(-1,b)   end
function MatEval:unm_x(s,a)     return scratch(a):scale(-1)   end
function MatEval:mul_xc(s,a,b)  return scratch(a):scale(b)    end
function MatEval:mul_cx(s,a,b)  return scratch(b):scale(a)    end
function MatEval:mul_yx(s,a,b)  return a:dot(b)               end
function MatEval:div_xc(s,a,b)  return scratch(a):scale(1/b)  end
function MatEval:transp_x(s,a)  return scratch(a)             end

MatEval.add_yy   = MatEval.add_xx
MatEval.sub_yy   = MatEval.sub_xx
MatEval.unm_y    = MatEval.unm_x
MatEval.mul_yc   = MatEval.mul_xc
MatEval.mul_cy   = MatEval.mul_cx
MatEval.div_yc   = MatEval.div_xc
MatEval.transp_y = MatEval.transp_x

function MatEval:add_cc(s,a,b) return a+b end
function MatEval:sub_cc(s,a,b) return a-b end
function MatEval:unm_c(s,a)    return -a  end
function MatEval:mul_cc(s,a,b) return a*b end
function MatEval:div_cc(s,a,b) return a/b end
function MatEval:transp_c(s,a) return a   end

function MatEval:mul_Ax(s,A,v)
   local result = A:vmult(v)
   result.scratch = true
   return result
end

function MatEval:mul_yA(s,v,A)
   local result = A:Tvmult(v)
   result.scratch = true
   return result
end

function MatEval:default(s)
   error("Unexpected op: " .. s.op)
end

--[[
## Caching evaluator

The caching version of `MatEval` looks the same as the normal
evaluator, except that it processes any incoming expressions into a
DAG and keeps a cache for things that have already been evaluated.
--]]

CMatEval = Class.new_base({}, MatEval)

function CMatEval.new()
   return Class.new(CMatEval, 
      { 
         dagify     = Sym.DAGifier.new(),   -- Tracks common subexpressions
         node_count = {},                   -- Counts repeated nodes
         cache      = {},                   -- Caches node values
         cache_time = {},                   -- Tracks when values were cached
         time       = 0                     -- Time stamp for updates
      })
end

--[[
Before evaluation, we register with the system anything that we might
want to evaluate.  The registration routine keeps track of the number
of times each node appears across all registered computations
(in the `node_counts` table), so that we know which things are worth
caching.  We increment the count of all top-level nodes by one extra,
since we assume that the top-level user may want the value more than once.

Before registering, we type, normalize, and remove common subexpressions,
so that the registered expression is ready to go for cached evaluation.
--]]

function CMatEval:register(s)

   -- Increment a node count
   local function inc(s)
      self.node_count[s] = (self.node_count[s] or 0) + 1 
      return s
   end

   -- Increment the count for a node and all children
   local function reg(s) 
      table.call(inc(s), reg)
      return s
   end

   s = Sym.MatTyper(s)
   s = Sym.MatNormalizer(s)
   s = Sym.CollapseIdent(s)
   s = self.dagify(s)
   return inc(reg(s))

end

--[[
We will only cache nonterminal vector and scalar subexpressions that
are used more than once.  We keep a logical time stamp with each time
entry for the purposes of tracking validity.  If a value to be cached
is an object with a `clone` method, we use that for making copies;
otherwise, we assume it is safe to pass by value.
--]]

function CMatEval:is_cacheable(s)
   return ((self.node_count[s] or 0) > 1 and
           s.op ~= "terminal")
end

function CMatEval:clone(v)
   return (type(v) == "table" and v.clone and v:clone()) or 
      (tolua.type(v) == "SparseV" and v:clone()) or
      v
end

function CMatEval:add_cache(s,v)
   if self:is_cacheable(s) then 
      self.time = self.time + 1
      self.cache_time[s] = self.time
      self.cache[s] = self:clone(v) 
   end
   return v
end

--[[
We may update a value at a terminal; if so, any cached values that
depend on that terminal are invalid.  The `validate` function figures
out the latest time at which any dependency was updated, and kills the
cached value if it was computed before one of the dependencies.
--]]

function CMatEval:update(s)
   self.time = self.time + 1
   self.cache_time[s] = self.time
end

function CMatEval:validate(s)
   local time = self.cache_time[s] or 0
   for i,t in ipairs(s) do
      time = math.max(time, self:validate(t))
   end
   if self.cache_time[s] and self.cache_time[s] < time then
      self.cache[s] = nil
   end
   return time
end

--[[
When we evaluate a node, the result may or may not already be in
the cache.  If it is in the cache, we make a clone and return it.
Otherwise, we evaluate using the basic evaluation routine and save
a copy of the result to cache if that is appropriate.  The call
metamethod is meant for top-level use -- it checks the validity of
cached values before evaluating.
--]]

function CMatEval:eval(s)
   return self.cache[s] or self:add_cache(s,MatEval.eval(self,s))
end

function CMatEval:__call(s)
   self:validate(s)
   return self.cache[s] or self:add_cache(s,MatEval.__call(self,s))
end

--[[
The `future` method returns a function that can be used to
compute a value for the given symbol later.
--]]

function CMatEval:future(s)
   s = self:register(s)
   return function() return self(s) end
end
