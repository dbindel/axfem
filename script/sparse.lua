--ldoc
--[[
% Lua sparse extensions

The C++ sparse matrix class naturally does no dynamic allocation of result
vectors.  For use in the Lua wrappers, it is convenient to add support for
dynamic allocation of garbage-collected result vectors.

The basic routines are `clone` and `vec`.  Respectively, these clone
an existing vector and create a new vector of a given size.  The `vec`
routine is local, and is only used to allocate scratch space for matrix
vector multiplies.
--]]

local SparseV_new = SparseV.new
function SparseV:new(arg)
   if type(arg) == "table" then
      local v = SparseV_new(SparseV, #arg)
      SparseV_copy(v, #arg, arg)
      return v
   else
      return SparseV_new(SparseV, arg)
   end
end

function SparseV:clone()
   local result = SparseV:new(self)
   tolua.takeownership(result)
   return result
end

local function vec(n)
   local result = SparseV:new(n)
   tolua.takeownership(result)
   return result
end

--[[
The `slice` routine is an extension to pull out a subset of a vector
associated with a given index range.  For the moment, we define slices
as contiguous ranges given by a start and end index.  We also define
a getter for individual elements using the `call` metamethod.
--]]

function SparseV:slice(idx)
   local result = SparseV_slice(self, idx[1], idx[2])
   tolua.takeownership(result)
   return result
end

function SparseV:__call(idx)
   return SparseV_element(self, idx)
end

--[[
We also add a layer around `add` and `scale` so that they return
the result.
--]]

local sv_add   = SparseV.add
function SparseV:add(...)
   sv_add(self, ...)
   return self
end

local sv_scale = SparseV.scale
function SparseV:scale(s)
   sv_scale(self, s)
   return self
end
