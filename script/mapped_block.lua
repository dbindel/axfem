--ldoc
--[[
% Mapped blocks

A mapped block is the image of a regular mesh of $[-1,1]^2$ under a
positively-oriented smooth mapping from the plane to itself.  The
`mapped_block` function produces the vertices in the mapped mesh, the
cell connectivity, and a ChebFun representation of the parameterized
boundary curve.

--]]
function mapped_block(p)

--[[
The `mapped_block` function takes the following named arguments
 (passed in the `p` table argument).
--]]   
local phi = p.phi or function(x,y) return x,y end  -- mapping function
local mat = p.material    -- material identifier
local m   = p.m   or 10   -- number of divisions in xi
local n   = p.n   or 10   -- number of divisions in eta
local mc  = p.mc  or 15   -- degree of Cheb approx of horizontal boundaries
local nc  = p.nc  or 15   -- degree of Cheb approx of vertical boundaries

--[[
## Correcting vertex ordering 

If the provided mapping is negatively oriented, we re-orient by composing
with a reflection.
--]]
local d = 1e-2
local x0,y0 = phi(0,0)
local x1,y1 = phi(d,0)
local x2,y2 = phi(0,d)
if (x1-x0)*(y2-y0) < (x2-x0)*(y1-y0) then
   local phi0 = phi
   function phi(x,y) return phi0(-x,y) end
end

--[[
## Mapped vertices

The vertices appear in a row-major order (in the parent domain) with
a one-based indexing scheme.  Note that there are actually $m+1$ nodes
per row and $n+1$ per column, corresponding to $m$ elements and $n$ elements
per row/column.

--]]   
local pts = {}
for j = 0,n do
   for i = 0,m do
      local x,y = phi(2*i/m-1, 2*j/n-1)
      pts[j*(m+1)+i+1] = {x=x,y=y}
   end
end

--[[
## Cell connectivity

The cell corners are labeled as `bl`, `br`, `tl`, and `tr` for bottom/top
and left/right (in the parent domain).

--]]
local cell = {}
for j = 1,n do
   for i = 1,m do
      local nw = j*(m+1)+i+1
      cell[(j-1)*m+i] = {
         bl = nw-m-2, 
         br = nw-m-1,
         tr = nw, 
         tl = nw-1,
         matid = mat }
   end
end

--[[
## Boundary parameterizations

The `make_curve` function builds a representation of a parameterized
curve $(x(s), y(s))$ based on polynomial interpolation at Chebyshev
nodes.  This should be very accurate for smooth curves.
--]]
local boundaries = {
   ChebBoundary:new(function(s) return phi(s, -1) end, mc), -- bottom
   ChebBoundary:new(function(s) return phi(s,  1) end, mc), -- top
   ChebBoundary:new(function(s) return phi(-1, s) end, nc), -- left
   ChebBoundary:new(function(s) return phi( 1, s) end, nc)  -- right
}

--[[
## Combined data structure

At the end, the data structure returned by `mapped_block` consists of
a vertex list (`vertices`), a connectivity array (`cell_info`),
and a list of four boundary indicator functions (`boundaries`).

--]]
return { vertices = pts, 
         cell_info = cell, 
         boundaries = boundaries }

--ldoc   
end
