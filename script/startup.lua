--
-- The startup.lua script requires this immediately, so it can be
-- used to load modules that are always to be used.
--

require "util"
require "sym"
require "sparse"
require "mvbuilder"
require "mateval"
require "problem"
require "solver"
