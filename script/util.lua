--ldoc
--[[
% General utilities

# Math library import

We use `sqrt` and `cos` frequently enough to want to avoid always
writing `math.sqrt` and `math.cos`.
--]]

table.foreach(math, function(k,v) _G[k] = v; end)

--[[
# Formatted output

The `printf` and `fprintf` commands give us C-style output formatting.
--]]

function printf(...)
   print(string.format(unpack(arg)))
end

function fprintf(fp, ...)
   fp:write(string.format(unpack(arg)))
end

--[[
# Table utilities

The map, transform, call, any, and all routines provide some basic
table processing methods that are missing from Lua 5.  Each of these
functions takes a function of one argument that is applied to each
table element in turn.  A table can be supplied instead, in which case,
we do table lookup; or, if the argument is nil, the identity function
is used.
--]]

local function make_filter(fn)
   if not fn then
      return function(v) return v end
   elseif type(fn) == "table" and not fn.__call then
      return function(v) return fn[v] end
   else
      return fn
   end
end

function table.map(tbl, fn)
   local result = {}
   fn = make_filter(fn)
   for i,t in ipairs(tbl) do
      result[i] = fn(t)
   end
   return result
end

function table.transform(tbl, fn)
   fn = make_filter(fn)
   for i,t in ipairs(tbl) do
      tbl[i] = fn(t)
   end
   return tbl
end

function table.call(tbl, fn)
   for i,t in ipairs(tbl) do 
      fn(t) 
   end
end   

function table.any(tbl, fn)
   local result = nil
   fn = make_filter(fn)
   for i,t in ipairs(tbl) do
      result = result or fn(t)
   end
   return result
end

function table.all(tbl, fn)
   local result = true
   fn = make_filter(fn)
   for i,t in ipairs(tbl) do
      result = result and fn(t)
   end
   return result
end

--[[
# Metatable-based classes

Lua tables can have attached metatables that are responsible for fallback
metamethod lookups.  Chaining together metatables is a common way of 
implementing object-oriented programming in Lua.  We define a `new_base`
method for things that we mean to treat as base classes for inheritance,
and `new` for new instances when we do not mean to inherit.
--]]

Class = {}

function Class.new_base(tbl, parent)
   tbl = tbl or {}
   tbl.__index = tbl
   if parent then setmetatable(tbl, parent) end
   return tbl
end

function Class.new(parent, tbl)
   tbl = tbl or {}
   setmetatable(tbl, parent)
   return tbl
end

function Class.is(tbl, class)
   return tbl and (getmetatable(tbl) == class or 
                   Class.is(getmetatable(tbl), class))
end

--[[
# Simple timers

The `tic` and `toc` commands in MATLAB are helpful for simple timing.
Here, we replicate those commands -- including the somewhat limiting
fact that they refer to a single global clock for the system.
--]]

local tic_clock = 0

function tic()
   tic_clock = os.clock()
end

function toc(msg)
   local result = os.clock()-tic_clock
   if not msg then
      printf('Time: %g', result)
   else
      printf('%s: %g', msg, result)
   end
   return result
end


--[[
# Profiling instrumentation

In addition to the simple `tic`/`toc` timer, we support two other types of
timing methods.  The `ftime` command takes a function and a message string,
and returns a wrapped function that times a call to the input function and
prints a timing message on each call.  Other than printing a timing message,
the wrapped function should have the same behavior as the original.
--]]

function ftime(f, msg)
   return function(...)
      local t0 = os.clock()
      local result = {f(unpack(arg))}
      local t1 = os.clock()
      printf('%s: %g', msg, t1-t0)
      return unpack{result}
  end
end

--[[
Where `ftime` wraps a given function so that it prints timing information
on each call, `fprofiler` wraps a function so that it records the time taken
for all calls.  Each call to `fprofiler` takes a function to be wrapped and
a tag.  The tag is used to identify the function for recording and reporting.
The relationship between tags and wrapped functions need not be one-to-one;
it is possible to assign several functions to a single tag or to wrap a single
function with multiple tags.
--]]

local profiler_tags  = {}
local profiler_times = {}

function fprofiler(f, tag)
   local tagnum = profiler_tags[tag]
   if not tagnum then
      tagnum = #profiler_times+1
      profiler_times[tagnum] = {tag = tag, ncall = 0, tcall = 0}
   end
   return function(...)
      local t0 = os.clock()
      local result = {f(unpack(arg))}
      local t1 = os.clock()
      profiler_times[tagnum].ncall = profiler_times[tagnum].ncall+1 
      profiler_times[tagnum].tcall = profiler_times[tagnum].tcall+(t1-t0)
      return unpack(result)
   end
end

--[[
The `print_profile` command writes a formatted record of the timing
data for each call to a function wrapped by `fprofiler`.  The output
only contains records for those functions that have actually been
called.  The records are printed in the order in which the functions
were registered with `fprofiler`.
--]]

function print_profile(fp)
   for i,r in ipairs(profiler_times) do
      if r.ncall > 0 then
         if fp then
            fprintf(fp, '%35s % 6d calls % 8.2f s\n', r.tag, r.ncall, r.tcall)
         else
            printf('%35s % 6d calls % 8.2f s', r.tag, r.ncall, r.tcall)
         end
      end
   end
end
