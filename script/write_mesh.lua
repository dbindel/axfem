
function write_msh(block, filename)
   fout = io.open(filename,"w")

   fout:write("$MeshFormat\n")
   fout:write("2.2 0 8\n")
   fout:write("$EndMeshFormat\n")

   fout:write("$Nodes\n")
   fout:write(#block.vertices,"\n")
   for k,v in ipairs(block.vertices) do
      fout:write(k," ",v.x," ",v.y," 0\n")
   end
   fout:write("$EndNodes\n")

   fout:write("$Elements\n")
   fout:write(#block.cell_info,"\n")
   for k,v in ipairs(block.cell_info) do 
      fout:write(k," 3 0 ")
      fout:write(v.bl," ",v.br," ",v.tr," ",v.tl,"\n")
   end
   fout:write("$EndElements\n")
 
   fout:close()
end



function write_ucd(block, filename)
   fout = io.open(filename,"w")
   fout:write(#block.vertices, " ", #block.cell_info," 0 0 0\n") 
   for k,v in ipairs(block.vertices) do
      fout:write(k," ",v.x," ",v.y," 0\n")
   end

   for k,v in ipairs(block.cell_info) do 
      fout:write(k," ", v.matid.id," quad ")
      fout:write(v.bl," ",v.br," ",v.tr," ",v.tl,"\n")
   end
 
   fout:close()
end


