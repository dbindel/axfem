require "util"

Sym = {}

--ldoc
--[[ =================================================================
% Symbolic expression package
% David Bindel
--]]

--[[ =================================================================
# Symbolic representations in Lua

This module provides a framework for transparently building abstract
syntax trees, and for doing simple walks over those trees.  The
purpose is to provide some basic symbolic algebra capabilities hosted
in Lua.

## Abstract syntax tree nodes

An abstract syntax tree node (`ASTNode`) is a table with an `op` field
that describes a type of node in a symbolic expression tree
(`terminal`, `add`, `mul`, etc), and indexed fields for child nodes.
--]]

local ASTNode = Class.new_base()

function Sym.new(op, ...)
   local result = (type(op) == "table") and op or { op=op, ... }
   return Class.new(ASTNode, result)
end

--[[ 
Terminal symbols have the `op` field set to `terminal`, and a
`terminal` field that contains any information about the terminal
(such as a value or name).
--]]

function Sym.new_term(s)
   local result = Sym.new("terminal")
   result.terminal = s
   return result
end

function Sym.new_terms(...)
   return unpack(table.map({...}, Sym.new_term))
end

function Sym.cast(s)
   return (Class.is(s, ASTNode) and s) or Sym.new_term(s)
end

function Sym.casts(argtbl)
   return unpack(table.map(argtbl, Sym.cast))
end

--[[
We overload the standard arithmetic operations so that arithmetic on
symbols yields a symbolic parse tree for the corresponding expression.
--]]

local ops = {"add", "sub", "mul", "div", "pow"}
for i,op in ipairs(ops) do 
   ASTNode["__" .. op] = function(a,b) 
      return Sym.new(op, Sym.cast(a), Sym.cast(b))
   end
end

function ASTNode:__unm()
   return Sym.new("unm", self)
end

function ASTNode.__call(...) 
   return Sym.new("call", Sym.casts{...})
end

--[[
## Processing ASTs

The `TreeProcessor` object encapsulates the standard operations on a
tree: transforming a tree or evaluating a tree.  The object has named
methods for nodes that require special handling; if no specific named
method is available, we fall back to the `default` handler.  The `eval`
operation actually runs the tree processor on a node.
--]]

local TreeProcessor = Class.new_base()

function TreeProcessor:__call(...)
   return self:eval(...)
end

function TreeProcessor:default(s)
   return s
end

--[[
There are three standard `eval` functions:

1. Simply look up the handler and call it.

2. Transform the tree by calling `eval` on each child, and replacing
   the child with its `eval` result.  Then call the handler on the
   current function.

3. Evaluate the tree by calling `eval` on each child, then calling
   the handler for the current node and passing the child values as
   arguments.  In this case, the tree need not be explicitly modified.

In the latter two cases, we look up the handler *after* we process
the children, since processing the children may change the node in a
way that changes the dispatch behavior.
--]]

local function simple_lookup(self,s)
   return self[s.op] or self.default 
end

function eval_op(op, lookup)
   lookup = lookup or simple_lookup
   local ops = {
      simple = function(self,s)
         local f = lookup(self,s)
         return f(self,s)
      end,
      transform = function(self,s)
         table.transform(s, function(t) return self:eval(t) end)
         local f = lookup(self,s)
         return f(self,s)
      end,
      evaluate = function(self,s)
         local vals = table.map(s,function(v) return self:eval(v) end)
         local f = lookup(self,s)
         return f(self,s,unpack(vals))
      end
   }
   return ops[op]
end
   
function Sym.new_pass(op,lookup)
   return Class.new_base({ eval = eval_op(op,lookup) }, TreeProcessor)
end

--[[
## String representations of ASTs

The simplest tree processor creates flattened representations for
parse trees.
--]]

local Stringify = Sym.new_pass "evaluate"

function Stringify:terminal(s) 
   return tostring(s.terminal)
end

function Stringify:default(s, ...)
   return "(" .. s.op .. " " .. table.concat({...}, " ") .. ")"
end

function ASTNode:__tostring() 
   return Stringify(self) 
end

--[[ =================================================================
# Matrix expression trees

The `matsym` package adds support for matrix-vector expressions to the
general symbolic package.  In order to interpret matrix-vector
operations, we need to know about the types of the expressions
involved, both in terms of the basic shape (matrix, scalar, row, or
column vector) and the dimensions.  The shape is important: we do
*not* consider an n-by-1 matrix to be the same as an n-by-1 column
vector, since the underlying data structures may be different!  We
therefore decorate matrix expressions with three additional fields:

* `mtype`: May be `matrix`, `scalar`, `rvec`, `cvec`, or `index`
* `m`: Number of rows
* `n`: Number of columns


## Matrix terminals

In addition to the usual constructors for AST leaf nodes, we add
specialized constructors for matrix and vector terminals and for
identity and zero matrices.  We also override the default string
conversion for these nodes so that we can see what the dimensions are.
--]]

function Sym.new_mat(name,m,n)
   return Sym.new{ op="terminal", terminal=name,
                   mtype="matrix", m=m, n=(n or m) }
end

function Sym.new_vec(name,m)
   return Sym.new{ op="terminal", terminal=name, mtype="cvec", m=m }
end

function Sym.new_rvec(name,n)
   return Sym.new{ op="terminal", terminal=name, mtype="rvec", n=n }
end

function Sym.new_index(name,m)
   return Sym.new{ op="terminal", terminal=name, mtype="index", m=m }
end

function Sym.eye(n)
   return Sym.new{ op="eye", mtype="matrix", m=n, n=n }
end

function Stringify:eye(s)
   return "eye[" .. (s.m or "?") .. "]"
end

function Sym.zero(m,n,mtype)
   local result = Sym.new{ op="zero", m=m, n=n or m }
   result.mtype = (mtype or
                   result.m > 1 and result.n > 1 and "matrix" or
                   result.m > 1 and "cvec" or
                   result.n > 1 and "rvec" or
                   "scalar")
   return result
end

function Stringify:zero(s)
   return "zero[" .. (s.m or "?") .. "," .. (s.n or "?") .. "]"
end

--[[
## Operations on matrices

In addition to the usual arithmetic operations, we can concatenate
matrices to build block matrices.  We override the usual string processing
on these nodes so that we can see the number of block rows and columns.
The `assemble` operation builds a concrete version of a block representation,
which may otherwise be kept in an abstract form.

In addition, we sometimes would like to transpose matrices or vectors.
--]]

function Sym.bmatrix(blockm, blockn, A)
   if type(blockm) == "table" then
      local AA = blockm
      blockm, blockn = #AA, #(AA[1])
      A = {}
      for i = 1,blockm do
         for j = 1,blockn do
            A[j+(i-1)*blockn] = AA[i][j]
         end
      end
   end
   local result = Sym.new("bmatrix", Sym.casts(A))
   result.blockm = blockm
   result.blockn = blockn
   function result:get(i,j)    return self[j+(i-1)*self.blockn]  end
   function result:set(i,j,x)  self[j+(i-1)*self.blockn] = x     end
   return result
end

function Stringify:bmatrix(s, ...)
   return "(bmatrix[" .. 
      (s.blockm or "?") .. "," .. 
      (s.blockn or "?") .. "] " ..
      table.concat({...}, " ") .. ")"
end

function Sym.hcat(...)
   local syms = {...}
   return Sym.bmatrix(1, #syms, syms)
end

function Sym.vcat(...)
   local syms = {...}
   return Sym.bmatrix(#syms, 1, syms)
end

function Sym.assemble(s)
   return Sym.new("assemble", Sym.cast(s))
end

function Sym.transp(s) 
   return Sym.new("transp", Sym.cast(s)) 
end


--[[
## Typed operation lookup

Once the nodes in an AST have been tagged by type, we can use those
types to write tree processors without a ton of if statements.  The
lookup rule (a la `mat_lookup`) is that we first try to use a tagged
operation name (e.g. `mul_AA` for multiplying two matrices, or `mul_xy`
for computing an outer product of a column and a row); if that fails,
we fall back to just the operation name; and if that fails, we use the
`default` method.
--]]

local type_tags = { matrix = "A", scalar = "c",
                    cvec = "x", rvec = "y", 
                    index="I" }
local function type_tag(s) return type_tags[s.mtype] end

function Sym.mat_lookup(self,s)
   local op = s.op .. "_" .. table.concat(table.map(s, type_tag))
   return self[op] or self[s.op] or self.default
end

--[[
## Assigning matrix type information

The `MatTyper` object transforms an AST by decorating it with matrix
type and dimension information.  Keeping in mind that some matrix tree
transformers may want to retype a few nodes that they rewrite in an
already-typed tree, we also add an `eval_node` function that types just
a single node, assuming the children are already correctly typed.
--]]

local MatTyper = Sym.new_pass("transform", Sym.mat_lookup)

function MatTyper:eval_node(s)
   local f = Sym.mat_lookup(self,s)
   return f(self,s)
end

--[[
Frequently, the type of a parent node is inherited from one of the
children (e.g. in almost any operation involving a scalar), so we
encapsulate that copy operation in `mtype_copy`.  The `mtype_error`
function reports a semi-legible error message when typing fails.
--]]

local function mtype_copy(s,a)
   s.mtype, s.m, s.n = a.mtype, a.m, a.n
   return s
end

local function mtype_error(s)
   error("Bad types: (" .. s.op .. " " .. 
         (s[1].mtype or "ERROR") .. " " .. 
         (s[2].mtype or "ERROR").. ") in " .. 
         tostring(s))
end

--[[ 
Addition and subtraction can only occur between identical types and
sizes.  For the moment, we do not allow the convention that vector +
scalar should be interpreted componentwise, for example.  This may
change in the future.
--]]

local function addsub(self, s)
   if s[1].mtype ~= s[2].mtype then mtype_error(s) end
   if (s[1].m or 1) ~= (s[2].m or 1) or 
      (s[1].n or 1) ~= (s[2].n or 1) then
      error("Incompatible sizes in " .. s.op .. ": " ..
            (s[1].m or 1) .. "x" .. (s[1].n or 1).. " vs " ..
            (s[2].m or 1) .. "x" .. (s[2].n or 1))
   end
   return mtype_copy(s, s[1])
end
MatTyper.add = addsub
MatTyper.sub = addsub

function MatTyper:unm(s) return mtype_copy(s,s[1]) end

--[[
There are two types of multiplication operations: scalar multiplication
and matrix-matrix multiplication.  The latter has many special flavors,
including matrix-vector products and inner and outer products, but in all
cases the basic dimension checks are the same.
--]]

local function matmul_type(s,mtype)
   assert(s[1].n == s[2].m, "Incompatible matrix-matrix multiply")
   s.mtype, s.m, s.n = mtype, s[1].m, s[2].n
   return s
end

function MatTyper:mul_AA(s) return matmul_type(s, "matrix") end
function MatTyper:mul_xy(s) return matmul_type(s, "matrix") end
function MatTyper:mul_Ax(s) return matmul_type(s, "cvec")   end
function MatTyper:mul_yA(s) return matmul_type(s, "rvec")   end
function MatTyper:mul_yx(s) return matmul_type(s, "scalar") end

function MatTyper:mul(s)
   if     s[1].mtype == "scalar" then return mtype_copy(s,s[2])
   elseif s[2].mtype == "scalar" then return mtype_copy(s,s[1])
   else                               mtype_error(s)
   end
end

--[[
We only allow division by scalars.  Division is not overloaded to
refer to applying a left or right matrix inverse, for example.
--]]

function MatTyper:div(s)
   if s[2].mtype == "scalar" then return mtype_copy(s,s[1])
   else                           mtype_error(s)
   end
end

--[[
We can take powers of a square matrix or a scalar, but the exponent
must always be a scalar.
--]]

function MatTyper:pow_Ac(s)
   assert(s[1].m == s[1].n, "Cannot take rectangular powers")
   return mtype_copy(s,s[1])
end

function MatTyper:pow_cc(s) return mtype_copy(s,s[1]) end
function MatTyper:pow(s)    mtype_error(s)            end

--[[
In order to keep life simple, we assume that during concatenation, a
literal 0 may mean a zero scalar, row vector, column vector, or
matrix -- whichever is the first one in that list that makes sense
dimensionally.  Because of this, this ends up being by far the most
complicated operation the typer does.  We break up the functionality
into three steps.  The `bmatrix_dims` function figures out what the
block dimensions should be given the rules about zeros; 
the `bmatrix_check_entries` function makes sure that all the block entries
are compatible with the inferred types; and `bmatrix_set_type` computes
the overall dimensions and type of the object.
--]]

local function bmatrix_dims(s)
   local ms = {}
   local ns = {}
   for i=1,s.blockm do
      for j=1,s.blockn do
         local Aij = s:get(i,j)
         ms[i] = math.max(ms[i] or 0, Aij.m or 1)
         ns[j] = math.max(ns[j] or 0, Aij.n or 1)
      end
   end
   return ms, ns
end

local function bmatrix_check_entries(s, ms, ns)
   for i=1,s.blockm do
      local mi = ms[i]
      for j=1,s.blockn do
         local nj = ns[j]
         local Aij = s:get(i,j)
         if Aij.op == "terminal" and Aij.terminal == 0 then
            s:set(i,j,Sym.zero(mi,nj))
         else
            assert(Aij and (Aij.m or 1) == mi and (Aij.n or 1) == nj,
                   "Wrong dimensions for " .. tostring(Aij))
         end
      end
   end
end

function bmatrix_set_type(s, ms, ns)
   s.m, s.n = 0, 0
   for i=1,s.blockm do s.m = s.m + ms[i] end
   for j=1,s.blockn do s.n = s.n + ns[j] end
   local mat_flag = table.any(s, function(t) return t.mtype == "matrix" end)
   if     s.m == 1 and not mat_flag then s.mtype = "rvec"
   elseif s.n == 1 and not mat_flag then s.mtype = "cvec"
   else                                  s.mtype = "matrix"
   end
end

function MatTyper:bmatrix(s)
   local ms, ns = bmatrix_dims(s)
   bmatrix_check_entries(s, ms, ns)
   bmatrix_set_type(s, ms, ns)
   return s
end

--[[
The `assemble` operation looks like an identity for the purposes of
the type checker.
--]]

function MatTyper:assemble(s)
   return mtype_copy(s,s[1])
end

--[[
A `slice` operation pulls out variables associated with an index set.
A length-one slice of a row or column vector returns a scalar;
similarly, a slice out of a matrix in which one of the index sets has
length one will give a vector or scalar.
--]]

function MatTyper:slice_yI(s)
   if s[2].m == 1 then
      s.op, s.mtype = "slice", "scalar"
   else
      s.op, s.mtype, s.n = "slice", "rvec", s[2].m
   end
   return s
end

function MatTyper:slice_xI(s)
   if s[2].m == 1 then
      s.op, s.mtype = "slice", "scalar"
   else
      s.op, s.mtype, s.m = "slice", "cvec", s[2].m
   end
   return s
end

function MatTyper:slice_AII(s)
   if s[2].m == 1 and s[3].m == 1 then
      s.op, s.mtype = "slice", "scalar"
   elseif s[2].m == 1 then
      s.op, s.mtype, s.n = "slice", "rvec", s[3].m
   elseif s[3].m == 1 then
      s.op, s.mtype, s.m = "slice", "cvec", s[2].m
   else
      s.op, s.mtype, s.m, s.n = "slice", "matrix", s[2].m, s[3].m
   end
   return s
end

--[[
A `call` with index arguments is treated as a slicing operation.
--]]

MatTyper.call_yI  = MatTyper.slice_yI
MatTyper.call_xI  = MatTyper.slice_xI
MatTyper.call_AII = MatTyper.slice_AII

--[[
Transposing a row gives us a column, and vice versa.  Matrices and
scalars remain matrices and scalars after transposition.
--]]

local transp_type = { matrix="matrix", scalar="scalar", 
                      cvec="rvec", rvec="cvec" }

function MatTyper:transp(s)
   local a = s[1]
   s.mtype, s.m, s.n = transp_type[a.mtype], a.n, a.m
   return s
end

--[[
The argument to an identity matrix constructor should be a literal
number at typing time; otherwise, I do not know how to set the size.
Terminals are assumed to be scalars unless otherwise stated.
--]]

function MatTyper:terminal(s)
   s.mtype = s.mtype or "scalar"
   assert(s.mtype == "scalar" or s.mtype == "rvec" or s.m,
          "Missing row count for " .. s.mtype .. " terminal")
   assert(s.mtype == "scalar" or s.mtype == "cvec" or 
          s.mtype == "index" or s.n,
          "Missing col count for " .. s.mtype .. " terminal")
   return s
end

function MatTyper:eye(s)
   s.mtype = "matrix"
   return self:terminal(s)
end

function MatTyper:zero(s)
   return self:terminal(s)
end

--[[
We complain when we do not recognize things 
(e.g. a call with non-slice arguments)
--]]

function MatTyper:default(s)
   error("Could not type: " .. tostring(s))
end

--[[ =================================================================
# Matrix expression normalization

In many cases, matrices may not be available in explicit form; the
only thing one can do is matrix-vector muliplications.  In other cases,
it is possible to add or multiply pairs of matrices, but it is expensive.
Either way, it is useful to normalize the form of expressions that
evaluate to scalars or vectors so that whenever a matrix appears in
an intermediate computation, it is immediately applied to a vector.

We do not currently normalize slicing operations.  This would be a
straightforward extension (assuming that we had some sort of
complementary scattering operation), but the utility is not clear to me.

## Symbolic matrix-vector multiplies

There are two basic processes for normalization: applying a matrix
expression to a row vector (`rmul`) and applying a matrix expression
to a column vector (`cmul`).
--]]

local mul  = Class.new_base()
local cmul = Class.new(mul)
local rmul = Class.new(mul)

local function mtype_node(s) return MatTyper:eval_node(s) end

function mul.__call(self, s,v)
   local f = Sym.mat_lookup(self,s)
   return mtype_node(f(self,s,v))
end

--[[
Adding, subtracting, and scaling look the same whether we're dealing
with rows or with columns.  For that matter, so does taking powers.
--]]

function mul:add_AA(s,v)  return self(s[1],v) + self(s[2],v) end
function mul:sub_AA(s,v)  return self(s[1],v) - self(s[2],v) end
function mul:unm_A(s,v)   return -self(s[1],v)               end
function mul:mul_cA(s,v)  return s[1] * self(s[2],v)         end
function mul:mul_Ac(s,v)  return s[2] * self(s[1],v)         end
function mul:div_Ac(s,v)  return self(s[1],v) / s[2]         end

function mul:pow_Ac(s,v)
   local n = s[2].terminal
   assert(s[2].op == "terminal" and type(n) == "number",
          "Cannot normalize abstract power expressions")
   local result = v
   for i=1,n do result = self(s[1],result) end
   return result
end

--[[
Applying matrix-matrix products, outer products, or transposed
operators is different depending on whether we apply to a row or a
column vector.
--]]

local function T(v)         return mtype_node(Sym.transp(v))   end
function cmul:default(s,v)  return s*v                         end
function rmul:default(s,v)  return v*s                         end
function cmul:mul_AA(s,v)   return self(s[1], self(s[2], v))   end
function rmul:mul_AA(s,v)   return self(s[2], self(s[1], v))   end
function cmul:mul_xy(s,v)   return s[1]*mtype_node(s[2]*v)     end
function rmul:mul_xy(s,v)   return mtype_node(v*s[1])*s[2]     end
function cmul:transp_A(s,v) return T(rmul(s[1],T(v)))          end
function rmul:transp_A(s,v) return T(cmul(s[1],T(v)))          end

--[[
There is only one tricky part of the symbolic multiplication, and that
has to do with block matrices.  We only allow conformal block expressions
to be normalized, so an expression like `[A, 0; 0, 1]*v` is not allowed 
if `v` is a terminal, even if the dimensions line up.
--]]

local function addto(result, term)
   return result and mtype_node(result+term) or term
end

function cmul:bmatrix(s,v)
   assert(v.op == "bmatrix" and s.blockn == v.blockm, 
          "Cannot apply bmatrix to block incompatible type")
   local result = {}
   for i=1,s.blockm do
      for j=1,s.blockn do
         result[i] = addto( result[i], cmul(s:get(i,j), v:get(j,1)) )
      end
   end
   return Sym.bmatrix(s.blockm,1,result)
end
   
function rmul:bmatrix(s,v)
   assert(v.op == "bmatrix" and v.blockn == s.blockm, 
          "Cannot apply bmatrix to block incompatible type")
   local result = {}
   for j=1,s.blockn do
      for i=1,s.blockm do
         result[j] = addto( result[j], rmul(s:get(i,j), v:get(1,i)) )
      end
   end
   return Sym.bmatrix(1,s.blockn,result)
end

--[[
## The normalization pass

The `MatNormalizer` object walks over the tree in order to produce a
normalized form in which matrix operations in scalar or vector
subexpressions only ever appear in products with vectors.
--]]

local MatNormalizer = Sym.new_pass("transform", Sym.mat_lookup)
function MatNormalizer:mul_Ax(s)  return cmul(s[1],s[2])  end
function MatNormalizer:mul_yA(s)  return rmul(s[2],s[1])  end

--[[ =================================================================
# Simplifying algebraic identities

## Collapsing identity elements

The `CollapseIdent` pass removes multiply-by-one (or
multiply-by-identity), multiply-by-zero, add zero, stacked transposes,
and transposes of scalars.
--]]

local CollapseIdent = Sym.new_pass "transform"

local function is_ident(s)
   return s.op == "eye" or s.op == "terminal" and s.terminal == 1
end

local function is_zero(s)
   return s.op == "zero" or s.op == "terminal" and s.terminal == 0
end

function CollapseIdent:add(s)
   if     is_zero(s[1]) then return s[2]
   elseif is_zero(s[2]) then return s[1]
   else   return s
   end
end

function CollapseIdent:mul(s)
   if     is_ident(s[1]) then return s[2]
   elseif is_ident(s[2]) then return s[1]
   elseif is_zero(s[1]) or is_zero(s[2]) then
      if s.mtype and s.mtype ~= "scalar" then
         return Sym.zero(s.m or 1, s.n or 1, s.mtype)
      else
         return Sym.cast(0)
      end
   else
      return s
   end
end

function CollapseIdent:transp(s)
   if s[1].op == "transp" then 
      return s[1][1]
   elseif s.mtype == "scalar" then
      return s[1]
   else
      return s
   end
end

--[[
## Identifying common subexpressions

The `SubexprId` object identifies equivalent subexpressions.  The node
methods produce lists of keys for any given subexpression, and two
nodes with overlapping key lists are assumed to be equivalent.  To
avoid namespace conflicts due to funny terminal names, we use the
convention that internal nodes map to string keys and terminals map to
integer keys.  To keep track of all of this, the `SubexprId` object
maintains four mappings.

1.  From terminals to integer keys
2.  From keys to integer class identifiers
3.  From class identifiers to representatives (in `self`)
4.  From expression nodes to class representatives (in `self`)

The mappings to class representatives are kept in the object itself.
--]]

local SubexprId = Class.new_base()

function SubexprId.new()
   return Class.new(SubexprId, 
      { 
         key_id = {},       -- Map keys to integer class IDs
         terminal_id = {},  -- Map terminals to integer keys
         num_terminals = 0  -- Counter for terminal key assignment
      })
end

--[[
When called as a function, the object returns a class identifier.  We
check the `self` map first to see if the node has already been
processed, then check if any of the keys associated with the node map
to an existing class.  If neither of those conditions hold, this is
the first member of a new equivalence class.
--]]

function SubexprId:__call(s) 
   if self[s] then return self[s] end
   local f = Sym.mat_lookup(self,s)
   local keys = {f(self,s)}
   id = table.any(keys, self.key_id)
   if not id then
      table.insert(self,s)
      id = #self
      table.call(keys, function(k) self.key_id[k] = id end)
   end
   self[s] = id
   return id
end

--[[
The keys for `zero`, `eye`, and `bmatrix` nodes need to include
dimension (or block dimension) information.  Otherwise, we would end
up saying different-sized zero matrices are equivalent, for instance.
--]]

function SubexprId:zero(s)  return tostring(s)  end
function SubexprId:eye(s)   return tostring(s) end

function SubexprId:bmatrix(s)
   local op = "bmatrix[" .. s.blockm .. "," .. s.blockn .. "] "
   return "(" .. op .. " " .. table.concat(table.map(s, self), " ") .. ")"
end

--[[
Terminals with the same `terminal` field are assumed to be equivalent,
unless that field is `nil`.  If the field is `nil`, it is assumed not
to be equivalent to anything else, a feature which may be used to define
globally unique terminals.
--]]

function SubexprId:terminal(s)
   local id = s.terminal and self.terminal_id[s.terminal]
   if not id then
      id = self.num_terminals + 1
      self.num_terminals = id
      if s.terminal then 
         self.terminal_id[s.terminal] = id 
      end
   end
   return id
end

--[[
We identify any other nodes by the operator name and the identifiers
of the children.
--]]

function SubexprId:default(s)
   return "(" .. s.op .. " " .. table.concat(table.map(s,self), " ") .. ")"
end

--[[
Once we have identified equivalent subexpressions, we can rewrite
the tree to be a DAG by replacing each reference to a subexpression
node with a reference to the representative for the equivalence class
to which it belongs.
--]]

local DAGifier = Class.new_base()

function DAGifier.new(ids)
   return Class.new(DAGifier, { ids = ids or SubexprId.new() })
end

function DAGifier:__call(s)
   table.transform(s, self)
   return self.ids[self.ids(s)]
end

--ldoc
--------------------------------------------------------------------
Sym.Stringify     = Stringify
Sym.MatTyper      = MatTyper
Sym.CollapseIdent = CollapseIdent
Sym.SubexprId     = SubexprId
Sym.DAGifier      = DAGifier
Sym.MatNormalizer = MatNormalizer

