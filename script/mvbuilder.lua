--ldoc
--[[
% Matrix-vector builder interfaces

The Lua matrix-vector evaluation interpreter uses `VecBuilder` and
`CSCBuilder` to reify block matrices when necessary (e.g. when preparing
a factorization of a block matrix).  This system expects objects to
support the following interface methods:

- `add_matrix(A,c,i,j)`: Add matrix $cA$ starting at $(i,j)$.
- `add_eye(n,c,i,j)`: Add $cI_n$ starting at $(i,j)$, where $I_n$ is
  an $n$-dim identity matrix.
- `add_rvec(v,c,i,j)`: Add row vector $cv$ starting at $(i,j)$
- `add_cvec(v,c,i,j)`: Add column vector $cv$ starting at $(i,j)$
- `add_scalar(x,c,i,j)`: Add scalar $cx$ starting at $(i,j)$ 
- `set/get_transpose`: Assemble into matrix or transpose?

# Vector builder interface
--]]

function VecBuilder:add_rvec(v, c, i, j)
   if type(v) == "table" then
      v = SparseV:new(v)
      self:add(v, c, (i-1)+(j-1))
      v:delete()
   else
      self:add(v, c, (i-1)+(j-1))
   end
end

VecBuilder.add_cvec = VecBuilder.add_rvec

function VecBuilder:add_scalar(x, c, i, j)
   self:add(x*c, (i-1)+(j-1))
end

--[[
# Matrix builder interface
--]]

local old_add_eye = CSCBuilder.add_eye
function CSCBuilder:add_eye(n, c, i, j)
   if self.transpose then i,j = j,i end
   old_add_eye(self, n, c, i-1, j-1)
end

local old_add_rvec = CSCBuilder.add_rvec
local old_add_cvec = CSCBuilder.add_cvec
function CSCBuilder:add_rvec(v, c, i, j)
   local adder = old_add_rvec
   if self.transpose then i,j = j,i; adder = old_add_cvec end
   if type(v) == "table" then
      v = SparseV:new(v)
      adder(self, v, c, i-1, j-1)
      v:delete()
   else
      adder(self, v, c, i-1, j-1)
   end
end

function CSCBuilder:add_cvec(v, c, i, j)
   local adder = old_add_cvec
   if self.transpose then i,j = j,i; adder = old_add_rvec end
   if type(v) == "table" then
      v = SparseV:new(v)
      adder(self, v, c, i-1, j-1)
      v:delete()
   else
      adder(self, v, c, i-1, j-1)
   end
end

local old_add_scalar = CSCBuilder.add_scalar
function CSCBuilder:add_scalar(x, c, i, j)
   if self.transpose then i,j = j,i end
   old_add_scalar(self, c*x, i-1, j-1)
end

local old_add_matrix  = CSCBuilder.add_matrix
local old_add_matrixT = CSCBuilder.add_matrixT
function CSCBuilder:add_matrix(A, c, i, j)
   local adder = old_add_matrix
   if self.transpose then i,j = j,i; adder = old_add_matrixT end
   adder(self, A, c, i-1, j-1)
end