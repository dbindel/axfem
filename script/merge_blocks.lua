--ldoc
--[[
% Merging blocks

The basic strategy of tying mapped blocks together is that we generate
blocks with compatible nodal points independently, then assign common
identifiers for all nodes that are sufficiently close.  We identify
"sufficiently close" nodes based on a position-based key, with roughly
16 bits for each of the nodal coordinates.

The main interface is via the `merge_blocks` function.
--]]
function merge_blocks(blocks)

--[[
## Getting a bounding box
--]]
local xmin, xmax, ymin, ymax
for i,block in ipairs(blocks) do
   for j,v in ipairs(block.vertices) do
      xmin = math.min(xmin or v.x, v.x)
      xmax = math.max(xmax or v.x, v.x)
      ymin = math.min(ymin or v.y, v.y)
      ymax = math.max(ymax or v.y, v.y)
   end
end

--[[
## Computing spatial keys

We notionally subdivide the bounding box into a ~60K-by-60K mesh,
indexed in row-major order.  The key for a bin is just the position
in the linear index.  Note that we leave a little "slop" room so that
we don't have to make special cases to deal with index wrap-around
at the boundaries.

Of course, this strategy will fail if vertices are so close that
multiple vertices naturally fall in neighboring boxes.  But this
seems highly unlikely unless we have truly ludicrous meshes.

--]]
local xrange = xmax-xmin
local yrange = ymax-ymin
local function hash(v)
   local x,y = v.x, v.y
   local ix = math.floor( 65500*((x-xmin)/xrange) )
   local iy = math.floor( 65500*((y-ymin)/yrange) )
   return 65536*iy + ix
end

--[[
## Vertex lookup

Whenever we see a new vertex, we first check the corresponding bin in
`vertex_hash` (and any neighboring bins) to see if a vertex with
nearly the same coordinates has already been assigned.  If it has, we
return the index of the previously-processed vertex, stored in `vertices`.
Otherwise, we add a new vertex to `vertices`, insert the corresponding
index in `vertex_hash`, and return the index for the new vertex.

--]]
local vertices = {}
local vertex_hash = {}

local function check_hash(v)
   local h = hash(v)
   local id = (vertex_hash[h] or 
               vertex_hash[h+1]     or vertex_hash[h-1] or 
               vertex_hash[h+65536] or vertex_hash[h-65536] or
               vertex_hash[h+65535] or vertex_hash[h-65535] or
               vertex_hash[h+65537] or vertex_hash[h-65537])
   if not id then
      table.insert(vertices, v)
      id = #vertices
      vertex_hash[h] = id
   end
   return id
end

--[[
## Cell rewriting

For each block, we first add all the vertices to the global list,
keeping track of the mapping from old (local) to new (global) vertex
numbers in a temporary `vmap` table.  Then we add each of the cells
to a global list, mapping the old local identifiers into new global
identifiers as we go.
--]]
local cells = {}

for i,block in ipairs(blocks) do
   local vmap = {}
   for j,v in ipairs(block.vertices) do
      vmap[j] = check_hash(v)
   end
   for j,c in ipairs(block.cell_info) do
      table.insert(cells, { bl = vmap[c.bl], br = vmap[c.br],
                            tl = vmap[c.tl], tr = vmap[c.tr],
                            matid = c.matid })
   end
end

--[[
## Returning the combined structure

At the end of the computation, we will have a combined vertex list and
a combined list of cells.  We discard any other information that may
have been added to the blocks along the way.
--]]

return { vertices=vertices, cell_info=cells }

--ldoc
end
