# AxFEM

## Project Description

AxFEM is an FEM application for axisymmetric vibrating structure 
gyroscope simulations. It is implemented in deal.ii and provides a scripting
interface by extending Lua.    

## Features 

For a given azimuthal number, AxFEM can compute:

* Eigenfrequencies
* Bryan''s factor
* Quality factor due to thermoelastic damping
* Sensitivities of the quantities above to geometric design parameters
 
## Installation and Running

There are two alternatives to run the software:

### Virtual Machine

1. Download and install [VirtualBox](https://www.virtualbox.org/)
   (or use other Virtual Machine (VM) platform which can run .vmx
   files, such as VMWare Player or Parallels).

2. Download the [zipped VM image](https://docs.google.com/open?id=0BzsFamPJRHfJTXlVZDl4Z0Z1aEU) and extract the files.  

3. Setting up the VM
   
   The VM image is based on
   [TurnKey Linux Core](http://www.turnkeylinux.org/core), a
   relatively lightweight system optimized for running on virtual
   machine platforms like VMWare and VirtualBox.  If the following
   instructions do not work, there is a
   [tutorial on installing appliances with VirtualBox](http://www.turnkeylinux.org/docs/installation-appliances-virtualbox)
   that may be helpful.  But ideally, you should be able to simply do
   the following steps:
   
   1. Run VirtualBox
   2. Click `New` to add the VM 
   3. Give a name to the VM (e.g. AxFEM)
   4. Select operating system `Linux`
   5. Select version `Other Linux`
   6. Use at least 512MB (more is better) of memory.
   7. Make sure `Start-up Disk` is selected.
   8. Choose `Use existing hard disk` and select AxFEM vmdk file
   9. Start the VM, use username `user` and password `user` to log in

   If you want to administer the virtual machine, the `root` password
   is `turnkey`.

4. The tutorials are located within the VM in the folder
   `/usr/local/axfem/doc/tutorials` 
   Alternately, they are [available online](http://www.cs.cornell.edu/~bindel/sw/axfem/doc/tutorials/index.html).

### Compile AxFEM  

1. You can download the source code from
   [the BitBucket page](https://bitbucket.org/dbindel/axfem) 
   in zip, gz or bz2 formats, or using Git: 
   
    git clone https://bitbucket.org/dbindel/axfem.git

2. Download and compile deal.ii
  1. [Download and compile deal.ii (>=7.1.0)](http://www.dealii.org)
  2. Configure with the following options:

    ./configure --disable-threads --without-petsc --without-slepc \
                    --with-blas --with-lapack --with-umfpack`
                    
  3. Then type `make all`

3. In `axfem` folder, you need to update the folder names in Makefile.in.ex
   and save it as Makefile.in.   Then type `make` and `make test`.
