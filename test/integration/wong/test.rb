require 'test/unit'

class TestHemisphere < Test::Unit::TestCase

   def setup
     @rel_tol = 3e-2;
   end

   def test_wong_compare_table1

     def split_lines(ls)
        def split_line (l)
          nl = Array.new
          l.split(" ").each { |item| nl << item }
          return nl
        end

        table = Array.new
        ls.each {|l| table << split_line(l)}
        table 
     end

     ref_lines = IO.readlines("ref/wong_table1.data")
     lines     = IO.readlines("table1.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 5)
     assert_equal(    table.size(), 5)
     for j in 0..4
        assert_equal(ref_table[j].size(), 2)
        assert_equal(    table[j].size(), 2)
     end

     for j in 0..4
        ref_temp = ref_table[j][0].to_f
            temp =     table[j][0].to_f
        assert_in_delta(ref_temp, temp, 1e-10, 
                        "Temperatures don't match! ")

        ref_qf = ref_table[j][1].to_f
            qf =     table[j][1].to_f

        rel_error = ((qf-ref_qf)/ref_qf).abs
        assert(rel_error < @rel_tol,
               "Relative error of Q for temperature #{temp} " \
               " is large : #{rel_error} > #{@rel_tol}")  
     end
  
   end


end
