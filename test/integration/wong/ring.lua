require "mapped_block"

SILICON=0

function ring_mesh_generator(R,h,L)
   local function map(x,y)
      local r = R + x*h/2;
      local z = (1+y)*L/2
      return r,z
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 2,              -- divisions in r
      n = 10}             -- divisions through thickness
end

function interpolate_lagrange(x,data)
   local size   = #data   
   local result = 0 
   for j=1,size do
     local num    = 1
     local den    = 1
     for k=1,size do
       if k ~= j then
        num = num * (        x  - data[k][1]) 
        den = den * (data[j][1] - data[k][1]) 
       end
     end
     result = result + data[j][2] * num / den 
   end
   return result 
end


function compute_alpha(T)
   local data = { {240,1.99e-6},
                  {258,2.24e-6},
                  {298,2.60e-6},
                  {320,2.85e-6},
                  {348,3.06e-6} }
   return interpolate_lagrange(T,data)
end

function compute_Cv(T)
   local data = { {240,1.51e6},
                  {258,1.52e6},
                  {298,1.64e6},
                  {320,1.68e6},
                  {348,1.73e6} }
   return interpolate_lagrange(T,data)
end

function compute_chi(T)
   local data = { {240,14.3e-5},
                  {258,11.7e-5},
                  {298,8.60e-5},
                  {320,7.92e-5},
                  {348,6.97e-5} }
   return interpolate_lagrange(T,data)
end

function compute_kappa(T)
   return compute_Cv(T)*compute_chi(T)
end

function silicon_properties(T)

   local E     = 165e9
   local nu    = 0.26
   local rho   = 2330.0

   local kappa = compute_kappa(T)
   local alpha = compute_alpha(T)
   local cv    = compute_Cv(T)/rho

   return {id     = SILICON, 
            E     = E,
            nu    = nu,
            rho   = rho,
            kappa = kappa,
            alpha = alpha,
            cv    = cv,
            T0    = T     }
end


R = 3e-3
h = 1.2e-4
L = 7e-4 


function solve_device(T)
   P = Problem:create{ params = {R,h,L},
		    m      = 2,
		    name   = "ring",
		    meshgen= ring_mesh_generator }

   P:add_material(silicon_properties(T))

   stepsizeratio = 1000;
   stepsizes = P:get_stepsizes(stepsizeratio);
   P:mesh_gradient(stepsizes);

   function free(r,z,d)
     return false 
   end

   P:set_boundary_conditions{ radial  ={free},
			      angular ={free},
			      axial   ={free},
			      temperature={free}}
   Q = P:compute_quality_factor()
   return Q
end

table1 = io.open("table1.data","w")
for i,T in ipairs{240,258,298,320,348} do
   Q = solve_device(T)
   table1:write(T," ",Q,"\n") 
end
table1:flush()
table1:close()


qfile = io.open("ring-QvsT.data","w")
for T=240,350,5 do
   Q = solve_device(T)
   qfile:write(T," ",Q,"\n") 
end
qfile:flush()
qfile:close()

