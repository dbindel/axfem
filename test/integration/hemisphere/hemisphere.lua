require 'mapped_block'

SILICON=0

--                              
--                     0,0________ 3_4
--                      |\ |       |*|
--                      |_\t2      /*/
--                      |  \      /*/
--                      |t1 \    /*/
--                      |     1 /*/
--                      |       \/
--                      v       2
--                              
function hemisphere_mesh_generator(R, h, theta1, theta2)
   local function map(x,y)
      local r = R + y*h/2
      local t = pi/2-( (1-x)*theta2 + (1+x)*theta1 )/2
      return r*cos(t), -r*sin(t)
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 32,             -- divisions along meridian
      n = 3}              -- divisions through thickness
end

R = 1e-3  -- mean radius
h = 1e-5  -- thickness

T0    = 300

function solve (P,theta1) 
   P:add_material{
      id    = SILICON,
      E     = 165e9;
      nu    = 0.26;
      rho   = 2330.0;
      kappa = 142;
      alpha = 2.6e-6;
      cv    = 710;
      T0    = 300;
   }

   stepsizeratio = 100;
   stepsizes     = P:get_stepsizes(stepsizeratio);
   P:mesh_gradient(stepsizes);

   function free(r,z,d)
     return false 
   end

   function fixed(r,z,d)
     return abs(theta1 - atan2(r,-z)) < 0.01*(d/r)
   end

   P:set_boundary_conditions{ radial  = { fixed},
			      angular = { fixed},
			      axial   = { fixed},
			      temperature={free} }

   BF = P:compute_bryans_factor()
   return BF
end

-- We will sweep parameters theta1 and theta2
data1 = io.open("colatitude15.data","w")
for t1=10,20,2.5 do
  local theta1 = t1*pi/180  
  local theta2 = 90*pi/180
  local P = Problem:create{ params  = {R,h,theta1,theta2}, 
		    m       = 2,
		    name    = "hemisphere", 
		    meshgen = hemisphere_mesh_generator }
   
   bf = solve(P,theta1)
   data1:write(t1," ",bf,"\n")
end
data1:flush()
data1:close()

data2 = io.open("colatitude90.data","w")   
for t2=80,100,5 do
   local theta1 = 15*pi/180;
   local theta2 = t2*pi/180;
   local P = Problem:create{ params  = {R,h,theta1,theta2}, 
		    m       = 2,
		    name    = "hemisphere", 
		    meshgen = hemisphere_mesh_generator }
   
   bf = solve(P,theta1)
   data2:write(t2," ",bf,"\n")
end
data2:flush()
data2:close()
