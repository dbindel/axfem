require 'test/unit'

class TestHemisphere < Test::Unit::TestCase

   def setup
     @rel_tol1      = 1e-1;
     @rel_tol4      = 1e-2;
     @rel_tol5      = 1e-2;
     @rel_tol6      = 1e-2;
     @rel_tol8      = 1e-2;
     @rel_tol9      = 1e-2;
   end

   def split_lines(ls)
      def split_line (l)
        nl = Array.new
          l.split(" ").each { |item| nl << item }
          return nl
      end

        table = Array.new
        ls.each {|l| table << split_line(l)}
        table 
   end

   def test_niordson_table1
     ref_lines = IO.readlines("ref/table1.data")
     lines     = IO.readlines("table1.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 40)
     assert_equal(    table.size(), 40)
     for j in 0..39
        assert_equal(ref_table[j].size(), 3)
        assert_equal(    table[j].size(), 3)
     end

     for j in 0..39
        ref_alpha1 = ref_table[j][0].to_f
            alpha1 =     table[j][0].to_f
        assert_in_delta(ref_alpha1, alpha1, 1e-10, 
                        "Opening angles don't match! ")
        ref_alpha2 = ref_table[j][1].to_f
            alpha2 =     table[j][1].to_f
        assert_in_delta(ref_alpha2, alpha2, 1e-10, 
                        "Opening angles don't match! ")

        ref_result = ref_table[j][2].to_f
            result =     table[j][2].to_f

        rel_error = ((result-ref_result)/ref_result).abs
        assert(rel_error < @rel_tol1,
               "Relative error in Table1, (alpha1=#{alpha1}, " \
               "alpha2=#{alpha2}) "\
               " is large : #{rel_error} > #{@rel_tol1}")  
     end
   end

   def test_niordson_table4
     ref_lines = IO.readlines("ref/table4.data")
     lines     = IO.readlines("table4.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 20)
     assert_equal(    table.size(), 20)
     for j in 0..19
        assert_equal(ref_table[j].size(), 3)
        assert_equal(    table[j].size(), 3)
     end

     for j in 0..19
        ref_m = ref_table[j][0].to_i
            m =     table[j][0].to_i
        assert_equal(ref_m, m, 
                        "Azimuthal numbers in row #{j} don't match!")

        ref_alpha2 = ref_table[j][1].to_f
            alpha2 =     table[j][1].to_f
        assert_in_delta(ref_alpha2, alpha2, 1e-10, 
                        "Opening angles in row #{j} don't match! ")

        ref_result = ref_table[j][2].to_f
            result =     table[j][2].to_f

        rel_error = ((result-ref_result)/ref_result).abs
        assert(rel_error < @rel_tol4,
               "Relative error in Table4 (m=#{m}, " \
               "alpha2=#{alpha2}) "\
               " is large : #{rel_error} > #{@rel_tol4}")  
     end
   end

   def test_niordson_table5
     ref_lines = IO.readlines("ref/table5.data")
     lines     = IO.readlines("table5.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 20)
     assert_equal(    table.size(), 20)
     for j in 0..19
        assert_equal(ref_table[j].size(), 3)
        assert_equal(    table[j].size(), 3)
     end

     for j in 0..19
        ref_hoR = ref_table[j][0].to_f
            hoR =     table[j][0].to_f
        assert_in_delta(ref_hoR, hoR, 1e-10, 
                        "h/R ratios in row #{j} don't match! ")
        ref_alpha2 = ref_table[j][1].to_f
            alpha2 =     table[j][1].to_f
        assert_in_delta(ref_alpha2, alpha2, 1e-10, 
                        "Opening angles don't match! ")

        ref_result = ref_table[j][2].to_f
            result =     table[j][2].to_f

        rel_error = ((result-ref_result)/ref_result).abs
        assert(rel_error < @rel_tol5,
               "Relative error in Table5, (hoR=#{hoR}, " \
               "alpha2=#{alpha2}) "\
               " is large : #{rel_error} > #{@rel_tol5}")  
     end
   end

   def test_niordson_table6
     ref_lines = IO.readlines("ref/table6.data")
     lines     = IO.readlines("table6.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 25)
     assert_equal(    table.size(), 25)
     for j in 0..24
        assert_equal(ref_table[j].size(), 3)
        assert_equal(    table[j].size(), 3)
     end

     for j in 0..24
        ref_nu = ref_table[j][0].to_f
            nu =     table[j][0].to_f
        assert_in_delta(ref_nu, nu, 1e-10, 
                        "Poissons ratios in row #{j} don't match! ")
        ref_alpha2 = ref_table[j][1].to_f
            alpha2 =     table[j][1].to_f
        assert_in_delta(ref_alpha2, alpha2, 1e-10, 
                        "Opening angles don't match! ")

        ref_result = ref_table[j][2].to_f
            result =     table[j][2].to_f

        rel_error = ((result-ref_result)/ref_result).abs
        assert(rel_error < @rel_tol6,
               "Relative error in Table6, (nu=#{nu}, " \
               "alpha2=#{alpha2}) "\
               " is large : #{rel_error} > #{@rel_tol6}")  
     end
   end
=begin
   def test_niordson_table8
     ref_lines = IO.readlines("ref/table8.data")
     lines     = IO.readlines("table8.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 48)
     assert_equal(    table.size(), 48)
     for j in 0..47
        assert_equal(ref_table[j].size(), 4)
        assert_equal(    table[j].size(), 4)
     end

     for j in 0..47
        ref_alpha = ref_table[j][0].to_f
            alpha =     table[j][0].to_f
        assert_in_delta(ref_alpha, alpha, 1e-10, 
                        "Opening angles in row #{j} don't match! ")
        ref_m = ref_table[j][1].to_i
            m =     table[j][1].to_i
        assert_equal(ref_m, m, 
                        "Azimuthal numbers in row #{j} don't match! ")

        ref_hoR = ref_table[j][2].to_f
            hoR =     table[j][2].to_f
        assert_in_delta(ref_hoR, hoR, 1e-10, 
                        "h/R ratios in row #{j} don't match! ")

        ref_result = ref_table[j][3].to_f
            result =     table[j][3].to_f

        rel_error = ((result-ref_result)/ref_result).abs
        assert(rel_error < @rel_tol8,
               "Relative error in Table8, (alpha=#{alpha}, " \
               "m=#{m},h/R=#{hoR}) "\
               " is large : #{rel_error} > #{@rel_tol8}")  
     end
   end

   def test_niordson_table9
     ref_lines = IO.readlines("ref/table9.data")
     lines     = IO.readlines("table9.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 20)
     assert_equal(    table.size(), 20)
     for j in 0..19
        assert_equal(ref_table[j].size(), 3)
        assert_equal(    table[j].size(), 3)
     end

     for j in 0..19
        ref_alpha = ref_table[j][0].to_f
            alpha =     table[j][0].to_f
        assert_in_delta(ref_alpha, alpha, 1e-10, 
                        "Opening angles in row #{j} don't match! ")

        ref_m = ref_table[j][1].to_i
            m =     table[j][1].to_i
        assert_equal(ref_m, m, 
                        "Azimuthal numbers in row #{j} don't match!")

        ref_result = ref_table[j][2].to_f
            result =     table[j][2].to_f

        rel_error = ((result-ref_result)/ref_result).abs
        assert(rel_error < @rel_tol9,
               "Relative error in Table9 (m=#{m}, " \
               "alpha=#{alpha}) "\
               " is large : #{rel_error} > #{@rel_tol9}")  
     end
   end
=end
end
