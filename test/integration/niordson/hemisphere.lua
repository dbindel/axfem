require 'mapped_block'

SILICON=0

function hemisphere_mesh_generator(R, h, theta1, theta2, nr, nt)
   local function map(x,y)
      local r = R + y*h/2
      local t = pi/2-( (1-x)*theta2 + (1+x)*theta1 )/2
      return r*cos(t), -r*sin(t)
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = nt,             -- divisions along meridian
      n = nr}             -- divisions through thickness
end

--                              
--                     0,0________ 3_4
--                      |\ |       |*|
--                      |_\t2      /*/
--                      |  \      /*/
--                      |t1 \    /*/
--                      |     1 /*/
--                      |       \/
--                      v       2
--                              
function hemisphere_two_boundaries(R, h, theta1, theta2)
   return hemisphere_mesh_generator(R, h, theta1, theta2, 4, 64)
end


--                              
--                     0,0_
--                      |  \      
--                      |_/ \      
--                      | t  \   
--                      |     3-4
--                      |     / /
--                      1----/ / 
--                      2---- / 
--
function hemisphere_one_boundary(R, h, theta)
   return hemisphere_mesh_generator(R, h, 0, theta, 3, 32)
end

R = 1e-3  -- mean radius
h = 1e-5  -- thickness

T0    = 300

function free_free(P)
   function free(r,z,d)
     return false 
   end

   P:set_boundary_conditions{ radial  = { free},
			      angular = { free},
			      axial   = { free},
			      temperature={free} }

end

--[[ TODO: need to think about table9
function clamped_clamped(P)
   function free(r,z,d)
     return false 
   end

   function fixed(r,z,d)
     return abs(theta1 - atan2(r,-z)) < 0.01*(d/r)
   end

   P:set_boundary_conditions{ radial  = { fixed},
			      angular = { fixed},
			      axial   = { fixed},
			      temperature={free} }

end

function clamped_free(P) 
   function free(r,z,d)
     return false 
   end

   function fixed(r,z,d)
     return abs(theta1 - atan2(r,-z)) < 0.01*(d/r)
   end

   P:set_boundary_conditions{ radial  = { fixed},
			      angular = { fixed},
			      axial   = { fixed},
			      temperature={free} }
end
]]



function solve (P,theta1,theta2, boundary_type) 
   stepsizeratio = 100
   stepsizes     = P:get_stepsizes(stepsizeratio)
   P:mesh_gradient(stepsizes)
   boundary_type(P)
   return  P:get_eigenfrequency()
end

--
table1 = io.open("table1.data","w")

for a2=15,75,15 do
   for a1=(a2+15),165,15 do
      local theta1 = a2*pi/180
      local theta2 = a1*pi/180 
      local P = Problem:create{ params  = {R,h,theta1,theta2}, 
		    m       = 2,
		    name    = "hemisphere", 
		    meshgen = hemisphere_two_boundaries }
      P:add_material{
         id    = SILICON,
         E     = 165e9,
         nu    = 0.3, --table1, niordson
         rho   = 2330.0,
         kappa = 142,
         alpha = 2.6e-6,
         cv    = 710,
         T0    = T0 
      }
      wx = h/R/R * sqrt(165e9/2/(1+0.3)/2330)
      freq = solve(P,theta1,theta2,free_free)
      table1:write(a1," ",a2," ",freq/wx,"\n")
   end
end

table1:flush()
table1:close()

--
table4 = io.open("table4.data","w")

for a2=15,75,15 do
   for m=2,5 do
      local theta1 = a2*pi/180
      local theta2 =    pi/2 
      local P = Problem:create{ params  = {R,h,theta1,theta2}, 
		    m       = m,
		    name    = "hemisphere", 
		    meshgen = hemisphere_two_boundaries }

      P:add_material{
         id    = SILICON,
         E     = 165e9,
         nu    = 0.3, -- as in table5, niordson's paper.
         rho   = 2330.0,
         kappa = 142,
         alpha = 2.6e-6,
         cv    = 710,
         T0    = T0 
      }
      wx = h/R/R * sqrt(165e9/2/(1+0.3)/2330)
      freq = solve(P,theta1,theta2,free_free)
      table4:write(m," ",a2," ",freq/wx,"\n")
   end
end

table4:flush()
table4:close()

--
table5 = io.open("table5.data","w")

for a2=15,75,15 do
   for _,hoR in ipairs{0.005,0.01,0.02,0.04} do
      local theta1 = a2*pi/180
      local theta2 =    pi/2
      -- here adjust the h/R ratio usgin hoR, let's keep R constant
      hx = R * hoR -- should keep the global variable safe!! 
      local P = Problem:create{ params  = {R,hx,theta1,theta2}, 
		    m       = 2,
		    name    = "hemisphere", 
		    meshgen = hemisphere_two_boundaries }

      P:add_material{
         id    = SILICON,
         E     = 165e9,
         nu    = 0.3, -- as in table5, niordson's paper.
         rho   = 2330.0,
         kappa = 142,
         alpha = 2.6e-6,
         cv    = 710,
         T0    = T0 
      }
      wx = hx/R/R * sqrt(165e9/2/(1+0.3)/2330)
      freq = solve(P,theta1,theta2,free_free)
      table5:write(hoR," ",a2," ",freq/wx,"\n")
   end
end

table5:flush()
table5:close()

--
table6 = io.open("table6.data","w")

for a2=15,75,15 do
   for nidx=0,4 do
      nu = nidx * 0.1
      local theta1 = a2*pi/180
      local theta2 =    pi/2
      local P = Problem:create{ params  = {R,h,theta1,theta2}, 
		    m       = 2,
		    name    = "hemisphere", 
		    meshgen = hemisphere_two_boundaries }
      P:add_material{

         id    = SILICON,
         E     = 165e9,
         nu    = nu,
         rho   = 2330.0,
         kappa = 142,
         alpha = 2.6e-6,
         cv    = 710,
         T0    = T0 
      }
      wx = h/R/R * sqrt(165e9/2/(1+nu)/2330)
      freq = solve(P,theta1,theta2,free_free)
      table6:write(nu," ",a2," ",freq/wx,"\n")
   end
end

table6:flush()
table6:close()


--[[ TODO also uncomment related methods in tests.rb
--
table8 = io.open("table8.data","w")

for a2=60,150,30 do
   for m=2,4 do
      for _,hoR in ipairs{0.04,0.02,0.01,0.005} do
        local theta = a2*pi/180
        hx = R * hoR
        local P = Problem:create{ params  = {R,hx,theta}, 
		               m       = 2,
                               name    = "hemisphere", 
		               meshgen = hemisphere_one_boundary }

      wx = hx/R/R * sqrt(165e9/2/(1+nu)/2330)
      freq = solve(P,theta,); -- THIS NEEDS BOUNDARY CONDITION
      table8:write(m," ",hoR," ",a2," ",freq/wx,"\n")
   end
  end
end
table8:flush()
table8:close()



--
table9 = io.open("table9.data","w")

for bcidx,bctype in ipairs{ }  do
   for a2=60,150,30 do
      local theta = a2*pi/180
      local P = Problem:create{ params  = {R,h,theta}, 
		    m       = 2,
		    name    = "hemisphere", 
		    meshgen = hemisphere_one_boundary }

      wx = hx/R/R * sqrt(165e9/2/(1+nu)/2330)
      freq = solve(P,theta); -- THIS NEEDS BOUNDARY CONDITION
      table9:write(bcidx," ",a2," ",freq/wx,"\n")
  end
end

table9:flush()
table9:close()
]]
