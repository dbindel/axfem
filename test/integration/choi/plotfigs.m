  for j=3:9
    fbase = ['fig' num2str(j)];
    fname = [fbase '.data'];
    data = load(fname);
    x = data(:,2);
    y = data(:,1);
    z = data(:,3);
    XI = unique(x);
    YI = unique(y);
    ZI = griddata(x,y,z,XI,YI');
    h  = figure();
    surf(XI,YI,ZI);
    view(135,30);
    print(h,'-dpng',[fbase '.png']);
    close(h)
  end

  exit

