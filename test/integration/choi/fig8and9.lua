fig8   = io.open("fig8.data","w")
fig8hs = io.open("fig8-hs.data","r")
fig8rg = io.open("fig3.data","r")

for nr=1,5,0.5 do
   for nh=0.4,1.6,0.12  do

   Rhs = fig8hs:read("*n")
   Rrg = fig8rg:read("*n")
   assert(math.abs((Rhs-Rrg)/Rhs) < 1e-16)

   hhs = fig8hs:read("*n")
   hrg = fig8rg:read("*n")
   assert(math.abs((hhs-hrg)/hhs) < 1e-16)

   Qhs = fig8hs:read("*n")
   Qrg = fig8rg:read("*n")
   Qdiff = 100*(Qhs-Qrg)/Qhs

   fig8:write(Rhs," ",hhs," ",Qdiff,"\n")

   end
end

fig8:close()
fig8hs:close()
fig8rg:close()

fig9   = io.open("fig9.data","w")
fig9hs = io.open("fig4.data","r")
fig9rg = io.open("fig9-rg.data","r")

for nr=2,7,0.5 do
   for nh=0.5,3,0.5  do

   Rhs = fig9hs:read("*n")
   Rrg = fig9rg:read("*n")
   assert(math.abs((Rhs-Rrg)/Rhs) < 1e-16)

   hhs = fig9hs:read("*n")
   hrg = fig9rg:read("*n")
   assert(math.abs((hhs-hrg)/hhs) < 1e-16)

   Qhs = fig9hs:read("*n")
   Qrg = fig9rg:read("*n")
   Qdiff = 100*(Qhs-Qrg)/Qhs

   fig9:write(Rhs," ",hhs," ",Qdiff,"\n")

   end
end

fig9:close()
fig9hs:close()
fig9rg:close()
