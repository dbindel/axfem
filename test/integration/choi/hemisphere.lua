require 'mapped_block'

SILICON=0

function hemisphere_mesh_generator(R, h)
   local function map(x,y)
      local r = R + y*h/2
      local t = pi*(1+x)/4
      return r*cos(t), -r*sin(t)
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 32,             -- divisions along meridian
      n = 3}              -- divisions through thickness
end

R = 1e-3  -- mean radius
h = 1e-5  -- thickness

T0    = 300

function solve (P) 
   P:add_material{
      id    = SILICON,
      E     = 165e9;
      nu    = 0.26;
      rho   = 2330.0;
      kappa = 142;
      alpha = 2.6e-6;
      cv    = 710;
      T0    = 300;
    }

   stepsizeratio = 100;
   stepsizes     = P:get_stepsizes(stepsizeratio);
   P:mesh_gradient(stepsizes);

   function free(r,z,d)
     return false 
   end

   function fixed(r,z,d)
      return (r < d/100)
   end

   P:set_boundary_conditions{ radial  = { fixed},
			      angular = { fixed},
			      axial   = { free },
			      temperature={free} }

   Q = P:compute_quality_factor()
   return Q
end

fig4 = io.open("fig4.data","w")
for nr=2,7,0.5 do
   for nh=0.5,3,0.5  do
       local R = nr*1e-2
       local h = nh*1e-3
       local P = Problem:create{ params  = {R,h}, 
		       m       =  2, 
		       name    = "hemisphere", 
		       meshgen = hemisphere_mesh_generator }
       Q = solve(P)
       fig4:write(R," ",h," ",Q,"\n") 
   end
end
fig4:flush()
fig4:close()

fig5 = io.open("fig5.data","w")
for nr=2,7,0.5 do
   for nh=0.5,3,0.5  do
       local R = nr*1e-2
       local h = nh*1e-3
       local P = Problem:create{ params  = {R,h}, 
		       m       =  3, 
		       name    = "hemisphere", 
		       meshgen = hemisphere_mesh_generator }
       Q = solve(P)
       fig5:write(R," ",h," ",Q,"\n") 
   end
end
fig5:flush()
fig5:close()

fig6 = io.open("fig6.data","w")
for nr=2,7,0.5 do
   for nh=0.5,3,0.5  do
       local R = nr*1e-2
       local h = nh*1e-3
       local P = Problem:create{ params  = {R,h}, 
		       m       =  4, 
		       name    = "hemisphere", 
		       meshgen = hemisphere_mesh_generator }
       Q = solve(P)
       fig6:write(R," ",h," ",Q,"\n") 
   end
end
fig6:flush()
fig6:close()

fig7 = io.open("fig7.data","w")
for nr=2,7,0.5 do
   for nh=0.5,3,0.5  do
       local R = nr*1e-2
       local h = nh*1e-3
       local P = Problem:create{ params  = {R,h}, 
		       m       =  5, 
		       name    = "hemisphere", 
		       meshgen = hemisphere_mesh_generator }
       Q = solve(P)
       fig7:write(R," ",h," ",Q,"\n") 
   end
end
fig7:flush()
fig7:close()


fig8 = io.open("fig8-hs.data","w")
for nr=1,5,0.5 do
   for nh=0.4,1.6,0.12  do
       local R = nr*1e-3
       local h = nh*1e-4
       local P = Problem:create{ params  = {R,h}, 
		       m       =  2, 
		       name    = "hemisphere", 
		       meshgen = hemisphere_mesh_generator }
       Q = solve(P)
       fig8:write(R," ",h," ",Q,"\n") 
   end
end
fig8:flush()
fig8:close()



