require "mapped_block"

SILICON=0
T0 = 300

function ring_mesh_generator(R,h,L)
   local function map(x,y)
      local r = R + x*h/2;
      local z = (1+y)*L/2
      return r,z
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 2,              -- divisions in r
      n = 10}             -- divisions through thickness
end

function solve(P)
   P:add_material{
      id    = SILICON,
      E     = 165e9;
      nu    = 0.26;
      rho   = 2330.0;
      kappa = 142;
      alpha = 2.6e-6;
      cv    = 710;
      T0    = 300;
    }

   stepsizeratio = 1000;
   stepsizes = P:get_stepsizes(stepsizeratio);
   P:mesh_gradient(stepsizes);

   function free(r,z,d)
     return false 
   end

   P:set_boundary_conditions{ radial  ={free},
			      angular ={free},
			      axial   ={free},
			      temperature={free}}
   Q = P:compute_quality_factor()
   return Q
end


fig3 = io.open("fig3.data","w")
for nr=1,5,0.5 do
   for nh=0.4,1.6,0.12  do
       local R = nr*1e-3
       local h = nh*1e-4
       local L = 5*h 
       local P = Problem:create{ params  = {R,h,L}, 
		       m       =  2, 
		       name    = "ring", 
		       meshgen = ring_mesh_generator }
       Q = solve(P)
       fig3:write(R," ",h," ",Q,"\n") 
   end
end
fig3:flush()
fig3:close()

fig9 = io.open("fig9-rg.data","w")
for nr=2,7,0.5 do
   for nh=0.5,3,0.5  do
       local R = nr*1e-2
       local h = nh*1e-3
       local L = 5*h 
       local P = Problem:create{ params  = {R,h,L}, 
		       m       =  2, 
		       name    = "ring", 
		       meshgen = ring_mesh_generator }
       Q = solve(P)
       fig9:write(R," ",h," ",Q,"\n") 
   end
end
fig9:flush()
fig9:close()



