require 'test/unit'

class TestHemisphere < Test::Unit::TestCase

   def setup
     @rel_tol1      = 1e-3;
     @rel_tol2      = 1e-3;
     @rel_tol3      = 1e-2;
   end



   def test_hwang_compare_table1

     def split_lines(ls)
        def split_line (l)
          nl = Array.new
          l.split(" ").each { |item| nl << item }
          return nl
        end

        table = Array.new
        ls.each {|l| table << split_line(l)}
        table 
     end

     ref_lines = IO.readlines("ref/hwang_table1.data")
     lines     = IO.readlines("table1.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 7)
     assert_equal(    table.size(), 7)
     for j in 0..6
        assert_equal(ref_table[j].size(), 3)
        assert_equal(    table[j].size(), 2)
     end

     for j in 0..6
        ref_theta = ref_table[j][0].to_f
            theta =     table[j][0].to_f
        assert_in_delta(ref_theta, theta, 1e-10, 
                        "Opening angles don't match! ")

        ref_angle = ref_table[j][1].to_f
            angle =     table[j][1].to_f

        rel_error = ((angle-ref_angle)/ref_angle).abs
        assert(rel_error < @rel_tol1,
               "Relative error (2,1), theta =#{theta} " \
               " is large : #{rel_error} > #{@rel_tol1}")  
     end
  
   end

   def test_hwang_compare_table2

     def split_lines(ls)
        def split_line (l)
          nl = Array.new
          l.split(" ").each { |item| nl << item }
          return nl
        end

        table = Array.new
        ls.each {|l| table << split_line(l)}
        table 
     end

     ref_lines = IO.readlines("ref/hwang_table2.data")
     lines     = IO.readlines("table2.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 9)
     assert_equal(    table.size(), 9)
     for j in 0..8
        assert_equal(ref_table[j].size(), 3)
        assert_equal(    table[j].size(), 3)
     end

     for j in 0..8
        ref_p     = ref_table[j][0].to_i
            p     =     table[j][0].to_i

        assert_equal(ref_p,p,"Azimuthal numbers don't match!")


        ref_theta1= ref_table[j][1].to_f
            theta1=     table[j][1].to_f
        assert_in_delta(ref_theta1, theta1, 1e-6, 
                        "Opening angles don't match! ")

        ref_angle = ref_table[j][2].to_f
            angle =     table[j][2].to_f

        rel_error = ((angle-ref_angle)/ref_angle).abs
        assert(rel_error < @rel_tol2,
               "Relative error (#{p},1), theta1=#{theta1} " \
               " is large : #{rel_error} > #{@rel_tol2}")  
     end
  
   end

   def test_hwang_compare_table3

     def split_lines(ls)
        def split_line (l)
          nl = Array.new
          l.split(" ").each { |item| nl << item }
          return nl
        end

        table = Array.new
        ls.each {|l| table << split_line(l)}
        table 
     end

     ref_lines = IO.readlines("ref/hwang_table3.data")
     lines     = IO.readlines("table3.data")
     
     ref_table = split_lines(ref_lines) 
     table     = split_lines(lines)

     assert_equal(ref_table.size(), 32)
     assert_equal(    table.size(), 16)
     for j in 0..15
        assert_equal(ref_table[j].size(), 4)
        assert_equal(    table[j].size(), 4)
     end

     for j in 0..15
        ref_p     = ref_table[j][0].to_i
            p     =     table[j][0].to_i

        assert_equal(ref_p,p,"Azimuthal numbers don't match!")


        ref_n     = ref_table[j][1].to_i
            n     =     table[j][1].to_i
 
       assert_equal(ref_n,n,"Meridional numbers don't match!")

        ref_nu     = ref_table[j][2].to_f
            nu     =     table[j][2].to_f
 
       assert_in_delta(ref_nu, nu, 1e-10, 
                        "Poisson's ratios don't match! ")

        ref_f = ref_table[j][3].to_f
            f =     table[j][3].to_f

        rel_error = ((f-ref_f)/ref_f).abs
        assert(rel_error < @rel_tol3,
               "Relative error (#{p},#{n}), nu=#{nu} " \
               " is large : #{rel_error} > #{@rel_tol3}")  
     end
  
   end


end
