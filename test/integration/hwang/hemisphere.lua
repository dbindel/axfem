require 'mapped_block'

SILICON=0

function hemisphere_mesh_generator(R, h, theta)
   local function map(x,y)
      local r = R + y*h/2
      local t = pi/2-(1-x)*theta/2
      return r*cos(t), -r*sin(t)
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 32,             -- divisions along meridian
      n = 3}              -- divisions through thickness
end

R = 1e-3  -- mean radius
h = 1e-5  -- thickness

T0    = 300

function solve (P) 

   stepsizeratio = 100;
   stepsizes     = P:get_stepsizes(stepsizeratio);
   P:mesh_gradient(stepsizes);

   function free(r,z,d)
     return false 
   end

   function fixed(r,z,d)
      return (r < d/100)
   end

   P:set_boundary_conditions{ radial  = { fixed},
			      angular = { fixed},
			      axial   = { free },
			      temperature={free} }

   BF = P:compute_bryans_factor()
   return BF
end

--
table1 = io.open("table1.data","w")
P = Problem:create{ params  = {R,h,pi/2}, 
                 m       =  2, 
		 name    = "hemisphere", 
		 meshgen = hemisphere_mesh_generator }
P:add_material{
      id    = SILICON,
      E     = 165e9;
      nu    = 0.26;
      rho   = 2330.0;
      kappa = 142;
      alpha = 2.6e-6;
      cv    = 710;
      T0    = 300;
   }
     
bf = solve(P)
for i,angle in ipairs{12,13,15,23,25,27,48} do
   table1:write(angle," ",angle*(1-bf),"\n")
end
table1:flush()
table1:close()

-- 
table2 = io.open("table2.data","w")   
for p = 2,4 do
   for t1=60,90,15 do
      local theta0 = 0;
      local theta1 = t1*pi/180;
      local P = Problem:create{ params  = {R,h,theta1}, 
		       m       =  p, 
		       name    = "hemisphere", 
		       meshgen = hemisphere_mesh_generator }
      
       P:add_material{
               id    = SILICON,
	       E     = 165e9;
	       nu    = 0.26;
	       rho   = 2330.0;
	       kappa = 142;
	       alpha = 2.6e-6;
	       cv    = 710;
	       T0    = 300;
	     }

      bf = solve(P)
      table2:write(p," ",t1," ",90*(1-bf),"\n") -- angle_rotated = 90*(1-bf) 
   end
end
table2:flush()
table2:close()

--
table3 = io.open("table3.data","w")   
for p = 2,5 do
   for nidx = 1,4 do
      nu = nidx * 0.1
      local theta0 = 0;
      local theta1 = pi/2;
      local P = Problem:create{ params  = {R,h,theta1}, 
		       m       =  p, 
		       name    = "hemisphere", 
		       meshgen = hemisphere_mesh_generator }
        P:add_material{
               id    = SILICON,
	       E     = 165e9;
	       nu    = nu;
	       rho   = 2330.0;
	       kappa = 142;
	       alpha = 2.6e-6;
	       cv    = 710;
	       T0    = 300;
	     }

      bf = solve(P)
      table3:write(p," 1 ",nu," ",-p*bf,"\n")
   end
end
table3:flush()
table3:close()


