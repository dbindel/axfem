require "models.test_helpers"
require "models.mesh_generators"
require "materials"

T0    = 300
SILICON=get_silicon(0,T0)

R = 2e-3
h = 1.6e-4
L = 7e-4 

P = Problem:create{ params = {R,h,L},
                 m      = 2,
                 refinement = 0,
                 name   = "ring",
                 meshgen= ring_mesh_generator }

P:add_material(SILICON)

stepsizeratio = 1000;
stepsizes = P:get_stepsizes(stepsizeratio)
P:mesh_gradient(stepsizes)
-- P:write_eps("ring.eps")

function free(r,z,d)
  return false 
end

P:set_boundary_conditions{ radial  ={free},
                           angular ={free},
                           axial   ={free},
                           temperature={free}}

compute_and_save_all(P)

print_profile()
