require "models.test_helpers"
require "models.mesh_generators"
require "materials"

T0 = 300
SILICON=get_silicon(0,T0)

P = Problem:create{ params = {2e-3,3.5e-4,1.6e-4}, 
                 m      = 2,
                 name   = "torus",
                 refinement = 1,
                 meshgen= torus_mesh_generator }
P:add_material(SILICON)
P:mesh_gradient()

P:write_eps("torus.eps")
P:write_svg("torus.svg")

function free(r,z,d)
  return false 
end

P:set_boundary_conditions{ radial     = {free},
                           angular    = {free},
                           axial      = {free}, 
                           temperature= {free}}
compute_and_save_all(P)

P:write_svg{"tdisp0.svg"; field=0}
P:write_svg{"tdisp1.svg"; field=1}
P:write_svg{"tdisp2.svg"; field=2}

print_profile()
