require 'test/unit'

class TestTorus < Test::Unit::TestCase

   def setup
     @rel_tol      = 1e-2;

     @rel_tol_freq = @rel_tol;
     @rel_tol_qf   = @rel_tol;
     @rel_tol_bf   = @rel_tol;

     @rel_tol_eigsens = @rel_tol;
     @rel_tol_qfsens  = @rel_tol;
     @rel_tol_bfsens  = @rel_tol;
   end

   def test_eigenfrequency
     ref_lines = IO.readlines("ref/eigenfrequency.txt")
     lines     = IO.readlines("eigenfrequency.txt")
     ref_freq  = ref_lines[0].to_f 
     freq      = lines[0].to_f
     rel_error = ((freq-ref_freq)/freq).abs
     assert(rel_error < @rel_tol_freq,"Relative error for eigenfrequency "\
                              "is large : #{rel_error} > #{@rel_tol_freq}")  
   end

   def test_eigenvector
   end
  
   def test_temperature
   end

   def test_quality_factor
     ref_lines = IO.readlines("ref/quality_factor.txt")
     lines     = IO.readlines("quality_factor.txt")
     ref_qf    = ref_lines[0].to_f 
     qf        = lines[0].to_f
     rel_error = ((qf-ref_qf)/qf).abs
     assert(rel_error < @rel_tol_qf,"Relative error for quality factor "\
                              "is large : #{rel_error} > #{@rel_tol_qf}")  
  end

   def test_bryans_factor 
     ref_lines = IO.readlines("ref/bryans_factor.txt")
     lines     = IO.readlines("bryans_factor.txt")
     ref_bf    = ref_lines[0].to_f 
     bf        = lines[0].to_f
     rel_error = ((bf-ref_bf)/bf).abs
     assert(rel_error < @rel_tol_bf,"Relative error for bryans factor "\
                             "is large : #{rel_error} > #{@rel_tol_bf}")  
  end

   def test_eigenfrequency_sensitivity
     ref_lines = IO.readlines("ref/eigenfrequency_sensitivities.txt")
     lines     = IO.readlines("eigenfrequency_sensitivities.txt")
     assert_equal(ref_lines.size,lines.size)
     nparams   = lines.size
     (0..(nparams-1)).each do |j|
        ref_sens  = ref_lines[j].to_f 
        sens      = lines[j].to_f
        rel_error = ((sens-ref_sens)/sens).abs
        assert(rel_error < @rel_tol_eigsens,"Relative error to parameter "\
                "index #{j} is large : #{rel_error} > #{@rel_tol_eigsens}")  
     end
  end

   def test_quality_factor_sensitivity
     ref_lines = IO.readlines("ref/quality_factor_sensitivities.txt")
     lines     = IO.readlines("quality_factor_sensitivities.txt")
     assert_equal(ref_lines.size,lines.size)
     nparams   = lines.size
     (0..(nparams-1)).each do |j|
        ref_sens  = ref_lines[j].to_f 
        sens      = lines[j].to_f
        rel_error = ((sens-ref_sens)/sens).abs
        assert(rel_error < @rel_tol_qfsens,"Relative error to parameter "\
                "index #{j} is large : #{rel_error} > #{@rel_tol_qfsens}")  
     end
  end

   def test_bryans_factor_sensitivity
     ref_lines = IO.readlines("ref/bryans_factor_sensitivities.txt")
     lines     = IO.readlines("bryans_factor_sensitivities.txt")
     assert_equal(ref_lines.size,lines.size)
     nparams   = lines.size
     (0..(nparams-1)).each do |j|
        ref_sens  = ref_lines[j].to_f 
        sens   = lines[j].to_f
        rel_error = ((sens-ref_sens)/sens).abs
        assert(rel_error < @rel_tol_bfsens,"Relative error to parameter "\
                "index #{j} is large : #{rel_error} > #{@rel_tol_bfsens}")  
     end
  end  

end
