require 'models.test_helpers'
require 'models.mesh_generators'
require 'materials'

T0    = 300
SILICON=get_silicon(0,T0)

R = 1e-3  -- mean radius
h = 1e-5  -- thickness
theta1 = 15*pi/180  -- angle at the south pole side, measured from south axis
theta2 = 90*pi/180  -- angle at the north pole side, measured from south axis


P = Problem:create{ params  = {R,h,theta1,theta2}, 
                 m       = 2,
                 name    = "hemisphere",
                 refinement = 3, 
                 meshgen = hemisphere_mesh_generator }

P:add_material(SILICON)
P:mesh_gradient()

P:write_eps("hemisphere.eps")
P:write_svg("hemisphere.svg")

-- Dirichlet boundary conditions are indicated as a boolean function
-- For each component [radial, angular, axial, temperature] we define
-- an associated list of test functions. If any one of these functions
-- return true at the inspection point, we assume the corresponding
-- component is fixed. 

-- For a boundary condition, radial  and axial coordinate of a point
-- is tested. The (r,z) plane is represented with (x,y) plane.
-- for the sake of tolerance and floating point comparison issues
-- the diameter of the smallest cell in the mesh is also provided in 
-- the third parameter, d. These functions always have r,z,d arguments
-- and are called from the C++ side. 

-- A free boundary is not a Dirichlet boundary
function free(r,z,d)
  return false 
end

-- We assume the spherical shell is fixed around the boundary
-- on to the pole side.
function fixed(r,z,d)
  return abs(theta1 - atan2(r,-z)) < 0.01*(d/r)
end

P:set_boundary_conditions{ radial  = { fixed},
                           angular = { fixed},
                           axial   = { fixed},
                           temperature={free} }

compute_and_save_all(P)

P:write_svg{"hdisp0.svg"; field=0}
P:write_svg{"hdisp1.svg"; field=1}
P:write_svg{"hdisp2.svg"; field=2}

print_profile()
