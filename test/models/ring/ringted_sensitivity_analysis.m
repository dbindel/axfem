function ringted_sensitivity_analysis

% ===================== Parameter setup ==============================

% -- Material parameters
E     = 165e9;
nu    = 0.26;
rho   = 2330.0;
kappa = 142;
alpha = 2.6e-6;
cv    = 710;
T0    = 300;

% -- Geometric parameters
R = 2e-3;
h = 1.6e-4;
L = 7e-4;

% -- Azimuthal wave number
aznum = 2;

% ===================== Analytical values ==============================

% -- Reference (analytical) values for frequency. BF
ref_w0 = sqrt(E/rho)*h*aznum*(aznum^2-1)/sqrt(aznum^2+1)/(R^2)/sqrt(12);
ref_BF = 2/(aznum^2+1);

% -- Derivatives of frequency and BF with respect to geometry
ref_dw(1) = -2*ref_w0/R;
ref_dw(2) = ref_w0/h;
ref_dw(3) = 0;

% -- Reference values for Q_ted

chi = kappa/rho/cv;
xsi = h*sqrt(ref_w0/2/chi);
y  = (sinh(xsi)+sin(xsi))/(cosh(xsi)+cos(xsi));
yy = (6/xsi^2-6/xsi^3*y);
ref_Q = 1/(E*alpha^2*T0/rho/cv * yy);

% -- Analytical sensitivity of Q_ted

dQdyy = -ref_Q^2*E*alpha^2*T0/rho/cv;
dydxsi = ((cosh(xsi)+cos(xsi))*(cosh(xsi)+cos(xsi))-...
   (sinh(xsi)-sin(xsi))*(sinh(xsi)+sin(xsi)))/...
   ((cosh(xsi)+cos(xsi))^2);
dyydxsi = -12/xsi^3 + 18/xsi^4*y - 6/xsi^3*dydxsi;

ref_dQ(1) = dQdyy*dyydxsi*( -xsi/R    );
ref_dQ(2) = dQdyy*dyydxsi*( 3*xsi/2/h );
ref_dQ(3) = dQdyy*dyydxsi*( 0         );

% ===================== FEA values ==============================

% -- Load system matrices
[Kttr,Mttr]      = load_kmb('ring-%sttg.txt');
[Kuur,Muur,Buur] = load_kmb('ring-%suug.txt');
n_u = length(Muur);
n_t = length(Mttr);
Kutr = load_sparse('ring-Kutg.txt', n_u, n_t);

% -- Initial eigenvalue / eigenvector computation
[V,D]   = eigs(Kuur,Muur,10,'sm');
[w,idx] = sort(sqrt(diag(D)));
V = V(:,idx);
u0 = V(:,1);
w0 = w(1);

% -- Finite element Bryan's factor computation
b0 = u0'*Buur*u0; 
m0 = u0'*Muur*u0;
BF = b0/m0/aznum;

% -- Finite element Q_ted computation
Ated  = 1i*w0*Mttr+Kttr;
theta = Ated\(-1i*w0 * T0 * Kutr' * u0);
imt   = imag(theta);
imz   = (u0'*Kutr*imt)/(u0'*Kuur*u0);
Q     = 1/abs(imz);

% ===================== FEA sensitivity ==============================

for k = 1:3

    % -- Load changes
    suffix = sprintf('-%d.txt', k-1);
    [Kttx,Mttx]      = load_kmb(['ring-%stts', suffix], n_t);
    [Kuux,Muux,Buux] = load_kmb(['ring-%suus', suffix], n_u);
    Kutx = load_sparse(['ring-Kuts', suffix], n_u, n_t);

    % -- Compute frequency derivs
    dw(k) = (u0'*(Kuux-w0^2*Muux)*u0)/(2*w0*u0'*Muur*u0);

    % -- Compute eigenvector derivs
    vbord = w0^2*Muur*u0;
    Amech = [ Kuur-w0^2*Muur, vbord ;
              u0',             0    ];
    rhs = [ w0^2*Muux*u0-Kuux*u0; 0 ];
    dx = Amech\rhs;
    u1 = dx(1:end-1);

    % -- Compute TED derivs
    thetap = Ated\(-1i*dw(k)*T0*Kutr'*u0-1i*w0*T0*Kutx'*u0-...
                   1i*w0*T0*Kutr'*u1-(1i*dw(k)*Mttr+1i*w0*Mttx+Kttx)*theta);

    imtp = imag(thetap);
    imzp = ((u1'*Kutr*imt + u0'*Kutx*imt + u0'*Kutr*imtp)*(u0'*Kuur*u0) - ...
            (u1'*Kuur*u0 + u0'*Kuur*u1 + u0'*Kuux*u0)*(u0'*Kutr*imt))/((u0'*Kuur*u0)^2);
     
    dQ(k) = -Q^2*imzp*sign(imz);
    
    % -- Compute BF derivs
    db = u1'*Buur*u0 + u0'*Buux*u0 + u0'*Buur*u1;
    dm = u1'*Muur*u0 + u0'*Muux*u0 + u0'*Muur*u1;
    dBF(k) = (db/m0 - b0*dm/(m0^2))/aznum;
    
end

% ===================== Compare results ==============================

fprintf('Relerr w0   : %.2e\n', abs(w0-ref_w0)/abs(w0));
fprintf('Relerr BF   : %.2e\n', abs(BF-ref_BF)/abs(BF));
fprintf('Relerr Q    : %.2e\n', abs(Q-ref_Q)/abs(Q));
fprintf('\n');

params = [R, h, L];
names  = {'R', 'h', 'L'};
rdiff = @(a,b) abs(a-b)/abs(b);
fprintf('             \t FEA     \t ref     \treldiff\n');
fprintf('             \t --------\t --------\t-------\n');
for k = 1:3
    
    % Sensitivities via FEA computation
    kappa_w_fea = params(k)*dw(k)/w0;
    kappa_B_fea = params(k)*dBF(k)/BF;
    kappa_Q_fea = params(k)*dQ(k)/Q;
    
    % Sensitivities via ref computation
    kappa_w_ref = params(k)*ref_dw(k)/ref_w0;
    kappa_B_ref = 0;
    kappa_Q_ref = params(k)*ref_dQ(k)/ref_Q;
    fprintf('Sens w  vs %s:\t% .2e\t% .2e\t%.2e\n', ...
            names{k}, kappa_w_fea, kappa_w_ref, ...
            rdiff(kappa_w_fea, kappa_w_ref));
    fprintf('Sens BF vs %s:\t% .2e\t% .2e\t%.2e\n', ...
            names{k}, kappa_B_fea, kappa_B_ref, ...
            rdiff(kappa_B_fea, kappa_B_ref));
    fprintf('Sens Q  vs %s:\t% .2e\t% .2e\t%.2e\n', ...
            names{k}, kappa_Q_fea, kappa_Q_ref, ...
            rdiff(kappa_Q_fea, kappa_Q_ref));
end

% ===================== Load matrices ==============================

function [K,M,B] = load_kmb(fpattern, m, n)

if nargin == 2, n = m; end

if nargin == 1
    K = load_sparse(sprintf(fpattern, 'K'));
    [m,n] = size(K);
else
    K = load_sparse(sprintf(fpattern, 'K'), m, n);
end
M = load_sparse(sprintf(fpattern, 'M'), m, n);
if nargout == 3
  B = load_sparse(sprintf(fpattern, 'B'), m, n);
end
