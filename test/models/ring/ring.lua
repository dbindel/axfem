require "models.test_helpers"
require "models.mesh_generators"
require "materials"

T0    = 300
SILICON = get_silicon(0,T0)


R = 2e-3
h = 1.6e-4
L = 7e-4 

P = Problem:create{ params = {R,h,L},
                 m      = 2,
                 refinement = 5,
                 name   = "ring",
                 meshgen= ring_mesh_generator }

P:add_material(SILICON)
P:mesh_gradient()

P:write_eps("ring.eps")
P:write_svg("ring.svg")

function free(r,z,d)
  return false 
end

P:set_boundary_conditions{ radial  ={free},
                           angular ={free},
                           axial   ={free},
                           temperature={free}}

compute_and_save_all(P)

P:write_svg{"rdisp0.svg"; field=0}
P:write_svg{"rdisp1.svg"; field=1}
P:write_svg{"rdisp2.svg"; field=2}

print_profile()
