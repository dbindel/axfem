assembler = Assembler:new(5,5)

assembler:add_entries({0,1}, {0,1}, {1,2,2,3})
assembler:add_entries({1,2}, {1,2}, {5,9,7,11})
assembler:add_entries({1,2,3,4}, {5}, {3,1,4,1})
assembler:add_entries({2}, {2,3,5}, {2,1,7})

A = CSCMatrix:new(assembler)
assembler:delete()

Acols = {{8, 9, 0, 0, 0},
         {7, 13, 0, 0, 0},
         {0, 1, 0, 0, 0},
         {0, 0, 0, 0, 0},
         {3, 8, 4, 1, 0}}

for j=1,5 do
   ej = {0,0,0,0,0}
   ej[j] = 1
   y = A:vmult(ej)
   assert(#y == 5)
   for i=1,5 do
      assert(Acols[j][i] == y[i])
   end
end

A:delete()
