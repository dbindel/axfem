--[[
A1        = [1, 2;  3, 4]
A2        = [0, 1; -1, 0]
A1+A2     = [1, 3; 2, 4]
A1+A2+2*I = [3, 3; 2, 6]

Then add bordering to get

C = [3, 3, 10;
     2, 7, 11;
     12, 13, 14]
--]]

local jc1 = {0, 2, 4}
local ir1 = {0,1, 0,1}
local pr1 = {1,3, 2,4}

local jc2 = {0, 1, 2}
local ir2 = {1, 0}
local pr2 = {-1, 1}

A1 = CSCMatrix:new(2,2, jc1,ir1,pr1)
A2 = CSCMatrix:new(2,2, jc2,ir2,pr2)

builder = CSCBuilder:new(3,3)
builder:add_matrix(A1,1,1,1)
builder:add_matrix(A2,1,1,1)
builder:add_eye(2,2,1,1)
builder:add_cvec({10,11}, 1, 1,3)
builder:add_rvec({12,13}, 1, 3,1)
builder:add_scalar(2,7,3,3)
C = builder:get_matrix()
builder:delete()

Cref = { {3, 3, 10},
         {2, 6, 11},
         {12, 13, 14} }
for i=1,3 do
   local ei = {0, 0, 0}
   ei[i] = 1
   y = C:Tvmult(ei)
   for j=1,3 do
      assert(y[j] == Cref[i][j])
   end
end

C:delete()
A2:delete()
A1:delete()
