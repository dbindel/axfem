-- Basic data

A1 = {11,  0,  0,  0,  0,  0,  8,
       0,  0,  0,  0,  0, 15,  0,
       0, 13,  7, 19, 11,  0,  0,
       1, 11,  0,  0, 11, 11,  2,
      17, 10,  2,  5,  8,  0,  0}

jc = {0,   3,  6,  8, 10, 13, 15, 17} 
ir = {0, 3, 4,
      2, 3, 4,  
      2, 4,
      2, 4,
      2, 3, 4,
      1, 3,
      0, 3}
pr = {11, 1, 17,
      13, 11, 10,
      7, 2,
      19, 5,
      11, 11, 8,
      15, 11,
      8, 2}

v  = {8, 3, 8, 5, 3, 3, 3}
Av = {112, 45, 223, 113, 231}

w = {1, 1, 8, 9, 0}
wA = {20, 203, 56, 152, 187, 114, 26}

A = CSCMatrix:new(5, 7, jc, ir, pr)

y = A:vmult(v)
z = A:Tvmult(w)

local function same(v1,v2)
   if #v1 ~= #v2 then return false end
   for i=1,#v1 do
      if v1[i] ~= v2[i] then return false end
   end
   return true
end

local function printv(v1)
   print("[ " .. table.concat(v1, " ") .. "]")
end

assert(A.m == 5,  "Incorrect behavior for m")
assert(A.n == 7,  "Incorrect behavior for n")
assert(same(Av,y), "Incorrect results from CSC vmult")
assert(same(wA,z), "Incorrect results from CSC Tvmult")

A:delete()
