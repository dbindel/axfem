jc = {0, 1, 4, 5, 7, 8}
ir = {0,  1, 2,3,  2,  1,3,  4}
pr = {2,  11,8,9,  4,  4,2,  2}

A = CSCMatrix:new(5,5, jc,ir,pr)
LU = UMFMatrix:new(A)

b   = {93, 30, 30, 27, 6}
Ab  = {186, 438, 360, 324, 12}
ATb = {186, 813, 120, 174, 12}

b1 = LU:vmult(Ab)
b2 = LU:Tvmult(ATb)

local function samet(v1,v2,tol)
   if #v1 ~= #v2 then return false end
   for i=1,#v1 do
      if math.abs(v1[i]-v2[i]) > tol then return false end
   end
   return true
end

assert(samet(b1,b,1e-10), "Check vmult with UMFmatrix")
assert(samet(b2,b,1e-10), "Check Tvmult with UMFmatrix")

LU:delete()
A:delete()
