require "models.test_helpers"
require "models.mesh_generators"
require "materials"

T0    = 300
SILICON = get_silicon(0,T0)

function solve(P)

  P:add_material(SILICON)
  P:mesh_gradient()

  function free(r,z,d)
     return false 
  end

  P:set_boundary_conditions{ radial  ={free},
                             angular ={free},
                             axial   ={free},
                             temperature={free}}

   fr = P:get_eigenfrequency();
   bf = P:compute_bryans_factor();
   qf = P:compute_quality_factor();
   
   frs = {}; bfs = {}; qfs = {}
   for j=0,(#P.params-1) do
      frs[j+1] = P:eigenfrequency_sensitivity(j)
      bfs[j+1] = P:bryansfactor_sensitivity(j)
      qfs[j+1] = P:qualityfactor_sensitivity(j)
   end

   return {fr=fr, bf=bf, qf=qf, frs=frs, qfs=qfs, bfs=bfs} 
end
--------------------------------------------------------------------------

R = 2e-3
h = 1.6e-4
L = 7e-4

prob =  { params = {R,h,L},
          m      = aznum,
          name   = "ring",
          meshgen= ring_mesh_generator }

test_convergence(prob)

