require 'models.test_helpers'
require 'models.mesh_generators'
require 'materials'

T0 = 300
SILICON=get_silicon(0,T0)

function solve(P)

   P:add_material(SILICON)
   P:mesh_gradient();

   function free(r,z,d)
      return false 
   end

   function fixed(r,z,d)
     return abs(theta1 - atan2(r,-z)) < 0.01*(d/r)
   end

   P:set_boundary_conditions{ radial  = { fixed},
                              angular = { fixed},
                              axial   = { fixed},
                              temperature={free} }

   fr = P:get_eigenfrequency();
   bf = P:get_bryans_factor();
   qf = P:get_quality_factor();
   
   frs = {}; bfs = {}; qfs = {}
   for j=0,(#P.params-1) do
      frs[j+1] = P:eigenfrequency_sensitivity(j)
      bfs[j+1] = P:bryansfactor_sensitivity(j)
      qfs[j+1] = P:qualityfactor_sensitivity(j)
   end

   return {fr=fr, bf=bf, qf=qf, frs=frs, qfs=qfs, bfs=bfs} 
end

----------------------------------------------------------------------------

R = 1e-3  -- mean radius
h = 1e-5  -- thickness
theta1 = 15*pi/180  -- angle at the south pole side, measured from south axis
theta2 = 90*pi/180  -- angle at the north pole side, measured from south axis

prob = { params     = {R,h,theta1,theta2}, 
         name       = "hemisphere",
         meshgen    = hemisphere_mesh_generator }

test_convergence(prob)

