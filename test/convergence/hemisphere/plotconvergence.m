function plotconvergence(aznum,num_params)
  qty_label = {'fr', 'bf','qf'};
  num_qty   = length(qty_label);
  
  for qty_idx=1:num_qty
     lbl = qty_label(qty_idx);
     fname  = [lbl{1}, '_aznum', num2str(aznum)];
     convergence_plot(fname,2,3,strcat([fname '_conv.png']));
     fsname = [lbl{1}, '_sens_aznum', num2str(aznum)];
     for param_idx=1:num_params
        convergence_plot(fsname,2,param_idx+2,strcat([fsname,'_conv', num2str(param_idx), '.png']));
     end
  end
  

function convergence_plot(fname,hmax_col,data_col,plotname)
  data = load(strcat(fname,'.data'));
  qty = data(:,data_col);
  hmax= data(:,hmax_col);
  n   = length(hmax);
  relerr = zeros(n,1);
  for j=2:n
     relerr(j) = abs((qty(j)-qty(j-1))/qty(j));
  end

  h = figure();
  loglog(1./hmax,relerr,'*-');
  print(h,'-dpng',plotname);
  close(h);
  
