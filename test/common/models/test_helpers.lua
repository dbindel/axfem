
function compute_and_save_all(P)

   f0 = P:get_eigenfrequency()
   BF = P:get_bryans_factor()
   Q  = P:get_quality_factor()

   f = io.open("eigenfrequency.txt","w")
   print("w0 : "..f0)
   f:write(f0)
   f:close()

   f = io.open("bryans_factor.txt","w")
   print("BF : "..BF)
   f:write(BF)
   f:close()

   f = io.open("quality_factor.txt","w")
   print("Q : "..Q)
   f:write(Q)
   f:close()

   nparams = P:get_num_params()

   f = io.open("eigenfrequency_sensitivities.txt","w")
   print("Eigenfrequency Sensitivities")
   for i=0,(nparams-1) do
     fs = P:eigenfrequency_sensitivity(i)
     f:write(fs.."\n")
     print("fs"..i.." : "..fs)
   end
   f:flush()
   f:close()

   f = io.open("bryans_factor_sensitivities.txt","w")
   print("Bryans Factor Sensitivities")
   for i=0,(nparams-1) do
     bfs = P:bryansfactor_sensitivity(i)
     f:write(bfs.."\n")
     print("bfs"..i.." : "..bfs)
   end
   f:flush()
   f:close()


   f = io.open("quality_factor_sensitivities.txt","w")
   print("Quality Factor Sensitivities")
   for i=0,(nparams-1) do
     qfs = P:qualityfactor_sensitivity(i)
     f:write(qfs.."\n")
     print("qfs"..i.." : "..qfs)
   end
   f:flush()
   f:close()

end


function test_convergence (prob,aznums,refinements)
   nparams = #prob.params
   aznums = aznums or {2,3}
   refinements = refinements or {0,1,2,3}

   for _,aznum in ipairs(aznums) do
      prob.m = aznum

      fr_file       = io.open("fr_aznum"..aznum..".data","w")
      bf_file       = io.open("bf_aznum"..aznum..".data","w")
      qf_file       = io.open("qf_aznum"..aznum..".data","w")
      fr_sens_file  = io.open("fr_sens_aznum"..aznum..".data","w")
      bf_sens_file  = io.open("bf_sens_aznum"..aznum..".data","w")
      qf_sens_file  = io.open("qf_sens_aznum"..aznum..".data","w")
     
      for _,refinement in ipairs(refinements) do
        prob.refinement = refinement 
	P = Problem:new(prob)
        result = solve(P)
        hmax = P:get_maximal_cell_diameter()
        P:delete()
      fr_file:write(refinement," ",hmax," ",result.fr,"\n")
      bf_file:write(refinement," ",hmax," ",result.bf,"\n")
      qf_file:write(refinement," ",hmax," ",result.qf,"\n")

      fr_sens_file:write(refinement," ",hmax)
      bf_sens_file:write(refinement," ",hmax)
      qf_sens_file:write(refinement," ",hmax)

      for j=0,(nparams-1) do
	 fr_sens_file:write(" ",result.frs[j+1])
	 bf_sens_file:write(" ",result.bfs[j+1])
	 qf_sens_file:write(" ",result.qfs[j+1])
      end
      fr_sens_file:write("\n")
      bf_sens_file:write("\n")
      qf_sens_file:write("\n")

      end -- of refinement loop

      fr_file:close()
      bf_file:close()
      qf_file:close()
      fr_sens_file:close()
      bf_sens_file:close()
      qf_sens_file:close()
   end

end
