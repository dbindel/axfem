require 'mapped_block'
require 'merge_blocks'

--                              
--                     0,0________ 3_4
--                      |\ |       |*|
--                      |_\t2      /*/
--                      |  \      /*/
--                      |t1 \    /*/
--                      |     1 /*/
--                      |       \/
--                      v       2
--                              
function hemisphere_mesh_generator(R, h, theta1, theta2)
   local function map(x,y)
      local r = R + y*h/2
      local t = pi/2-( (1-x)*theta2 + (1+x)*theta1 )/2
      return r*cos(t), -r*sin(t)
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 24,             -- divisions along meridian
      n = 1}              -- divisions through thickness
end

function ring_mesh_generator(R,h,L)
   local function map(x,y)
      local r = R + x*h/2;
      local z = (1+y)*L/2
      return r,z
   end
   return mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 1,              -- divisions in r
      n = 5}             -- divisions through thickness
end

--
--                   ____4____
--                  / ___3___ \
--                 / /       \ \
--                / /         \ \
--(0,0)          | |     R     | |
--  -------------6-5----->\    1 2
--               | |       \r  | |
--                \ \       \ / /
--                 \ \___7___/h/
--                  \____8____/
--                         

function torus_mesh_generator(R, r, h)
   local function map(x,y)
      local rr = r + y*h/2
      local t  = (1+x)*pi
      return R + rr*cos(t), -rr*sin(t)
   end
   return merge_blocks { mapped_block {
      phi = map,          -- mapping function
      material = SILICON, -- material type
      m = 4*32,           -- divisions along meridian
      n = 8}}              -- divisions through thickness
end
