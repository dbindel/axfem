
function get_silicon (ID,T0)
  return       {   
   id    = ID;
   E     = 165e9;
   nu    = 0.26;
   rho   = 2330.0;
   kappa = 142;
   alpha = 2.6e-6;
   cv    = 710;
   T0    = T0; }
end

function get_pyrex (id)
   return {}
end

function get_nitride (ID,T0)
   return   {
   id   = ID,
   E    = 310e9,
   nu   = 0.24,
   rho  = 3270.0,
   kappa= 30,
   alpha= 3.3e-6,
   cv   = 500,
   T0   = T0 }
end
